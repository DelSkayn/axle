extern crate axle;
extern crate notify;
extern crate libloading;
#[macro_use]
extern crate log;

use self::notify::*;

use axle::registery::Registery;
use axle::console::Console;
use axle::render::Render;
use axle::window::WindowBuilder;
use axle::window::context::Context;
use axle::Game;

use std::result::Result;

use std::process::{Stdio,Command};
use std::fs;
use std::sync::mpsc::{self,Receiver};
use std::time::Duration;

mod linked_game; 
use self::linked_game::LinkedGame;

enum CompilerError{
    FailedToCompile,
    CouldNotRename,
}

fn build_library(name: &str, version_addition: u32) -> Result<(),CompilerError>{
    println!("[runtime] Building library!");

    //clean old files
    for file in fs::read_dir("target/debug/").unwrap(){
        let dir = file.unwrap();
        if dir.file_type().unwrap().is_file(){
            fs::remove_file(dir.path()).unwrap();
        }
    }
    let result = Command::new("cargo")
            .arg("build")
            .arg("--lib")
            .arg("--features")
            .arg("dylib")
            .arg("--")
            .env("CARGO_TARGET_DIR","target")
            .stdout(Stdio::inherit())
            .spawn()
            .unwrap()
            .wait()
            .unwrap();

    info!("[runtime] build res: {}",result);

    if !result.success() {
        return Err(CompilerError::FailedToCompile);
    }

    info!("[runtime] Renaming library to: \"target/debug/lib{}_v{}.so\"",name,version_addition);
    if let Err(_) = fs::rename(format!("target/debug/lib{}.so",name)
                              ,format!("target/debug/lib{}_v{}.so",name,version_addition)) {
        return Err(CompilerError::CouldNotRename);
    }
    Ok(())
}

fn game_loop(name:String,r: Receiver<DebouncedEvent>){
    let mut version = 1;
    let mut running = true;
    let mut window = WindowBuilder::from_registery().build();
    let mut render = Render::new(Context::new(&window)); 
    let mut game = Some(LinkedGame::from_path(format!("target/debug/lib{}_v{}.so",name,0),&mut render,&window).unwrap());
    info!("[runtime] Entering axle game loop!");
    while running {
        //-------------------- Hot loading part ------------------------- 
        while let Ok(x) = r.try_recv() {
            match x {
                DebouncedEvent::Write(_) => {
                    info!("[runtime] Rebuilding!");
                    if build_library(&name,version).is_ok() {
                        info!("[runtime] Compilation complete running game!");
                        game = Some(LinkedGame::from_old(
                                format!("target/debug/lib{}_v{}.so",name,version),game.take().unwrap()).unwrap());
                        version += 1;
                    }else{
                        info!("[runtime] Compilation failed continuing with old version");
                    }
                }
                _ => {}
            }
        }
        //------------------- Hot loading part ------------------------
        window.update();
        game.as_mut().unwrap().update(&mut running,&window,&mut render);
        Console::update();
    }
    info!("[runtime] Game shutdown, exiting axle runtime!");
}

fn main(){
    Console::update();
    let name: String = Registery::get_basic("cargo.package.name").unwrap();
    let (s,r) = mpsc::channel();
    let mut watcher: RecommendedWatcher = Watcher::new(s,Duration::from_secs(5)).unwrap();
    watcher.watch("src/",RecursiveMode::Recursive).unwrap();
    info!("[runtime] === Starting axle runtime! ===");
    if build_library(&name,0).is_err() {
        info!("[runtime] Compilation failed, waiting on changes.");
        loop {
            if let Ok(x) = r.recv() {
                match x {
                    DebouncedEvent::Write(_) => {
                        info!("[runtime] Found changes: Rebuilding!");
                        if build_library(&name,0).is_ok(){
                            info!("[runtime] Compilation complete running game!");
                            break;
                        }
                    }
                    _ => {}
                }
            } else {
                break;
            }
        }
    }else{
        info!("[runtime] Compilation complete running game!");
    }
    game_loop(name,r);
}


