extern crate libloading;

use ::axle::window::{Window};
use ::axle::render::Render;
use ::axle::{hotload,Game};

use libloading::{Library,Symbol};
use std::io::Error;
use std::mem;

type UpdateFunction = extern fn(*mut (), &mut bool, &Window,&mut Render);

pub struct LinkedGame{
    #[allow(dead_code)]
    library: Box<Library>,
    update_func: Symbol<'static,UpdateFunction>,
    game: *mut (),
}

impl LinkedGame{
    pub fn from_path(path: String,render: &mut Render,window: &Window) -> Result<Self,Error>{
        let lib = Box::new(Library::new(path)?);
        {
            // load the runtime into the library
            let runtime_func: Symbol<hotload::LoadFunc> = unsafe{ lib.get(b"__axle_core_receive")? };
            unsafe{ runtime_func(hotload::provide()) };
            // retreive the game function from the library
        }
        // create the game.
        let game = {
            let create_func: Symbol<extern fn(r: &mut Render,&Window) -> *mut ()> = unsafe{ lib.get(b"__axle_create_new_game")? };
            create_func(render,window)
        };

        // load the update func from the library
        let update_func = unsafe{ mem::transmute(lib.get::<UpdateFunction>(b"__axle_update_game")?) };
        Ok(LinkedGame{
            library: lib,
            update_func: update_func,
            game: game,
        })
    }

    pub fn from_old(path: String, old: LinkedGame)  -> Result<Self,Error>{
        let lib = Box::new(Library::new(path)?);
        {
            // load the runtime into the library
            let runtime_func: Symbol<hotload::LoadFunc> = unsafe{ lib.get(b"__axle_core_receive")? };
            unsafe{ runtime_func(hotload::provide()) };
            // retreive the game function from the library
        }

        // load the update func from the library
        let update_func = unsafe{ mem::transmute(lib.get::<UpdateFunction>(b"__axle_update_game")?) };
        Ok(LinkedGame{
            library: lib,
            update_func: update_func,
            game: old.game,
        })
    }
}

impl Game for LinkedGame{
    fn new(_: &mut Render,_: &Window) -> Self{
        panic!("new should not be called on linked game!");
    }

    fn update(&mut self,running: &mut bool,window: &Window,render: &mut Render){
        (*self.update_func)(self.game,running,window,render);
    }
}
