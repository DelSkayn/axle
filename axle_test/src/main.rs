extern crate axle_core;
extern crate axle_render;

use axle_core::*;

fn main(){
    console::Logger::init().unwrap();
    let registery = registery::Registery::from_file("axle.json");
    let config: axle_render::Config = registery.get("render").unwrap();
    let _renderer = axle_render::Renderer::new(config).unwrap();
}
