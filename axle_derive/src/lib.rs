
extern crate proc_macro;
extern crate syn;
#[macro_use]
extern crate quote;

use quote::{ToTokens, Tokens};
use self::syn::{VariantData, Body, TyParam};
use self::proc_macro::TokenStream;

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};


#[proc_macro_derive(Any)]
pub fn derive_type_id(input: TokenStream) -> TokenStream {
    let source = input.to_string();
    let input = syn::parse_derive_input(&source).unwrap();
    let mut hash = DefaultHasher::new();
    input.ident.hash(&mut hash);
    match input.body {
        Body::Enum(x) => {
            for var in x.iter() {
                match var.data {
                    VariantData::Struct(ref x) => {
                        for field in x {
                            let mut tokens = Tokens::new();
                            field.ty.to_tokens(&mut tokens);
                            tokens.as_str().hash(&mut hash);
                        }
                    }
                    VariantData::Tuple(ref x) => {
                        for field in x {
                            let mut tokens = Tokens::new();
                            field.ty.to_tokens(&mut tokens);
                            tokens.as_str().hash(&mut hash);
                        }
                    }
                    ref x => {
                        let mut tokens = Tokens::new();
                        x.to_tokens(&mut tokens);
                        tokens.as_str().hash(&mut hash);
                    }
                }
            }
        }
        Body::Struct(var) => {
            match var {
                VariantData::Struct(ref x) => {
                    for field in x {
                        let mut tokens = Tokens::new();
                        field.ty.to_tokens(&mut tokens);
                        tokens.as_str().hash(&mut hash);
                    }
                }
                VariantData::Tuple(ref x) => {
                    for field in x {
                        let mut tokens = Tokens::new();
                        field.ty.to_tokens(&mut tokens);
                        tokens.as_str().hash(&mut hash);
                    }
                }
                ref x => {
                    let mut tokens = Tokens::new();
                    x.to_tokens(&mut tokens);
                    tokens.as_str().hash(&mut hash);
                }
            }
        }
    }
    let hash = hash.finish();
    let name = &input.ident;
    let (impl_gen, ty_gen, where_clause) = input.generics.split_for_impl();
    let tokens = if let Some(x) = gen_add(&input.generics.ty_params) {
        quote!{
            impl #impl_gen Any for #name #ty_gen #where_clause {
                fn id_u64(&self) -> u64{
                    use std::mem;
                    #hash.wrapping_add((#hash << 1).wrapping_add(#x))
                }
            }
        }
    } else {
        quote!{
            impl #impl_gen Any for #name #ty_gen #where_clause {
                fn id_u64(&self) -> u64{
                    #hash
                }
            }
        }
    };
    tokens.parse().unwrap()
}

fn gen_add(types: &[TyParam]) -> Option<Tokens> {
    if types.len() == 0 {
        None
    } else {
        Some(if let Some(inner) = gen_add(&types[1..]) {
                 let ty = &types[0].ident;
                 quote!{
                {
                    let val = unsafe{ mem::transmute::<_,&#ty>(&()).id_u64() };
                    val.wrapping_add(val << 1).wrapping_add(#inner)
                }
            }
             } else {
                 let ty = &types[0].ident;
                 quote!{
                unsafe{ mem::transmute::<_,&#ty>(&()).id_u64() }
            }
             })
    }
}
