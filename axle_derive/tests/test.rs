
#[macro_use]
extern crate axle_derive;

#[derive(Debug,Copy,Clone,Eq,PartialEq,Hash)]
pub struct TypeId(u64);

impl TypeId{
    pub fn of<T: Any>() -> Self{
        use std::mem;
        unsafe{
            // should be fine since type_id does not access any of 
            // data that is pressent in borrow
            let borrow: &T = mem::transmute(&());
            borrow.type_id()
        }
    }
}

pub trait Any: 'static {
    #[doc(hidden)]
    fn id_u64(&self) -> u64;

    fn type_id(&self) -> TypeId{
        TypeId(self.id_u64())
    }
}

impl Any for i32{
    fn id_u64(&self) -> u64{
        1
    }
}

impl Any for u32{
    fn id_u64(&self) -> u64{
        3
    }
}

impl Any for u64{
    fn id_u64(&self) -> u64{
        2
    }
}

#[derive(Any)]
pub struct Foo<T: 'static + Any>{
    _inner: T
}
mod a{
    use super::Any;
    #[derive(Any)]
    pub struct Foo(u64);

    #[derive(Any)]
    pub struct Bar{
        a: u32,
        b: u32,
    }
}
mod b{
    use super::Any;
    #[derive(Any)]
    pub struct Foo(u64);

    #[derive(Any)]
    pub struct Bar{
        d: u32,
        e: u32,
    }

}
mod c{
    use super::Any;
    #[derive(Any)]
    pub struct Foo(u32);

    #[derive(Any)]
    pub struct Bar{
        a: i32,
        b: i32,
    }
}

#[test]
fn same_type_id(){
    println!("a::Foo: {:#?}",TypeId::of::<a::Foo>());
    println!("b::Foo: {:#?}",TypeId::of::<b::Foo>());
    println!("a::Bar: {:#?}",TypeId::of::<a::Bar>());
    println!("b::Bar: {:#?}",TypeId::of::<b::Bar>());
    if TypeId::of::<a::Foo>() != TypeId::of::<b::Foo>(){
        panic!();
    }
    if TypeId::of::<a::Bar>() != TypeId::of::<b::Bar>(){
        panic!();
    }
}

#[test]
fn not_the_same_type_id(){
    println!("a::Foo: {:#?}",TypeId::of::<a::Foo>());
    println!("c::Foo: {:#?}",TypeId::of::<c::Foo>());
    println!("a::Bar: {:#?}",TypeId::of::<a::Bar>());
    println!("c::Bar: {:#?}",TypeId::of::<c::Bar>());
    println!("Foo<i32>: {:#?}",TypeId::of::<Foo<i32>>());
    println!("Foo<u32>: {:#?}",TypeId::of::<Foo<u32>>());
    if TypeId::of::<a::Foo>() == TypeId::of::<c::Foo>(){
        panic!();
    }
    if TypeId::of::<a::Bar>() == TypeId::of::<c::Bar>(){
        panic!();
    }
    if TypeId::of::<Foo<i32>>() == TypeId::of::<Foo<u32>>(){
        panic!();
    }
    if TypeId::of::<Foo<Foo<i32>>>() == TypeId::of::<Foo<Foo<u32>>>(){
        panic!();
    }
}

