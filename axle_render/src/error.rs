
use vulkano::{instance,device};

error_chain! {

    errors{

        RendererCreation (msg: String) {
            description("error happend during renderer creation"),
            display("error happend during renderer creation: {}",msg),
        }

        NoPhysicalDevice {
            description("no graphical device which supports vulkan found"),
        }

        NoQueueFamily{
            description("the physical device does not support a proper queue family")
        }
    }

    foreign_links {
        Device(device::DeviceCreationError);
        Instance(instance::InstanceCreationError);
        LayerList(instance::LayersListError);
    }
}
