use super::core::window::context::{Context as CoreContext};
use super::core::{self,window};
use std::sync::Arc;
use std::{error,fmt};

use vulkano::instance::{Instance,InstanceExtensions,ApplicationInfo,Version,InstanceCreationError};
use vulkano::swapchain::{Surface,SurfaceCreationError};

#[derive(Debug)]
pub enum Error{
    Surface(SurfaceCreationError),
    Instance(InstanceCreationError),
}

impl fmt::Display for Error{
    fn fmt(&self,fmt: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::Surface(ref x) => write!(fmt,"Error while creating surface: {}", x),
            Error::Instance(ref x) => write!(fmt,"Error while creating instance: {}", x),
        }
    }
}

impl error::Error for Error{
    fn description(&self) -> &str{
        match *self{
            Error::Surface(ref x) => x.description(),
            Error::Instance(ref x) => x.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error>{
        match *self{
            Error::Surface(ref x) => Some(x),
            Error::Instance(ref x) => Some(x),
        }
    }
}

impl From<SurfaceCreationError> for Error{
    fn from(e: SurfaceCreationError) -> Self{
        Error::Surface(e)
    }
}

impl From<InstanceCreationError> for Error{
    fn from(e: InstanceCreationError) -> Self{
        Error::Instance(e)
    }
}

pub trait IntoWindow{
    type Target: Window;
    type Error: error::Error;

    fn into_window<'a,L>(self,app_name: &str,version:(u16,u16,u16),layers: L) -> Result<Self::Target,Self::Error>
        where L: IntoIterator<Item = &'a &'a str>;
}

/// A trait which represents a window in which the engine will render.
pub trait Window: Send + 'static{
    fn instance(&self) -> Arc<Instance>;

    fn surface(&self) -> Arc<Surface>;

    fn get_size(&self) -> [u32; 2];
}

fn required_extensions() -> InstanceExtensions {
    let ideal = InstanceExtensions {
        khr_surface: true,
        khr_xlib_surface: true,
        khr_xcb_surface: true,
        khr_wayland_surface: true,
        khr_mir_surface: true,
        khr_android_surface: true,
        khr_win32_surface: true,
        mvk_ios_surface: true,
        mvk_macos_surface: true,
        ..InstanceExtensions::none()
    };

    InstanceExtensions::supported_by_core()
        .expect("Could not get supported extension!")
        .intersection(&ideal)
}

impl IntoWindow for window::context::Context{
    type Target = CoreWindow;
    type Error = Error;

    fn into_window<'a,L>(self,app_name: &str,version:(u16,u16,u16),layers: L) -> Result<Self::Target,Self::Error>
        where L: IntoIterator<Item = &'a &'a str>
    {
        let app_info = ApplicationInfo{
            application_name: Some(app_name.into()),
            application_version: Some(Version{
                major: version.0,
                minor: version.1,
                patch: version.2,
            }),
            engine_name: Some("AxleEngine".into()),
            engine_version: Some(Version{
                major: env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap(), 
                minor: env!("CARGO_PKG_VERSION_MINOR").parse().unwrap(), 
                patch: env!("CARGO_PKG_VERSION_PATCH").parse().unwrap(), 
            }),
        };
        let instance = Instance::new(Some(&app_info),&required_extensions(),layers)?;
        let surface = self.create_surface(&instance)?;
        Ok(CoreWindow{
            instance,
            surface,
            context: self,
        })
    }
}

impl<T: Window> IntoWindow for T{
    type Target = T;
    type Error = core::common::NoError;

    fn into_window<'a,L>(self,_: &str,_:(u16,u16,u16),_: L) -> Result<Self::Target,Self::Error>
        where L: IntoIterator<Item = &'a &'a str>
    {
        Ok(self)
    }
}

pub struct CoreWindow{
    instance: Arc<Instance>,
    surface: Arc<Surface>,
    context: window::context::Context
}

unsafe impl Send for CoreWindow{}

impl Window for CoreWindow{

    fn instance(&self) -> Arc<Instance>{
        self.instance.clone()
    }

    fn surface(&self) -> Arc<Surface>{
        self.surface.clone()
    }

    fn get_size(&self) -> [u32; 2]{
        self.context.get_size()
    }
}
