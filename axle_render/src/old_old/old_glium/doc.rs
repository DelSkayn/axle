//!
//! # Renderengine requirements:
//!
//! - Needs to run on opengl.
//!     - Maybe switch to vulkan in a future.
//! -
//!
//!
//! # Begrippen (What is the english word again)
//!
//! - Scene
//!     
//!     The Object containing the non-static information of renderobjects used in rendering.
//!     That is the information which can change while the programm is running.
//!
//! - Pipeline
//!
//!     A object which determins how an object is rendered from recieving the initial data to 
//!     the final output. The pipeline can contain multiple STAGES,
//!
//! - Stage
//!     
//!     A single pass in an pipeline, Goes through all objects that need to be rendered, with
//!     possible some filters, and does some form of rendering. This can be forinstance a depthpass
//!     the g-buffer pass or a light pass. Contains some SHADERS and a TARGET
//!
//! - Target
//!     
//!     A Texture like object to which can be rendered.
//!
//!
