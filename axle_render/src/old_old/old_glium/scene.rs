
use core::task::sync::{double_buffer,mutate_inspect};
use core::asset::{AssetRef,Model};
use core::cgmath::*;
use core::common::color::*;

use window::Window;

use std::sync::atomic::{AtomicUsize,Ordering};
use std::sync::Arc;

/// A id allowing identification of unique renderobjects.
#[derive(Clone,Copy,Eq,PartialEq)]
pub struct RenderObjectId(u64);

/// A struct which specifies a orientation.
#[derive(Clone,Copy)]
pub struct Transform{
    pub position: Vector3<f32>,
    pub scale: f32,
    pub rotation: Quaternion<f32>,
}

impl Default for Transform{
    fn default() -> Self{
        Transform{
            position: Vector3{
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            scale: 1.0,
            rotation: Quaternion{
                s: 1.0,
                v: Vector3{
                    x: 0.0,
                    y: 0.0,
                    z: 0.0,
                }
            },
        }
    }
}

/// A render object which has a position in the scene.
#[derive(Clone)]
pub struct RenderObject{
    pub id: RenderObjectId,
    pub mesh: AssetRef<Model>,
    pub transform: Transform,
}

/// A light originating from a point.
pub struct PointLight{
    pub position: Vector3<f32>,
    pub strenght: f32,
    pub color: Rgb,
}

/// A light which has no origin but only a direction
pub struct DirectionalLight{
    pub direction: Vector3<f32>,
    pub strenght: f32,
    pub color: Rgb,
}

/// A light originating from a point with a cone of light.
pub struct SpotLight{
    pub position: Vector3<f32>,
    pub direction: Vector3<f32>,
    pub angle: Rad<f32>,
    pub strenght: f32,
    pub color: Rgb,
}

/// A struct which contains all the lights in the scene
pub struct Lights{
    pub point_lights: Vec<PointLight>,
    pub spot_lights: Vec<SpotLight>,
    pub directional_light: Vec<DirectionalLight>,
}

impl Lights{
    fn new() -> Self{
        Lights{
            point_lights: Vec::new(),
            spot_lights: Vec::new(),
            directional_light: Vec::new(),
        }
    }
}

/// A struct which specifies the ambient light.
#[derive(Clone,Copy)]
pub struct Ambient{
    pub color: Rgb,
    pub strenght: f32,
}

impl Ambient{
    /// TODO: replace with default.
    pub fn new() -> Self{
        Ambient {
            color: Rgb(1.0,1.0,1.0),
            strenght: 1.0
        }
    }
}

/// A struct which specifies the global environment of the scene.
#[derive(Clone,Copy)]
pub struct Environment{
    pub ambient: Ambient,
}

impl Environment{
    pub fn new() -> Self{
        Environment{
            ambient: Ambient::new(),
        }
    }
}

#[derive(Clone,Copy)]
pub struct Camera{
    pub transform: Transform,
    pub fov: Rad<f32>,
}

impl Default for Camera{
    fn default() -> Self{
        Camera{
            transform: Transform::default(),
            fov: Deg(90.0).into()
        }
    }
}

/// A struct which contains data about the scene.
pub struct Scene{
    objects: double_buffer::Front<Vec<RenderObject>>,
    lights: double_buffer::Front<Lights>,
    environment: mutate_inspect::Mutator<Environment>,
    camera: mutate_inspect::Mutator<Camera>,
    frame_count: Arc<AtomicUsize>,
    // TODO: find a better solution for creating identities for the render objects.
    next_object: u64,
}


impl Scene{
    pub(super) fn new() -> (Self,SceneBack){
        let (f_objects, b_objects) = double_buffer::double_buffer(Vec::new(),Vec::new());
        let (f_lights, b_lights) = double_buffer::double_buffer(Lights::new(),Lights::new());
        let (f_environment, b_environment) = mutate_inspect::mutate_inspect(Environment::new());
        let (f_camera, b_camera) = mutate_inspect::mutate_inspect(Camera::default());
        let frame_count = Arc::new(AtomicUsize::new(0));

        let scene = Scene{
            frame_count: frame_count.clone(),
            objects: f_objects,
            lights: f_lights,
            environment:
            f_environment,
            camera: f_camera,
            next_object: 0,
        };

        let back = SceneBack{
            frame_count: frame_count,
            objects: b_objects,
            lights: b_lights,
            environment: b_environment,
            camera: b_camera,
        };

        (scene,back)

    }

    /// A function called in order to draw a frame.
    /// Gives acces to the scene data and notifies the renderer to update data.
    pub fn frame<F: FnOnce(&mut SceneAccess)>(&mut self,func: F){
        {
            let mut access = SceneAccess {
                objects: &mut *self.objects,
                lights: &mut *self.lights,
                environment: &mut self.environment,
                camera: &mut self.camera,
            };
            func(&mut access);
        }
        self.lights.swap();
        self.objects.swap();
        self.frame_count.fetch_add(1,Ordering::AcqRel);
    }

    /// Creates a new render object form a mesh and a transform.
    /// This function is a tempory sollution it will probebly sucseded by a better api.
    pub fn render_object(&mut self,mesh: AssetRef<Model>,transform: Transform) -> RenderObject{
        let id = RenderObjectId(self.next_object);
        self.next_object += 1;
        RenderObject{
            id,
            mesh,
            transform,
        }
    }
}

/// A struct representing access to the scene.
pub struct SceneAccess<'a>{
    pub objects: &'a mut Vec<RenderObject>,
    pub lights: &'a mut Lights,
    camera: &'a mut mutate_inspect::Mutator<Camera>,
    environment: &'a mut mutate_inspect::Mutator<Environment>,
}

impl<'a> SceneAccess<'a>{
    pub fn set_environment(&mut self,env: Environment){
        (*self.environment.borrow_mut()) = env;
    }

    pub fn set_camera(&mut self,camera: Camera){
        (*self.camera.borrow_mut()) = camera;
    }

    pub fn get_camera(&self) -> Camera{
        *self.camera.borrow()
    }

    pub fn get_environment(&self) -> Environment{
        *self.environment.borrow()
    }
}

pub struct SceneBack{
    pub objects: double_buffer::Back<Vec<RenderObject>>,
    pub lights: double_buffer::Back<Lights>,
    pub environment: mutate_inspect::Inspector<Environment>,
    pub camera: mutate_inspect::Inspector<Camera>,
    pub frame_count: Arc<AtomicUsize>,
}
