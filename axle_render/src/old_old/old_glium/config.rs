extern crate serde;

use self::serde::{Deserializer,Deserialize};

fn deserialize_debug_mode<'a,D>(de: D) -> Result<DebugMode, D::Error>
    where D: Deserializer<'a>
{
    let s: String = Deserialize::deserialize(de)?;
    match s.as_ref() {
        "all" => Ok(DebugMode::All),
        "ignore" => Ok(DebugMode::Ignore),
        other => Err(serde::de::Error::custom(format!("unknown protocol: {}", other))),
    }
}

#[derive(Deserialize)]
pub enum DebugMode{
    All,
    Ignore,
}

impl Default for DebugMode{
    fn default() -> Self{
        DebugMode::Ignore
    }
}

#[derive(Deserialize)]
pub struct ShaderSourcePath{
    pub vs: String,
    pub fs: String,
    pub gs: Option<String>,
} 

#[derive(Deserialize)]
pub struct Shaders{
    pub depth_pass: ShaderSourcePath,
    pub g_buffer: ShaderSourcePath,
    pub directional: ShaderSourcePath,
    pub spot: ShaderSourcePath,
    pub point: ShaderSourcePath,
}

impl Default for Shaders{
    fn default() -> Self{
        Shaders{
            depth_pass: ShaderSourcePath{
                vs: "shader/depth_pass.vert".to_string(),
                fs: "shader/depth_pass.frag".to_string(),
                gs: None,
            },
            g_buffer: ShaderSourcePath{
                vs: "shader/geom.vert".to_string(),
                fs: "shader/geom.frag".to_string(),
                gs: None,
            },
            directional: ShaderSourcePath{
                vs: "shader/directional_vs.glsl".to_string(),
                fs: "shader/directional_fs.glsl".to_string(),
                gs: None,
            },
            spot: ShaderSourcePath{
                vs: "shader/spot_vs.glsl".to_string(),
                fs: "shader/spot_fs.glsl".to_string(),
                gs: None,
            },
            point: ShaderSourcePath{
                vs: "shader/point.vert".to_string(),
                fs: "shader/point.frag".to_string(),
                gs: None,
            }
        }
    }
}

#[derive(Deserialize)]
pub struct Camera{
    pub fov: f32,
    pub near: f32,
    pub far: f32,
}

impl Default for Camera {
    fn default() -> Self{
        Camera{
            fov: 90.0,
            near: 0.01,
            far: 100000.0,
        }
    }
}

#[derive(Deserialize,Default)]
#[serde(rename(deserialize = "render"))]
pub struct Config{
    #[serde(deserialize_with = "deserialize_debug_mode")]
    #[serde(default)]
    pub debug_mode: DebugMode,
    pub shaders: Shaders,
    pub camera: Camera,
}

