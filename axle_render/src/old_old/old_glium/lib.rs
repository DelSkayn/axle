//! This crate contains the renderer for the axle game engine.

#![allow(unused_imports)]
#![allow(dead_code)]
#![feature(fnbox)]

extern crate axle_core as core;
#[macro_use]
extern crate glium;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
extern crate crossbeam;

//mod scene;
mod back;
mod window;
pub mod config;
pub mod scene;

pub use scene::{Scene,RenderObject};
use glium::backend::Facade;
use crossbeam::sync::SegQueue;

use core::task::JoinHandle;

pub use self::window::{Window,IntoWindow,View};

use std::boxed::FnBox;
use std::sync::atomic::{Ordering,AtomicBool};
use std::sync::Arc;


type MessageQue<W> = SegQueue<Box<FnBox(&back::Back<W>) + 'static + Send>>;

pub struct Render{
    running: Arc<AtomicBool>,
    join: Option<JoinHandle>,
}

impl Render{
    pub fn new<A,W,I>(window: I) -> (Self,scene::Scene)
        where I: IntoWindow<Target = W> + Send + 'static,
              W: Window<Facade = A>,
              A: Facade + Clone
    {
        let running = Arc::new(AtomicBool::new(true));
        let running_clone = running.clone();
        let (front,back) = scene::Scene::new();
        let join = {
            let sync = core::task::sync();
            core::task::spawn(&sync,||{
                back::Back::new(window.into_window(config::DebugMode::All),back,running_clone).run();
            })
        };
        (Render{
            running,
            join: Some(join),
        },front)
    }
}

impl Drop for Render{
    fn drop(&mut self){
        self.running.store(false,Ordering::Release);
        self.join.take().unwrap().join();
    }
}

