
mod shader;
mod static_buffer;
mod texture;
mod fragment;
mod material;
mod pipeline;
mod blit;

use glium::backend::Facade;

use std::sync::Arc;
use std::sync::atomic::{AtomicBool,Ordering};

use window::{Window,View};
use scene::SceneBack;

use ::core::registery::{Value,Error};
use super::config::Config;

pub struct Back<W: Window + View>{
    pub pipeline: pipeline::Pipeline<W::Facade>,
    pub textures: texture::Textures<W::Facade>,
    pub materials: material::Materials<W::Facade>,
    pub static_buffers: static_buffer::StaticBuffers<W::Facade>,
    pub shaders: shader::Shaders<W::Facade>,
    pub fragments: fragment::Fragments,
    pub scene: SceneBack,
    pub running: Arc<AtomicBool>,
    pub config: Value<Config>,
    pub window: W,
}

impl<A,W> Back<W>
    where W: Window<Facade = A> + View,
          A: Facade + Clone,
{

    pub fn new(window: W,scene: SceneBack,running: Arc<AtomicBool>) -> Self{
        //TODO: handle errors.

        let config = match Value::new_or_default("render"){
            Ok(x) => x,
            Err((x,e)) => {
                match e {
                    Error::NotFound => {},
                    e => warn!("error with config for renderer: {}", e),
                }
                x
            }
        };
        let mut shaders = shader::Shaders::new(window.facade());

        let pipeline = pipeline::Pipeline::new(window.facade(),&mut shaders,&config);

        Back{
            textures: texture::Textures::new(window.facade()),
            static_buffers: static_buffer::StaticBuffers::new(window.facade()),
            fragments: fragment::Fragments::new(),
            materials: material::Materials::new(window.facade()),
            shaders,
            scene,
            running,
            config,
            window,
            pipeline,
        }
    }
}

impl<W: Window> Back<W>{
    pub fn run(mut self){
        info!("Render engine starting");
        while self.running.load(Ordering::Acquire){
            trace!("Frame start");
            {
                let frame = self.window.surface();
                self.fragments.update( &mut self.scene
                    , &mut self.static_buffers
                    , &mut self.textures
                    , &mut self.materials);
                self.pipeline.render( &self.static_buffers
                    , &self.shaders
                    , &self.materials
                    , &self.textures
                    , &self.fragments.fragments
                    , &self.scene.lights.borrow()
                    , frame
                    , self.scene.camera.get());
            }
            self.window.finish();
            trace!("Frame end");
        }
        info!("Render engine quiting");
    }
}

