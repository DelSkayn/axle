
use glium::{VertexBuffer,Program,Surface,DrawError};
use glium::backend::Facade;
use glium::uniforms::Uniforms;
use glium::draw_parameters::DrawParameters;
use glium::index::{IndexBuffer,NoIndices,PrimitiveType};

#[derive(Clone,Copy)]
pub struct RectVertex{
    position: [f32; 2],
}

implement_vertex!(RectVertex,position);

const RECT_VERTECIES: &'static [RectVertex; 6] = 
    &[ RectVertex { position: [ 1.0, 1.0] }
     , RectVertex { position: [-1.0, 1.0] }
     , RectVertex { position: [-1.0,-1.0] }
                                         
     , RectVertex { position: [ 1.0, 1.0] }
     , RectVertex { position: [-1.0,-1.0] }
     , RectVertex { position: [ 1.0,-1.0] } ];

#[derive(Clone,Copy)]
pub struct CubeVertex{
    position: [f32; 3],
}

implement_vertex!(CubeVertex,position);

const CUBE_VERTECIES: &'static [CubeVertex; 8] = 

    &[ CubeVertex { position: [-1.0,-1.0, 1.0] }
     , CubeVertex { position: [ 1.0,-1.0, 1.0] }
     , CubeVertex { position: [-1.0, 1.0, 1.0] }
     , CubeVertex { position: [ 1.0, 1.0, 1.0] }

     , CubeVertex { position: [-1.0,-1.0,-1.0] }
     , CubeVertex { position: [ 1.0,-1.0,-1.0] }
     , CubeVertex { position: [-1.0, 1.0,-1.0] }
     , CubeVertex { position: [ 1.0, 1.0,-1.0] } ];

const CUBE_INDECIES: &'static [u16; 36] =
    &[ 1, 2, 3
     , 3, 2, 4

     , 3, 4, 5
     , 5, 4, 6

     , 5, 6, 7
     , 7, 6, 8

     , 7, 8, 1
     , 1, 8, 2

     , 2, 8, 4
     , 4, 8, 6

     , 7, 1, 5
     , 5, 1, 3 ];




pub struct BlitRect{
    vertex: VertexBuffer<RectVertex>,
}

impl BlitRect{
    pub fn new<F: Facade>(facade: &F) -> Self{
        BlitRect{
            vertex: VertexBuffer::immutable(facade,RECT_VERTECIES).unwrap(),
        }
    }

    pub fn draw<S: Surface, U: Uniforms>(&self
       , surface: &mut S
       , program: &Program
       , uniform: &U
       , draw_parameters: &DrawParameters) -> Result<(), DrawError>
    {
        surface.draw(&self.vertex,NoIndices(PrimitiveType::TrianglesList),program,uniform,draw_parameters)
    }
}

pub struct BlitCube{
    vertex: VertexBuffer<CubeVertex>,
    index: IndexBuffer<u16>,
}

impl BlitCube{
    pub fn new<F: Facade>(facade: &F) -> Self{
        BlitCube{
            vertex: VertexBuffer::immutable(facade,CUBE_VERTECIES).unwrap(),
            index: IndexBuffer::immutable(facade,PrimitiveType::TrianglesList,CUBE_INDECIES).unwrap(),
        }
    }

    pub fn draw<S: Surface, U: Uniforms>(&self
       , surface: &mut S
       , program: &Program
       , uniform: &U
       , draw_parameters: &DrawParameters) -> Result<(), DrawError>
    {
        surface.draw(&self.vertex,&self.index,program,uniform,draw_parameters)
    }
}
