
use ::core::common::linked_vec::LinkedVec;
use ::core::asset;

use ::glium::{backend,Program};
use ::glium::program::ProgramCreationError;

use std::{ops,borrow};
use std::collections::HashMap;
use std::mem;

pub struct Shader{
    pub vertex: asset::AssetRef<asset::Text>,
    pub fragment: asset::AssetRef<asset::Text>,
}

//Can be u32 because no graphics drivers is able to handle more than u32::MAX programs.
#[derive(Debug,Eq,PartialEq,Hash)]
pub struct ShaderId(u32);

impl Drop for ShaderId{
    fn drop(&mut self){
        warn!("Droped shader id instead of removing it properly");
    }
}

pub struct Shaders<F: backend::Facade>{
    loaded: HashMap<(asset::AssetId,asset::AssetId),ShaderId>,
    shaders: LinkedVec<(usize,Program)>,
    context: F,
}

impl<F: backend::Facade> Shaders<F>{
    pub fn new(context: F) -> Self{
        Shaders{
            loaded: HashMap::new(),
            shaders: LinkedVec::new(),
            context,
        }
    }

    pub fn register(&mut self,shader: Shader) -> ShaderId{
        if let Some(x) = self.loaded.get_mut(&(shader.vertex.id(),shader.fragment.id())){
            x.0 += 1;
            return ShaderId(x.0);
        }
        let vertex = shader.vertex.borrow().unwrap();
        let fragment = shader.fragment.borrow().unwrap();
        let program = match Program::from_source(&self.context,&vertex.text,&fragment.text,None){
            Ok(x) => x,
            Err(ProgramCreationError::CompilationError(e)) => {
                error!("Error while trying to compile shader:\n{}\n", e);
                panic!();
            },
            Err(ProgramCreationError::LinkingError(e)) => {
                error!("Error while trying to link shader:\n{}\n", e);
                panic!();
            },
            _ => {
                panic!();
            }
        };
        let id = self.shaders.insert((1,program));
        let shader_id = ShaderId(id as u32);
        self.loaded.insert((shader.vertex.id(),shader.fragment.id()),ShaderId(id as u32));
        return shader_id;
    }

    pub fn cleanup(&mut self){
        let full_len = self.shaders.len() + self.shaders.free();
        for i in 0..full_len{
            if let Some(not_used) = self.shaders.try_get(i).map(|e| e.0 == 0){
                if not_used {
                    self.shaders.remove(i);
                }
            }
        }
        self.shaders.clean();
    }

    pub fn increment(&mut self,id: ShaderId) -> ShaderId{
        self.shaders[id.0 as usize].0 += 1;
        ShaderId(id.0)
    }

    pub fn remove(&mut self,id: ShaderId){
        self.shaders[id.0 as usize].0 -= 1;
        mem::forget(id);
    }
}

impl<'a,F: backend::Facade> ops::Index<&'a ShaderId> for Shaders<F>{
    type Output = Program;

    fn index(&self,idx: &ShaderId) -> &Program{
        &self.shaders.get(idx.0 as usize).1
    }
}
