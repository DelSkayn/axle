use super::super::glium;


use glium::{texture,backend};

use std::{ops,borrow};
use ::core::common::linked_vec::LinkedVec;
use ::core::asset;

use std::collections::HashMap;
use std::rc::Rc;
use std::mem;


//Can be u32 because no graphics drivers is able to handle more than u32::MAX index or vertex buffers.
#[derive(Debug,Eq,PartialEq,Hash)]
pub struct TextureId(u32);

impl Drop for TextureId{
    fn drop(&mut self){
        warn!("Droped texture id instead of removing it properly");
    }
}

pub struct TextureData{
    referenced: usize,
    texture: texture::Texture2d,
}

pub struct Textures<F: backend::Facade>{
    loaded: HashMap<asset::AssetId,TextureId>,
    buffers: LinkedVec<TextureData>,
    context: F,
}

impl<F: backend::Facade> Textures<F>{
    pub fn new(context: F) -> Self{
        Textures{
            loaded: HashMap::new(),
            buffers: LinkedVec::new(),
            context: context
        }
    }

    pub fn register(&mut self,texture: &asset::AssetRef<asset::Texture>) -> TextureId{
        // I dont like the need to pass the assetref only because we need a way 
        // to identify identical vertex buffers. 
        if let Some(x) = self.loaded.get(&texture.id()){
            self.buffers[x.0 as usize].referenced += 1;
            return TextureId(x.0);
        }
        let borrow = texture.borrow().unwrap();

        let width = borrow.width;
        let height = borrow.height;
        let format = match borrow.channel {
            asset::Channel::Rgba => texture::ClientFormat::U8U8U8U8,
            asset::Channel::Rgb => texture::ClientFormat::U8U8U8,
            asset::Channel::Gray => texture::ClientFormat::U8,
        };

        let data: borrow::Cow<[_]> = borrow::Cow::Borrowed(&borrow.data);

        let raw = texture::RawImage2d{
            data,
            width,
            height,
            format,
        };

        let text = texture::Texture2d::new( &self.context
                                             , raw ).unwrap();

        let idx_idx = self.buffers.insert(TextureData{
            referenced: 1,
            texture: text,
        });

        let id = TextureId(idx_idx as u32);
        self.loaded.insert(texture.id(),TextureId(id.0));
        id
    }

    pub fn remove(&mut self,id: TextureId){
        self.buffers[id.0 as usize].referenced -= 1;
        mem::forget(id);
    }

    pub fn cleanup(&mut self){
        let full_len = self.buffers.len() + self.buffers.free();
        for i in 0..full_len{
            if let Some(not_used) = self.buffers.try_get(i).map(|e| e.referenced == 0){
                if not_used {
                    self.buffers.remove(i);
                }
            }
        }
        self.buffers.clean();
    }

    pub fn amount(&self) -> usize{
        self.buffers.len()
    }
}

impl<'a, F: backend::Facade> ops::Index<&'a TextureId> for Textures<F>{
    type Output = texture::Texture2d;

    fn index(&self,idx: &TextureId) -> &texture::Texture2d{
        &self.buffers.get(idx.0 as usize).texture
    }
}


