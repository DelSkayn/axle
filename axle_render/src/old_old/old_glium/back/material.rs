

use ::glium::{index,vertex,backend};

use super::texture::{TextureId,Textures};

use std::{ops,borrow};
use ::core::common::linked_vec::LinkedVec;
use ::core::asset;
use ::core::common::color::Rgb;

use std::collections::HashMap;
use std::rc::Rc;
use std::mem;

#[derive(Debug)]
pub struct Material{
    pub diffuse: Rgb,
    pub metalness: f32,
    pub roughness: f32,
    pub diffuse_map: Option<TextureId>,
    pub roughness_map: Option<TextureId>,
    pub normal_map: Option<TextureId>,
}



//Can be u32 because no graphics drivers is able to handle more than u32::MAX index or vertex buffers.
#[derive(Debug,Eq,PartialEq,Hash)]
pub struct MaterialId(u32);

impl Drop for MaterialId{
    fn drop(&mut self){
        warn!("Droped material id instead of removing it properly");
    }
}

pub struct Materials<F: backend::Facade>{
    loaded: HashMap<asset::AssetId,MaterialId>,
    materials: LinkedVec<(usize,Material)>,
    context: F,
}

impl<F: backend::Facade> Materials<F>{
    pub fn new(context: F) -> Self{
        Materials{
            loaded: HashMap::new(),
            materials: LinkedVec::new(),
            context: context
        }
    }

    pub fn register(&mut self,textures: &mut Textures<F>,material: &asset::AssetRef<asset::Material>) -> MaterialId{

        if let Some(x) = self.loaded.get(&material.id()){
            self.materials[x.0 as usize].0 += 1;
            return MaterialId(x.0);
        }

        let m = material.borrow().unwrap();

        let mat = Material{
            diffuse: Rgb(
                m.diffuse[0],
                m.diffuse[1],
                m.diffuse[2],
            ),
            metalness: m.metalness,
            roughness: m.roughness,
            diffuse_map: m.diffuse_map.as_ref().map(|e| textures.register(&e)),
            roughness_map: m.roughness_map.as_ref().map(|e| textures.register(&e)),
            normal_map: m.normal_map.as_ref().map(|e| textures.register(&e)),
        };

        let idx_idx = self.materials.insert((1,mat));
        let id = MaterialId(idx_idx as u32);
        self.loaded.insert(material.id(),MaterialId(idx_idx as u32));
        id
    }

    pub fn increment(&mut self,id: &MaterialId) -> MaterialId{
        self.materials[id.0 as usize].0 += 1;
        MaterialId(id.0)
    }

    pub fn remove(&mut self,id: MaterialId){
        self.materials[id.0 as usize].0 -= 1;
        mem::forget(id);
    }

    pub fn cleanup(&mut self,textures: &mut Textures<F>){
        let full_len = self.materials.len() + self.materials.free();
        for i in 0..full_len{
            if let Some(not_used) = self.materials.try_get(i).map(|e| e.0 == 0){
                if not_used {
                    let mat = self.materials.remove(i);
                    mat.1.diffuse_map.map(|e| textures.remove(e));
                    mat.1.roughness_map.map(|e| textures.remove(e));
                    mat.1.normal_map.map(|e| textures.remove(e));
                }
            }
        }
        self.materials.clean();
    }

    pub fn amount(&self) -> usize{
        self.materials.len()
    }
}

impl<'a,F: backend::Facade> ops::Index<&'a MaterialId> for Materials<F>{
    type Output = Material;

    fn index(&self,idx: &MaterialId) -> &Material{
        &self.materials.get(idx.0 as usize).1
    }
}


