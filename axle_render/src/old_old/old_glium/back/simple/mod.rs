//!
//! This module implements a simple render for testing.
//! The renderer in this module is not meant to be used in a game but is created to help testing.
//!

pub struct SimplePipeline{
    mesh_shader: Shader,
}
