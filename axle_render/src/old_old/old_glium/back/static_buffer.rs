use super::super::glium;


use glium::{index, vertex, backend};

use std::{ops, borrow};
use core::common::linked_vec::LinkedVec;
use core::asset;

use std::collections::HashMap;
use std::rc::Rc;
use std::mem;

#[derive(Debug,Clone,Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub texture_coord: [f32; 2],
}

implement_vertex!(Vertex, position, normal, texture_coord);


//Can be u32 because no graphics drivers is able to handle more than u32::MAX index or vertex buffers.
#[derive(Debug,Eq,PartialEq,Hash)]
pub struct StaticBufferId(u32);

impl Drop for StaticBufferId {
    fn drop(&mut self) {
        warn!("Droped static buffer id instead of removing it properly");
    }
}

pub struct StaticBufferData {
    referenced: usize,
    pub vertex: vertex::VertexBuffer<Vertex>,
    pub index: index::IndexBuffer<u32>,
}

pub struct StaticBuffers<F: backend::Facade> {
    loaded: HashMap<asset::AssetId, StaticBufferId>,
    buffers: LinkedVec<StaticBufferData>,
    context: F,
}

impl<F: backend::Facade> StaticBuffers<F> {
    pub fn new(context: F) -> Self {
        StaticBuffers {
            loaded: HashMap::new(),
            buffers: LinkedVec::new(),
            context: context,
        }
    }

    pub fn register(&mut self, model: &asset::AssetRef<asset::Model>) -> StaticBufferId {

        if let Some(x) = self.loaded.get(&model.id()) {
            self.buffers[x.0 as usize].referenced += 1;
            return StaticBufferId(x.0);
        }

        let m = model.borrow().unwrap();

        let vertecies: Vec<_> = m.vertices
            .iter()
            .zip(m.normals.iter())
            .zip(m.texture_coords.iter())
            .map(|((v, n), c)| {
                Vertex {
                    position: *v,
                    normal: *n,
                    texture_coord: *c,
                }
            })
            .collect();

        let vertex = vertex::VertexBuffer::new(&self.context, &vertecies).unwrap();

        let index = index::IndexBuffer::new(&self.context,
                                            index::PrimitiveType::TrianglesList,
                                            &m.indecies)
                .unwrap();

        let idx_idx = self.buffers
            .insert(StaticBufferData {
                        referenced: 1,
                        vertex: vertex,
                        index: index,
                    });
        let id = StaticBufferId(idx_idx as u32);
        self.loaded
            .insert(model.id(), StaticBufferId(idx_idx as u32));
        id
    }

    pub fn increment(&mut self, id: &StaticBufferId) -> StaticBufferId {
        self.buffers[id.0 as usize].referenced += 1;
        StaticBufferId(id.0)
    }

    pub fn remove(&mut self, id: StaticBufferId) {
        self.buffers[id.0 as usize].referenced -= 1;
        mem::forget(id);
    }

    pub fn cleanup(&mut self) {
        let full_len = self.buffers.len() + self.buffers.free();
        for i in 0..full_len {
            if let Some(not_used) = self.buffers.try_get(i).map(|e| e.referenced == 0) {
                if not_used {
                    self.buffers.remove(i);
                }
            }
        }
        self.buffers.clean();
    }

    pub fn amount(&self) -> usize {
        self.buffers.len()
    }
}

impl<'a, F: backend::Facade> ops::Index<&'a StaticBufferId> for StaticBuffers<F> {
    type Output = StaticBufferData;

    fn index(&self, idx: &StaticBufferId) -> &StaticBufferData {
        &self.buffers.get(idx.0 as usize)
    }
}
