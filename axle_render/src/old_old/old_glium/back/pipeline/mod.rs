

use super::static_buffer::StaticBuffers;
use super::material::Materials;
use super::fragment::{Fragments,Fragment};
use super::texture::Textures;
use super::shader::{Shader,Shaders,ShaderId};

use ::scene::{Camera,Lights};

use ::config::Config;

use ::glium::backend::Facade;
use ::glium::Surface;

use core::asset::Assets; 

mod depth_pass;
mod g_buffer;
mod light_pass;

use self::depth_pass::DepthPass;
use self::g_buffer::GBuffer;
use self::light_pass::LightPass;

pub struct Pipeline<F: Facade>{
    quick_depth: DepthPass<F>,
    g_buffer: GBuffer<F>,
    light_pass: LightPass<F>,
}

impl<F: Facade + Clone> Pipeline<F>{

    pub fn new(facade: F,shaders: &mut Shaders<F>,config: &Config) -> Self{
        let dims = facade.get_context().get_framebuffer_dimensions();

        let depth_shader_source = Shader{
            vertex: Assets::load(&config.shaders.depth_pass.vs),
            fragment: Assets::load(&config.shaders.depth_pass.fs),
        };

        let g_buffer_source = Shader{
            vertex: Assets::load(&config.shaders.g_buffer.vs),
            fragment: Assets::load(&config.shaders.g_buffer.fs),
        };

        let point_source = Shader{
            vertex: Assets::load(&config.shaders.point.vs),
            fragment: Assets::load(&config.shaders.point.fs),
        };

        /*
        let directional_source = Shader{
            vertex: Assets::load(&config.shaders.directional.vs),
            fragment: Assets::load(&config.shaders.directional.fs),
        };

        let spot_source = Shader{
            vertex: Assets::load(&config.shaders.spot.vs),
            fragment: Assets::load(&config.shaders.spot.fs),
        };
        */

        depth_shader_source.fragment.wait();
        depth_shader_source.vertex.wait();
        g_buffer_source.fragment.wait();
        g_buffer_source.vertex.wait();
        //directional_source.fragment.wait();
        //directional_source.vertex.wait();
        point_source.fragment.wait();
        point_source.vertex.wait();
        //spot_source.fragment.wait();
        //spot_source.vertex.wait();

        Pipeline{
            quick_depth: DepthPass::new( facade.clone() 
                , shaders.register(depth_shader_source)
                , dims),
            g_buffer: GBuffer::new( facade.clone()
                , shaders.register(g_buffer_source)
                , dims),
            light_pass: LightPass::new( facade.clone()
                //, shaders.register(directional_source)
                , shaders.register(point_source)
                //, shaders.register(spot_source) 
                ),
        }
    }
}

impl<F: Facade> Pipeline<F>{

    pub fn render<S: Surface>( &mut self
             , static_buffers: &StaticBuffers<F>
             , shaders: &Shaders<F>
             , materials: &Materials<F>
             , textures: &Textures<F>
             , fragments: &[Fragment]
             , light: &Lights
             , out: &mut S
             , camera: Camera)
    {
        self.quick_depth.run( fragments 
            , static_buffers
            , shaders
            , camera);

        self.g_buffer.run( fragments
            , static_buffers
            , shaders
            , materials
            , textures
            , camera
            , &self.quick_depth);

        self.light_pass.run( light
            , shaders
            , &self.g_buffer
            , out
            , camera)


    }
}
