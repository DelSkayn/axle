
use glium::backend::Facade;
use glium::Surface;

use super::super::shader::{Shaders,ShaderId};
use super::super::static_buffer::{StaticBuffers};
use super::super::fragment::Fragment;

use super::super::blit::BlitRect;

use super::g_buffer::GBuffer;

use ::scene::{Lights,Camera};

use ::core::cgmath::prelude::*;
use ::core::cgmath::conv::*;
use ::core::cgmath::{PerspectiveFov,Decomposed,Deg,Matrix4};

pub struct LightPass<F: Facade>{
    blit_rect: BlitRect,
    //directional: ShaderId,
    point: ShaderId,
    //spot: ShaderId,
    facade: F,
}

impl<F: Facade> LightPass<F>{
    pub fn new( facade: F 
              //, directional: ShaderId
              , point: ShaderId
              //, spot: ShaderId 
              ) -> Self
    {
        LightPass{
            blit_rect: BlitRect::new(&facade),
            //directional,
            point,
            //spot,
            facade,
        }
    }

    pub fn run<S: Surface>( &mut self
             , lights: &Lights
             , shaders: &Shaders<F>
             , g_buffer: &GBuffer<F>
             , out: &mut S
             , camera: Camera )
    {
        for light in lights.point_lights.iter(){
            let (w,h) = out.get_dimensions();

            let view: Matrix4<f32> = Decomposed{
                scale: 1.0,
                rot: camera.transform.rotation,
                disp: camera.transform.position,
            }.into();

            let perspective: Matrix4<f32> = PerspectiveFov{
                fovy: camera.fov,
                aspect: (w as f32 / h as f32),
                near: 0.001,
                far: 10000.0,
            }.into();

            let uniform = uniform!{
                position: g_buffer.position(),
                normal: g_buffer.normals(),
                diffuse: g_buffer.diffuse(),
                view: array4x4(view),
                view_position: array3(camera.transform.position),
                perspective: array4x4(perspective),
                light_position: array3(light.position),
                light_strenght: light.strenght,
                light_color: (light.color.0,light.color.1,light.color.2),
            };

            self.blit_rect.draw(out,&shaders[&self.point],&uniform,&Default::default()).unwrap();
        }
    }
}

