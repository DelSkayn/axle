

use ::glium::texture::DepthTexture2d;
use ::glium::program::Program;
use ::glium::framebuffer::{SimpleFrameBuffer,RenderBuffer};
use ::glium::draw_parameters::{DrawParameters,BackfaceCullingMode};
use ::glium::texture::UncompressedFloatFormat;
use ::glium::{Depth,DepthTest};
use ::glium::backend::Facade;
use ::glium::Surface;

use super::super::shader::{Shaders,ShaderId};
use super::super::static_buffer::{StaticBuffers};
use super::super::fragment::Fragment;

use ::scene::Camera;

use ::core::cgmath::prelude::*;
use ::core::cgmath::conv::*;
use ::core::cgmath::{PerspectiveFov,Decomposed,Deg,Matrix4};

pub struct DepthPass<F: Facade>{
    texture: DepthTexture2d,
    null_buffer: RenderBuffer,
    facade: F,
    shader: ShaderId,
}

impl<F: Facade> DepthPass<F>{
    pub fn new(f: F,shader: ShaderId,size: (u32,u32))-> Self{
        DepthPass{
            //TODO: Handle error
            texture: DepthTexture2d::empty(&f,size.0,size.1).unwrap(),
            null_buffer: RenderBuffer::new(&f,UncompressedFloatFormat::U8,size.0,size.1).unwrap(),
            facade: f,
            shader: shader,
        }
    }

    pub fn run(&self, fragments: &[Fragment], static_buffers: &StaticBuffers<F>, shaders: &Shaders<F>,camera: Camera){
        let mut frame_buffer = SimpleFrameBuffer::with_depth_buffer(&self.facade, &self.null_buffer, &self.texture).unwrap();
        frame_buffer.clear_depth(-1.0);

        let (w,h) = frame_buffer.get_dimensions();

        let view: Matrix4<f32> = Decomposed{
            scale: 1.0,
            rot: camera.transform.rotation,
            disp: camera.transform.position,
        }.into();

        let perspective: Matrix4<f32> = PerspectiveFov{
            fovy: camera.fov,
            aspect: (w as f32 / h as f32),
            near: 0.001,
            far: 10000.0,
        }.into();

        for fragment in fragments.iter(){

            let model: Matrix4<f32> = Decomposed{
                scale: 1.0,
                rot: fragment.transform.rotation,
                disp: fragment.transform.position,
            }.into();


            let uniform = uniform!{
                view: array4x4(view),
                perspective:  array4x4(perspective),
                model:  array4x4(model),
            };

            let depth = Depth{
                test: DepthTest::IfLess,
                write: true,
                .. Default::default()
            };

            let parameters = DrawParameters{
                depth: depth,
                color_mask: (false,false,false,false),
                backface_culling: BackfaceCullingMode::CullClockwise,
                .. Default::default()
            };

            let static_buffer = &static_buffers[&fragment.static_buffer];
            frame_buffer.draw( &static_buffer.vertex
                , &static_buffer.index
                , &shaders[&self.shader]
                , &uniform
                , &parameters).unwrap();
        }
    }

    pub fn texture(&self) -> &DepthTexture2d{
        &self.texture
    }
}

