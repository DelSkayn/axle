
use glium::texture::{Texture2d,UncompressedFloatFormat,MipmapsOption};
use glium::framebuffer::MultiOutputFrameBuffer;
use glium::draw_parameters::{DrawParameters,Depth,DepthTest,BackfaceCullingMode};
use glium::backend::Facade;
use glium::Surface;

use super::super::shader::{Shaders,ShaderId};
use super::super::static_buffer::StaticBuffers;
use super::super::material::Materials;
use super::super::texture::Textures;
use super::super::fragment::Fragment;

use super::depth_pass::DepthPass;

use ::core::cgmath::prelude::*;
use ::core::cgmath::conv::*;
use ::core::cgmath::{PerspectiveFov,Decomposed,Deg,Matrix4};

use ::scene::Camera;
/*
pub trait GLayout{
    type Output: GBuf;

    fn into_buffer<F: Facade>(self,facade: F) -> Self::Output;
} 

pub trait GBuf<T>{
    type Attachment: ToColorAttachment,
    type Output: IntoIterator<Item = (&str,Self::Attachment)>,

    fn as_buffer(&self) -> Output;
} */

pub struct GBuffer<F: Facade>{
    facade: F,
    normals: Texture2d,
    position: Texture2d,
    diffuse: Texture2d,
    shader: ShaderId,
}

impl<F: Facade> GBuffer<F>{

    pub fn new(facade: F,shader: ShaderId,dims: (u32,u32)) -> Self{
        //TODO proper g_buffer format
        let normals = Texture2d::empty_with_format(&facade
            , UncompressedFloatFormat::F16F16F16F16
            , MipmapsOption::NoMipmap
            , dims.0
            , dims.1).unwrap();
        let position = Texture2d::empty_with_format(&facade
            , UncompressedFloatFormat::F16F16F16F16
            , MipmapsOption::NoMipmap
            , dims.0
            , dims.1).unwrap();
        let diffuse = Texture2d::empty_with_format(&facade
            , UncompressedFloatFormat::F16F16F16F16
            , MipmapsOption::NoMipmap
            , dims.0
            , dims.1).unwrap();

        GBuffer{
            facade,
            normals,
            position,
            diffuse,
            shader,
        }
    }

    pub fn run( &self 
              , fragments: &[Fragment]
              , static_buffers: &StaticBuffers<F>
              , shaders: &Shaders<F>
              , material: &Materials<F>
              , _textures: &Textures<F>
              , camera: Camera
              , depth_pass: &DepthPass<F>){

        let g_buffer = 
            &[ ("position",&self.position)
            , ("normal",&self.normals)
            , ("diffuse",&self.diffuse) ];

        let mut frame_buffer = 
            MultiOutputFrameBuffer::with_depth_buffer( &self.facade 
                                                     , g_buffer.iter().cloned()
                                                     , depth_pass.texture())
            .unwrap();


        let (w,h) = frame_buffer.get_dimensions();

        let view: Matrix4<f32> = Decomposed{
            scale: 1.0,
            rot: camera.transform.rotation,
            disp: camera.transform.position,
        }.into();

        let perspective: Matrix4<f32> = PerspectiveFov{
            fovy: camera.fov,
            aspect: (w as f32 / h as f32),
            near: 0.001,
            far: 10000.0,
        }.into();

        for fragment in fragments.iter(){

            let model: Matrix4<f32> = Decomposed{
                scale: 1.0,
                rot: fragment.transform.rotation,
                disp: fragment.transform.position,
            }.into();

            let material = &material[&fragment.material];

            let uniform = uniform!{
                view: array4x4(view),
                perspective: array4x4(perspective),
                model: array4x4(model),
                roughness: material.roughness,
                metalness: material.metalness,
                diffuse: Into::<(f32,f32,f32)>::into(material.diffuse),
            };

            let depth = Depth{
                test: DepthTest::IfEqual,
                write: false,
                .. Default::default()
            };

            let parameters = DrawParameters{
                depth: depth,
                backface_culling: BackfaceCullingMode::CullClockwise,
                .. Default::default()
            };

            let static_buffer = &static_buffers[&fragment.static_buffer];
            frame_buffer.draw(&static_buffer.vertex
                , &static_buffer.index
                , &shaders[&self.shader]
                , &uniform
                , &parameters).unwrap();
        }
    }

    pub fn diffuse(&self) -> &Texture2d{
        &self.diffuse
    }

    pub fn normals(&self) -> &Texture2d{
        &self.normals
    }

    pub fn position(&self) -> &Texture2d{
        &self.position
    }
}
