use super::core::window::context::{Context as CoreContext};
use super::core::{self,window};
use super::glium::{self,backend};
use self::glium::debug::{self,DebugCallbackBehavior};
use std::rc::Rc;
use std::sync::Arc;

use super::config::DebugMode;

// A view is a part of the window, The whole window or a texture or a part of the window,
// in which it is possible to render.
pub trait View{
    type Surface: glium::Surface;

    fn surface(&mut self) -> &mut Self::Surface;

    fn dimensions(&self) -> [u32; 2];

    fn finish(&mut self);
}

pub trait IntoWindow{
    type Target: Window;

    fn into_window(self,debug_mode: DebugMode) -> Self::Target;
}

/// A trait which represents a window in which the engine will render.
pub trait Window: View + Send + 'static{
    type Backend: glium::backend::Backend;
    type Facade: glium::backend::Facade;

    fn backend(&self) -> &Self::Backend;

    fn facade(&self) -> Self::Facade;
}

impl<'a> IntoWindow for window::context::OpenGlContext{
    type Target = CoreWindow;

    fn into_window(self,debug_mode: DebugMode) -> CoreWindow{
        let debug_behavior = match debug_mode{
            DebugMode::Ignore => DebugCallbackBehavior::Ignore,
            DebugMode::All => DebugCallbackBehavior::Custom{
                callback: Box::new(|_source,_ty,_severity,_ident,_handeld,message|{
                    warn!("[opengl]{}",message);
                }),
                synchronous: true,
            }
        };

        let context = unsafe{
            glium::backend::Context::new::<_,()>(self.clone(), true, debug_behavior).unwrap()
        };
        CoreWindow{
            context: context,
            backend: self,
            surface: None,
        }
    }
}

impl<T: Window> IntoWindow for T{
    type Target = T;

    fn into_window(self,_: DebugMode) -> T{
        self
    }
}

pub struct CoreWindow{
    context: Rc<glium::backend::Context>,
    backend: window::context::OpenGlContext,
    surface: Option<glium::Frame>,
}

unsafe impl Send for CoreWindow{}

impl View for CoreWindow{
    type Surface = glium::Frame;

    fn surface(&mut self) -> &mut Self::Surface{
        if self.surface.is_none(){
            let dimensions = self.backend.get_size();
            self.surface = Some(glium::Frame::new(self.context.clone(),(dimensions[0],dimensions[1])));
        }
        self.surface.as_mut().unwrap()
    }

    fn finish(&mut self){
        self.surface.take();
    }

    fn dimensions(&self) -> [u32; 2]{
        self.backend.get_size()
    }
}

impl Window for CoreWindow{
    type Backend = window::context::OpenGlContext;
    type Facade = Rc<glium::backend::Context>;

    fn backend(&self) -> &Self::Backend{
        &self.backend
    }

    fn facade(&self) -> Self::Facade{
        self.context.clone()
    }
}
