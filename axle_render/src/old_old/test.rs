
trait Material{

    fn albedo(&self) -> Rgb;

    fn roughness(&self) -> f32;

    //etc..
}

trait Fragment{
    type Mat: Material;

    fn index_range(&self) -> (u32,u32);

    fn material(&self) -> &Self::Mat;
}

trait Model{
    type Frag: Fragment;

    fn vertecies(&self) -> &[Vertex];

    fn indecies(&self) -> &[usize];

    fn fragments(&self) -> &[Self::Frag];
}

struct RenderObject<M: Model>{
    model: M,
    transform: Transform,
}
