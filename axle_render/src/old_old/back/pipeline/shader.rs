
pub struct Shader{
}

pub struct ProgramBuilder{
    shader: Vec<Shader>,
}

pub enum Error{
    DuplicatePipelineStage,
}

impl ProgramBuilder{
    pub fn new() -> Self{
        ProgramBuilder{
            shader: Vec::new(),
        }
    }

    pub link(self,shader: Shader) -> Result<Self,Error>{
        unimplemented!()
    }

    pub build(self) -> Result<Program,Error>{
        unimplemented!()
    }
}

pub struct Program<F,V,G>
{
}
