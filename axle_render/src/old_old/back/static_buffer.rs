use vulkano::{buffer,command_buffer,sync,device};
use self::sync::GpuFuture;

use std::{ops, borrow};
use core::common::linked_vec::LinkedVec; use core::asset;

use std::collections::HashMap;
use std::sync::Arc;
use std::mem;

#[derive(Debug,Clone,Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub normal: [f32; 3],
    pub texture_coord: [f32; 2],
}

impl_vertex!(Vertex, position, normal, texture_coord);

type SingleStaticBufferFuture = command_buffer::CommandBufferExecFuture<sync::NowFuture, command_buffer::AutoCommandBuffer>;

pub type StaticBufferFuture = sync::JoinFuture<SingleStaticBufferFuture,SingleStaticBufferFuture>;

//Can be u32 because no graphics drivers is able to handle more than u32::MAX index or vertex buffers.
#[derive(Debug,Eq,PartialEq,Hash)]
pub struct StaticBufferId(u32);

impl Drop for StaticBufferId {
    fn drop(&mut self) {
        warn!("Droped static buffer id instead of removing it properly");
    }
}

pub struct StaticBufferData {
    referenced: usize,
    pub vertex: Arc<buffer::ImmutableBuffer<[Vertex]>>,
    pub index: Arc<buffer::ImmutableBuffer<[u32]>>,
    pub waiting: Option<StaticBufferFuture>,
}

pub struct StaticBuffers{
    loaded: HashMap<asset::AssetId, StaticBufferId>,
    buffers: LinkedVec<StaticBufferData>,
    queue: Arc<device::Queue>,
}

impl StaticBuffers{
    pub fn new(queue: Arc<device::Queue>) -> Self {
        StaticBuffers {
            loaded: HashMap::new(),
            buffers: LinkedVec::new(),
            queue,
        }
    }

    pub fn register(&mut self, model: &asset::AssetRef<asset::Model>) -> StaticBufferId {

        if let Some(x) = self.loaded.get(&model.id()) {
            self.buffers[x.0 as usize].referenced += 1;
            return StaticBufferId(x.0);
        }

        let m = model.borrow().unwrap();

        let vertecies = m.vertices
            .iter()
            .zip(m.normals.iter())
            .zip(m.texture_coords.iter())
            .map(|((v, n), c)| {
                Vertex {
                    position: *v,
                    normal: *n,
                    texture_coord: *c,
                }
            });

        

        let (vertex,vertex_future) = buffer::ImmutableBuffer::from_iter(vertecies
            , buffer::BufferUsage::vertex_buffer()
            , self.queue.clone() ).unwrap();

        let (index,index_future) = buffer::ImmutableBuffer::from_iter(m.indecies.iter().cloned()
            , buffer::BufferUsage::index_buffer()
            , self.queue.clone() ).unwrap();

        let full_future = vertex_future.join(index_future);


        let idx_idx = self.buffers
            .insert(StaticBufferData {
                        referenced: 1,
                        vertex: vertex,
                        index: index,
                        waiting: Some(full_future),
                    });
        let id = StaticBufferId(idx_idx as u32);
        self.loaded
            .insert(model.id(), StaticBufferId(idx_idx as u32));
        id
    }



    pub fn increment(&mut self, id: &StaticBufferId) -> StaticBufferId {
        self.buffers[id.0 as usize].referenced += 1;
        StaticBufferId(id.0)
    }

    pub fn remove(&mut self, id: StaticBufferId) {
        self.buffers[id.0 as usize].referenced -= 1;
        mem::forget(id);
    }

    pub fn cleanup(&mut self) {
        let full_len = self.buffers.len() + self.buffers.free();
        for i in 0..full_len {
            if let Some(not_used) = self.buffers.try_get(i).map(|e| e.referenced == 0) {
                if not_used {
                    self.buffers.remove(i);
                }
            }
        }
        self.buffers.clean();
    }

    pub fn amount(&self) -> usize {
        self.buffers.len()
    }
}

impl<'a> ops::Index<&'a StaticBufferId> for StaticBuffers {
    type Output = StaticBufferData;

    fn index(&self, idx: &StaticBufferId) -> &StaticBufferData {
        &self.buffers.get(idx.0 as usize)
    }
}
