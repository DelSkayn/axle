
use vulkano::instance::Instance;
use vulkano::image::ImageUsage;
use vulkano::device::Device;
use vulkano::swapchain::{Surface
    , Swapchain
    , SwapchainImage
    , SwapchainCreationError
    , SurfaceTransform
    , CompositeAlpha
    , PresentMode}
use vulkano::format::R8G8B8Sint;

pub struct Swapchain<W: Window>{
    window: W,
    instance: Arc<Instance>,
    surface: Arc<Surface>,
    swapchain: Arc<Swapchain>,
    images: Vec<Arc<SwapchainImage>>,
}

impl<W: Window> Swapchain<W>{

    pub fn new(device: Arc<Device>,window: W,queue: &Arc<Queue>) -> Result<Self,SwapchainCreationError>{
        let instance = window.instance();
        let surface = window.surface();
        let surface_capabilities = surface.capabilities(device.physical_device());

        for format in surface_capabilities.supported_formats.iter(){
            info!("Surface supports format: {:?}",format);
        }
        info!("Supported PresentModes: {:#?}",surface_capabilities.present_modes);

        let (swapchain,images) = Swapchain::new( device
            , surface.clone()
            , surface_capabilities.min_image_count
            , R8G8B8Sint
            , window.get_size()
            , 1
            , surface_capabilities.supported_usage_flags
            , queue
            , SurfaceTransform::Identity
            , CompositeAlpha::Opaque
            , PresentMode::Immediate
            , true
            , None )?;
    }

    pub fn instance(&self) -> &Arc<Instance>{
        &self.instance
    }

    pub fn surface(&self) -> &Arc<Surface>{
        &self.surface
    }
}

pub struct Frame<'a>{
    borrow: &'a Swapchain,
}
