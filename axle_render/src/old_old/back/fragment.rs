// Problem
//
// How do we match a fragment to an renderobject in a way that does not use hashmap.
//

use super::static_buffer::{StaticBufferId,StaticBuffers};


use ::scene::{Transform,RenderObjectId,SceneBack};

pub struct Fragment{
    pub static_buffer: StaticBufferId,
    //pub material: MaterialId,
    pub transform: Transform,
    pub vertex_slice: (u32, u32),
    pub index: usize,
    pub object_id: RenderObjectId,
}

pub struct Fragments{
    pub fragments: Vec<Fragment>,
    last_scene_size: usize,
}

impl Fragments{
    pub fn new() -> Self{
        Fragments{
            fragments: Vec::new(),
            last_scene_size: 0,
        }
    }

    pub fn update(&mut self, scene: &mut SceneBack, static_buffers: &mut StaticBuffers){
        // This function makes 2 assumptions
        // 1 There are no renderobject in the scene in the range 0..last_scene_size which do not
        //   have a fragment pointing to to index of the renderobject
        //
        // 2 renderobject ids are unique.
        //
        // The restriction 1 should be upheld by fragments itself.
        // resriction 2 should be upheld by the scene.


        let scene_borrow = scene.objects.borrow();

        // first update existing fragments and check if fragments are invalidated.
        let mut invalidated_fragments = Vec::new();
        let mut new_objects = Vec::new();

        for (index,fragment) in self.fragments.iter_mut().enumerate(){
            // check if fragment is invalidated.
            // fragments are checked for invalidity front to back.
            if fragment.index >= scene_borrow.len(){
                invalidated_fragments.push(index);
            } else  if fragment.object_id != scene_borrow[fragment.index].id{
                invalidated_fragments.push(index);
                new_objects.push(fragment.index);
            }else{
                fragment.transform = scene_borrow[fragment.index].transform;
            }
        }

        // If the size of the scene changed we need to create fragments
        // for the newly added renderobjects.
        if self.last_scene_size != scene_borrow.len(){
            for i in self.last_scene_size..scene_borrow.len(){
                new_objects.push(i)
            }
            self.last_scene_size = scene_borrow.len();
        }

        // if we swap remove fragments back to front we perserve to index
        // of the not yet removed fragments
        while let Some(x) =  invalidated_fragments.pop(){
            self.fragments.swap_remove(x);
        }

        if new_objects.len() > 0 {
            trace!("Caching {} new renderobjects",new_objects.len());
        }
        // Then add new fragments.
        for new_object_index in new_objects.drain(..){
            let object = &scene_borrow[new_object_index];
            let static_buffer_id = static_buffers.register(&object.mesh);

            {
                //TODO find a way to deal with unloaded assets.
                let model_borrow = object.mesh.borrow().unwrap();
                let new_fragments = model_borrow.meshes.iter().map(|e| {
                    Fragment{
                        static_buffer: static_buffers.increment(&static_buffer_id),
                        //material: materials.register(textures,&e.material),
                        transform: object.transform,
                        vertex_slice: (e.index_range.start as u32,e.index_range.end as u32),
                        index: new_object_index,
                        object_id: object.id,
                    }
                });
                for frag in new_fragments{
                    self.fragments.push(frag);
                }
            }
        }
    }

    pub fn build_command_buffer(&mut self) -> Comma
}

