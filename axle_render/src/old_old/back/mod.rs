use super::window::Window;
use super::scene::SceneBack;

use std::sync::Arc;
use std::sync::atomic::{AtomicBool,Ordering};

use ::core::registery::{Value,Error};

use vulkano::device::{Device,Queue};
use vulkano::instance::{Instance,PhysicalDevice,DeviceExtensions,Features};

mod config;
mod static_buffer;
mod fragment;

pub struct Back<W: Window>{
    pub window: W,
    pub scene: SceneBack,
    pub running: Arc<AtomicBool>,
    pub config: Value<config::Config>,
    pub device: Arc<Device>,
    pub queue: Arc<Queue>,

    pub static_buffers: static_buffer::StaticBuffers,
    pub fragments: fragment::Fragments,
}

impl<W: Window> Back<W>{
    pub fn new(window: W,scene: SceneBack,running: Arc<AtomicBool>) -> Self{
        //TODO: handle errors.

        let config = match Value::new_or_default("render"){
            Ok(x) => x,
            Err((x,e)) => {
                match e {
                    Error::NotFound => {},
                    e => warn!("error with config for renderer: {}", e),
                }
                x
            }
        };

        let instance = window.instance();
        let (device,queue) = Self::build_device(&instance,&config);

        let static_buffers = static_buffer::StaticBuffers::new(queue.clone());
        let fragments = fragment::Fragments::new();

        Back{
            scene,
            running,
            config,
            window,
            device,
            queue,

            static_buffers,
            fragments,
        }
    }

    pub fn run(mut self){
        info!("Render engine starting!");
        while self.running.load(Ordering::Acquire){
            self.fragments.update(&mut self.scene,&mut self.static_buffers);
        }
        info!("Render engine quiting!");
    }

    pub fn build_device(instance: &Arc<Instance>,config: &Value<config::Config>) -> (Arc<Device>,Arc<Queue>){
        let physical_device_iter = PhysicalDevice::enumerate(&instance);

        let mut physical_device = None;

        for physical_dev in physical_device_iter{
            info!("Found device: {}",physical_dev.name());
            info!("\ttype: {:?}",physical_dev.ty());
            info!("\tapi version: {}",physical_dev.api_version());
            info!("\tuuid: {:?}",physical_dev.uuid());
            info!("\tmemory heaps:");
            for mem in physical_dev.memory_heaps(){
                info!("\tFound memory heap:");
                info!("\t\tid: {}",mem.id());
                info!("\t\tsize: {}",mem.size());
                info!("\t\tdevice local: {}",mem.is_device_local());
            }
            if physical_device.is_none(){
                physical_device = Some(physical_dev);
            }else if *physical_dev.uuid() == config.render_device_uuid{
                physical_device = Some(physical_dev);
            }

        }
        let physical_device = physical_device.expect("No rendering device found!");

        let queue = physical_device.queue_families()
            .filter(|e| e.supports_graphics() && e.supports_transfers())
            .next()
            .expect("No graphics capable queue families found");

        let features = Features{
            .. Features::none()
        };

        let extensions = DeviceExtensions{
            khr_swapchain: true,
            .. DeviceExtensions::none()
        };

        let (device, mut queue) = Device::new(physical_device,&features,&extensions,[(queue,1.0)].iter().cloned())
            .unwrap();

        (device,queue.next().unwrap())
    }
}
