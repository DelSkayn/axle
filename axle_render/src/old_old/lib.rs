//! This crate contains the renderer for the axle game engine.

#![allow(unused_imports)]
#![allow(dead_code)]
#![feature(fnbox)]

extern crate axle_core as core;
#[macro_use]
extern crate vulkano;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate crossbeam;

mod scene;
mod window;
mod back;

pub use scene::{Scene,RenderObject};
use crossbeam::sync::SegQueue;

use core::task::JoinHandle;

pub use self::window::{Window,IntoWindow};

use std::boxed::FnBox;
use std::sync::atomic::{Ordering,AtomicBool};
use std::sync::Arc;
use core::registery;


//type MessageQue<W> = SegQueue<Box<FnBox(&back::Back<W>) + 'static + Send>>;

pub struct Render{
    running: Arc<AtomicBool>,
    join: Option<JoinHandle>,
}

impl Render{
    pub fn new<A,W,I>(window: I) -> (Self,scene::Scene)
        where I: IntoWindow,
    {
        let name = registery::Registery::get("cargo.name").unwrap_or("AxleGame".to_string());
        let version = registery::Registery::get("cargo.version").unwrap_or("0.0.0".to_string());
        let mut version_iter = version.split(".");
        let version = 
            ( version_iter.next().unwrap_or("0").parse().unwrap_or(0)
            , version_iter.next().unwrap_or("0").parse().unwrap_or(0)
            , version_iter.next().unwrap_or("0").parse().unwrap_or(0));


        let layers: Vec<String> = registery::Registery::get("render.enabled_layers").unwrap_or(Vec::new());
        let layers_str: Vec<&str> = layers.iter().map(|e| e.as_str()).collect();

        let window = window.into_window(&name,version,layers_str.iter())
            .expect("Could not start render engine, window initialisation failed");

        let (scene,back) = scene::Scene::new();
        let running = Arc::new(AtomicBool::new(true));
        let running_clone = running.clone();
        let join = {
            let sync = core::task::sync();
            Some(core::task::spawn(&sync,||{
                back::Back::new(window,back,running_clone).run();
            }))
        };
        (Render{
            running,
            join,
        },scene)
    }
}

impl Drop for Render{
    fn drop(&mut self){
        self.running.store(false,Ordering::Release);
        self.join.take().unwrap().join();
    }
}


