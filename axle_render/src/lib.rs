#![allow(dead_code)]
extern crate axle_core;
extern crate vulkano;
#[macro_use]
extern crate log;

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate crossbeam;

use vulkano::{instance,device};

mod error;
pub use error::*;

pub use std::sync::Arc;

#[derive(Deserialize)]
pub struct Version{
    pub major: u16,
    pub minor: u16,
    pub patch: u16,
}

#[derive(Deserialize)]
pub struct Config{
    #[serde(default)]
    application_name: Option<String>,
    #[serde(default)]
    application_version: Option<Version>,
    #[serde(default)]
    activated_layers: Vec<String>,
    #[serde(default)]
    physical_device_index: Option<usize>,
}

pub struct Renderer{
    instance: Arc<instance::Instance>,
    device: Arc<device::Device>,
    queue: Arc<device::Queue>,
}

impl Renderer{
    pub fn new(mut config: Config) -> Result<Self>{
        info!("initializing renderer");
        let version = config.application_version.as_ref().map(|v|{
            instance::Version{
                major: v.major,
                minor: v.minor,
                patch: v.patch,
            }
        });

        let engine_version = instance::Version{
            major: env!("CARGO_PKG_VERSION_MAJOR").parse().unwrap(),
            minor: env!("CARGO_PKG_VERSION_MINOR").parse().unwrap(),
            patch: env!("CARGO_PKG_VERSION_PATCH").parse().unwrap(),
        };

        let app_info = instance::ApplicationInfo{
            application_name: config.application_name.map(|e| e.into()),
            application_version: version,
            engine_name: Some("axle engine".into()),
            engine_version: Some(engine_version),
        };

        let instance_extensions = instance::InstanceExtensions::supported_by_core()
            .chain_err(|| ErrorKind::RendererCreation("could not get instance extension".to_string()))?;

        let layer_list = instance::layers_list()
            .chain_err(|| ErrorKind::RendererCreation("could not get supported layers".to_string()))?;

        let mut supported_layers = Vec::new();

        for layer in layer_list{
            debug!("supported layer: {}",layer.name());
            debug!("{}",layer.description());
            for i in 0..config.activated_layers.len(){
                if config.activated_layers[i] == layer.name(){
                    supported_layers.push(config.activated_layers.swap_remove(i));
                    break;
                }
            }
        }
        for layer in config.activated_layers{
            warn!("layer \"{}\" not supported, not enabled",layer);
        }

        let supported_layers_ref: Vec<_> = supported_layers.iter().map(|e| e.as_str()).collect();

        let instance = instance::Instance::new(Some(&app_info)
            ,&instance_extensions
            ,supported_layers_ref.iter())
            .chain_err(|| ErrorKind::RendererCreation("could not create vulkan instance".to_string()))?;

        let instance_clone = instance.clone();

        let physical_devices = instance::PhysicalDevice::enumerate(&instance);
        for physical_device in physical_devices{
            debug!("found physical device: {}",physical_device.name());
            debug!("index: {}",physical_device.index());
            debug!("vulkan api version: {}",physical_device.api_version());
            debug!("type: {:?}",physical_device.ty());
            debug!("supported features: {:?}",physical_device.supported_features());
            debug!("queue families:");
            for queue_family in physical_device.queue_families(){
                debug!("\tid: {}",queue_family.id());
                debug!("\tsupports graphics: {}",queue_family.supports_graphics());
                debug!("\tsupports compute: {}",queue_family.supports_compute());
                debug!("\tsupports transfers: {}",queue_family.supports_transfers());
                debug!("\tsupports sparse bindings: {}",queue_family.supports_sparse_binding());
            }
        }
        let physical_device = config.physical_device_index.and_then(|i|{
            let res = instance::PhysicalDevice::from_index(&instance,i);
            if res.is_none(){
                warn!("invalid physical device index in config")
            }
            res
        });
        let physical_device = if let Some(x) = physical_device{
            x
        }else{
            instance::PhysicalDevice::enumerate(&instance)
                .next()
                .ok_or(Error::from_kind(ErrorKind::NoPhysicalDevice))?
        };

        let features = instance::Features{
            .. instance::Features::none()
        };

        let extensions = device::DeviceExtensions{
            khr_swapchain: true,
            .. device::DeviceExtensions::none()
        };
        
        let queue_family = physical_device.queue_families()
            .filter(|q|{
                q.supports_graphics() && q.supports_transfers()
            }).next().ok_or(ErrorKind::NoQueueFamily)?;

        let (device,mut queues) = device::Device::new(physical_device
            ,&features
            ,&extensions
            , Some((queue_family,1.0)))?;


        info!("finished initializing renderer");
        Ok(Renderer{
            instance: instance_clone,
            device: device,
            queue: queues.next().unwrap()
        })
    }
}
