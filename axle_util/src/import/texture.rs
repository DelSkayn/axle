
extern crate image;

use core::asset::{Texture,Channel};
use self::image::{DynamicImage};
pub use self::image::ImageError as Error;
use std::path::Path;


pub fn import<P: AsRef<Path>>(p: P) -> Result<Texture,super::Error>{
    info!("Loading texture: {}",p.as_ref().to_str().unwrap_or("Could not parse path"));
    let image = image::open(p)?;
    let texture = match image{
        DynamicImage::ImageRgb8(x) => {
            let (w,h) = x.dimensions();
            Texture{
                data: x.into_raw(),
                width: w,
                height: h,
                channel: Channel::Rgb,
            }
        },
        DynamicImage::ImageRgba8(x) => {
            let (w,h) = x.dimensions();
            Texture{
                data: x.into_raw(),
                width: w,
                height: h,
                channel: Channel::Rgba,
            }
        },
        DynamicImage::ImageLuma8(x) => {
            let (w,h) = x.dimensions();
            Texture{
                data: x.into_raw(),
                width: w,
                height: h,
                channel: Channel::Gray,
            }
        },
        _ => panic!("Not supported do something about it."),
    };
    Ok(texture)
}
