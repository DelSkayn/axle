extern crate assimp;
use self::assimp::{Scene, PostProcess, logger};
use self::assimp::scene::TextureParams;
pub use self::assimp::Error;

use core::asset::{Model,Mesh,Material,AssetRef,Texture};
use core::task::promise::{Promise,Promises};
use super::texture;


use std::path::{Path,PathBuf};
use std::collections::HashMap;
use std::str;

type LoadedTextureMap = HashMap<PathBuf,AssetRef<Texture>>;
type LoadedMaterialMap = HashMap<*const assimp::Material,AssetRef<Material>>;

pub fn import<P: AsRef<Path>,PT: AsRef<Path>>(p: P,n: PT) -> Result<Model,super::Error>{
    info!("Loading model: {}",p.as_ref().to_str().unwrap_or("Could not confert path."));
    load_model(p.as_ref(),n.as_ref())
}

fn log(message: &str) {
    info!("[assimp] {}", message);
}

/// loads a model from a path.
/// # Args
/// path: The path of the file to be loaded as a model.
/// new: The place the file will be imported to, used for setting the path of child assets
/// correctly. 
///
pub fn load_model(path: &Path,new: &Path) -> Result<Model, super::Error> {
    info!("Start reading model");
    logger::init_logger(log);
    let flags = PostProcess::max_quality();
    let mut scene = Scene::from_path(path, flags)?;

    let mut vertices = Vec::new();
    let mut normals = Vec::new();
    let mut texture_coords = Vec::new();
    let mut indecies = Vec::new();

    let mut meshes = Vec::with_capacity(scene.meshes.len());
    let mut materials: Vec<AssetRef<Material>> = Vec::new();

    load_materials(path
        , new
        , &mut materials
        , scene.meshes.iter()
        , HashMap::new()
        , HashMap::new()
        , 0)?;

    for (mesh,material) in scene.meshes.iter_mut().zip(materials){
        let vertex_offset = vertices.len() as u32;

        // Append all the data to the global data.
        vertices.extend_from_slice(&mesh.vertices);
        normals.extend_from_slice(&mesh.normals.as_ref().unwrap());

        if let Some(x) = mesh.texture_coords.as_ref(){
            texture_coords.extend_from_slice(&x[0]);
        }else{
            for _ in 0..mesh.vertices.len(){
                texture_coords.push([0.0f32,0.0]);
            }
        }

        let start_len = indecies.len();
        indecies.reserve(mesh.faces.len()*3);
        for face in mesh.faces.iter(){
            indecies.push(face[0]+vertex_offset);
            if face.len() == 1{
                indecies.push(face[0]+vertex_offset);
                indecies.push(face[0]+vertex_offset);
            }else if face.len() == 2{
                indecies.push(face[1]+vertex_offset);
                indecies.push(face[1]+vertex_offset);
            }else{
                indecies.push(face[1]+vertex_offset);
                indecies.push(face[2]+vertex_offset);
            }
        }

        meshes.push(Mesh {
            index_range: (start_len..indecies.len()),
            material: material,
        })
    }

    let vert_len = vertices.len();
    assert_eq!(normals.len(),vert_len);
    assert_eq!(texture_coords.len(),vert_len);

    info!("Finished reading model");
    Ok(Model {
        normals: normals,
        vertices: vertices,
        texture_coords: texture_coords,
        indecies: indecies,
        meshes: meshes
    })
}



fn load_single_texture( texture: &Option<TextureParams>
    , path: &Path
    , new: &Path
    , promise: &mut Option<Box<Promises<Result<(),super::Error>>>>
    , loaded_textures: &mut LoadedTextureMap) -> Option<AssetRef<Texture>>
{
    texture.as_ref().and_then(|texture|{
        texture.path.as_ref()
    }).and_then(|import_path_buf|{
        // Remove unnessary withspace which might creep in when ussing assimp.
        // TODO: Look into the origin of the withspace in assimp lib
        let import_path_str = import_path_buf.to_str().unwrap().trim_left();
        // Remove the filename and take the directory 
        let import_path = path.parent().unwrap().join(&import_path_str);
        Some(loaded_textures.entry(import_path.clone()).or_insert_with(||{
            // The parent directory of the new model joined with the file name of the
            // import texture
            let export_path = new.parent().unwrap().join(import_path.file_stem().unwrap());
            let rf = AssetRef::new(export_path.clone());
            let rf_clone = rf.clone();
            *promise = Some(Box::new(Promise::<_,Result<(),super::Error>>::new(move||{
                let text = texture::import(import_path)?;
                rf_clone.load(text);
                Ok(())
            })));
            rf
        }).clone())
    })
}

/// Recsively Loads materials from the models 
///
/// NOTE: There are some ideas here were about chaining of promises,
/// Should look into for task-rs.
/// 
/// # Args
/// path: The path of the model being imported,
/// new: The path of where the model will be placed
/// mesh_vec: A vec in which the materials will be saved.
/// meshes: An iterator over assimp meshes.
/// loaded_textures: An hashmap with the textures the function has already loaded.
/// loaded_materials: An hashmap with the materials the function has already loaded.
/// next_material_mod: Used for generating a unsique name, should be initialy set to 0
fn load_materials<'a,I: Iterator<Item = &'a assimp::Mesh>>(path: &Path
    , new: &Path
    , materials: &mut Vec<AssetRef<Material>>
    , mut meshes: I
    , mut loaded_textures: LoadedTextureMap
    , mut loaded_materials: LoadedMaterialMap
    , mut next_material_mod: usize) -> Result<(),super::Error>
{
    let mut diff_promise: Option<Box<Promises<Result<(),super::Error>>>> = None;
    let mut roug_promise: Option<Box<Promises<Result<(),super::Error>>>> = None;
    let mut norm_promise: Option<Box<Promises<Result<(),super::Error>>>> = None;

    if let Some(mesh) = meshes.next(){
        {
        let material = loaded_materials.entry(&*mesh.material as *const assimp::Material)
            .or_insert_with(||{
            let diffuse_map = load_single_texture(&mesh.material.textures.diffuse
                , path
                , new
                , &mut diff_promise
                , &mut loaded_textures);
            let roughness_map = load_single_texture(&mesh.material.textures.shininess
                , path
                , new
                , &mut roug_promise 
                , &mut loaded_textures);
            let normal_map = load_single_texture(&mesh.material.textures.height
                , path
                , new
                , &mut norm_promise 
                , &mut loaded_textures);

            let roughness = shininess_to_roughness(mesh.material.shininess.unwrap_or(0.0));
            let diffuse = mesh.material.diffuse.unwrap_or([1.0,0.0,1.0]);
            let metalness = 0.0;

            let material_name = mesh.material.name.as_ref().cloned().unwrap_or_else(||{
                let r = format!("{}_material_{}",new.file_stem().unwrap().to_str().unwrap(),next_material_mod);
                next_material_mod += 1;
                r
            });

            let path = new.parent().unwrap().join(material_name);
            let rf = AssetRef::new(path);

            rf.load(Material{
                roughness,
                metalness,
                diffuse,
                diffuse_map,
                roughness_map,
                normal_map
            });
            rf
        });
        materials.push(material.clone());
        }

        load_materials(path,new,materials,meshes,loaded_textures,loaded_materials,next_material_mod)?;

        if let Some(x) = diff_promise{
            x.get()?;
        }
        if let Some(x) = roug_promise{
            x.get()?;
        }
        if let Some(x) = norm_promise{
            x.get()?;
        }
        Ok(())
    }else{
        Ok(())
    }
}

fn shininess_to_roughness(shiny: f32) -> f32 {
    (2.0 / shiny + 2.0).sqrt()
}
