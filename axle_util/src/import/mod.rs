#![allow(unused_imports)]
//!
//! This crate provides functionality for importing asset of an external data type into the axle
//! asset system.
//!



use std::{io,error,fmt};

pub mod model;
pub mod texture;

#[derive(Debug)]
pub enum Error{
    Model(model::Error),
    Texture(texture::Error),
    Io(io::Error),
}

impl From<model::Error> for Error{
    fn from(e: model::Error) -> Self{
        Error::Model(e)
    }
}

impl From<texture::Error> for Error{
    fn from(e: texture::Error) -> Self{
        Error::Texture(e)
    }
}

impl From<io::Error> for Error{
    fn from(e: io::Error) -> Self{
        Error::Io(e)
    }
}

impl fmt::Display for Error{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::Model(ref e) => write!(f,"Error during model loading: {}", e),
            Error::Texture(ref e) => write!(f,"Error during texture loading: {}", e),
            Error::Io(ref e) => write!(f,"Io error: {}", e),
        }
    }
}

impl error::Error for Error{
    fn description(&self) -> &str{
        match *self{
            Error::Model(ref e) => e.description(),
            Error::Texture(ref e) => e.description(),
            Error::Io(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error>{
        match *self {
            Error::Model(ref e) => Some(e),
            Error::Texture(ref e) => Some(e),
            Error::Io(ref e) => Some(e),
        }
    }
}
