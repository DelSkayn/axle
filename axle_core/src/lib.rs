//!
//! This crate defines functionality which is used throughout the engine.
//!
//! All axle crates depend on axle core.
//!


#![allow(dead_code)]
#![feature(const_fn)]
#![feature(const_unsafe_cell_new)]
// For error chain
#![recursion_limit = "1024"]


//#[macro_use]
//extern crate axle_derive;
pub extern crate rayon;
pub extern crate cgmath;
extern crate crossbeam;
#[macro_use]
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
//extern crate bincode;
#[macro_use]
extern crate log;
extern crate chrono;
extern crate backtrace;
extern crate fern;
extern crate vulkano;
#[macro_use]
extern crate error_chain;
extern crate image;

pub extern crate specs;

pub mod registery;
pub mod console;
pub mod window;
pub mod asset;
pub mod common;

