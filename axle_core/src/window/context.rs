///
/// This module is dealing with the render interfaceing part of the rendering system.
/// This includes doing certian platform specific parts.
///
extern crate libc;
extern crate vulkano;

use self::vulkano::instance::Instance;
use self::vulkano::swapchain::{Surface, SurfaceCreationError};

#[cfg(all(unix,not(target_os = "android")))]
use super::winit::os::unix::WindowExt;
use super::winit::Window as WinitWindow;

use super::Window;

use std::sync::Arc;

/// The context used to initialize a rendering api

pub struct Context{
    window: Arc<WinitWindow>,
}

impl Context {
    pub fn new(w: &Window) -> Self {
        Context { 
            window: w.win.clone(),
        }
    }

    /// Create a vulkano surface out of an instance.
    pub fn create_surface(&self, inst: &Arc<Instance>) -> Result<Arc<Surface>, SurfaceCreationError> {
        unsafe { create_surface(&self.window, inst) }
    }

    /// Return the size of the window.
    /// TODO: Create framebuffer trait?
    pub fn get_size(&self) -> [u32; 2] {
        self.window
            .get_inner_size_pixels()
            .map(|e| [e.0, e.1])
            .unwrap_or([0, 0])
    }
}


#[cfg(all(unix, not(target_os = "android")))]
unsafe fn create_surface(window: &WinitWindow,
                         instance: &Arc<Instance>)
                         -> Result<Arc<Surface>, SurfaceCreationError> {
    match (window.get_wayland_display(), window.get_wayland_surface()) {
        (Some(display), Some(surface)) => Surface::from_wayland(instance.clone(), display, surface),
        _ => {
            // No wayland display found, check if we can use xlib.
            // If not, we use xcb.
            if instance.loaded_extensions().khr_xlib_surface {
                Surface::from_xlib(instance.clone(),
                                   window.get_xlib_display().unwrap(),
                                   window.get_xlib_window().unwrap() as _)
            } else {
                Surface::from_xcb(instance.clone(),
                                  window.get_xcb_connection().unwrap(),
                                  window.get_xlib_window().unwrap() as _)
            }
        }
    }
}

#[cfg(target_os = "windows")]
unsafe fn create_surface(window: &WinitWindow,
                         instance: &Arc<Instance>) -> Result<Arc<Surface>, SurfaceCreationError> {
    Surface::from_hwnd(instance,
                       ptr::null() as *const (), // FIXME
                       win.get_hwnd());
}
