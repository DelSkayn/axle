//! This crate defines the object managing the window.
//!
//! The window is the source of input for the engine.
//! For retrieving all input, like mouse position and button presses, you will need to use the
//! window.

extern crate winit;

mod event;
pub mod context; 
pub mod input;

use self::input::{Mouse,Keyboard,EventSink};

pub use self::event::*;
use std::sync::Arc;

use ::registery::{FromRegistery};
use self::winit::{Window as WinitWindow, WindowBuilder as WinitWindowBuilder,EventsLoop as WinitEventsLoop};


fn default_window_position() -> Option<[i32; 2]>{
    WindowBuilder::new().window_position }

fn default_window_size() -> [u32;2] {
    WindowBuilder::new().window_size
}

fn default_title() -> String {
    WindowBuilder::new().title
}



/// A struct used to build a new window with specific settings.
#[derive(Deserialize,Debug,Clone)]
#[serde(rename(deserialize = "window"))]
pub struct WindowBuilder {
    #[serde(default = "default_window_size")]
    window_size: [u32; 2],
    #[serde(default = "default_window_position")]
    window_position: Option<[i32; 2]>,
    #[serde(default = "default_title")]
    title: String,
}

impl FromRegistery for WindowBuilder{
    type Config = Self;

    fn from_registery(config: Self) -> Self{
        config
    }
}

impl WindowBuilder {
    pub fn new() -> Self {
        WindowBuilder {
            window_size: [800, 600],
            window_position: None,
            title: "Axle Game-engine".to_string(),
        }
    }

    /// Set the size of the to be created window. 
    pub fn with_size(mut self, width: u32, height: u32) -> Self {
        self.window_size = [width, height];
        self
    }

    /// Create a window with a given position on the screen.
    pub fn with_position(mut self, x: i32, y:i32) -> Self{
        self.window_position = Some([x,y]);
        self
    }

    /// Actually create a window.
    pub fn build(self) -> Window {
        let builder = WinitWindowBuilder::new()
            .with_dimensions(self.window_size[0],self.window_size[1])
            .with_title(self.title)
            // disable resizing of window
            .with_min_dimensions(self.window_size[0],self.window_size[1])
            .with_max_dimensions(self.window_size[0],self.window_size[1]);

        let event_loop = WinitEventsLoop::new();
        let win = builder.build(&event_loop).unwrap();
        if let Some(x) = self.window_position{
            win.set_position(x[0],x[1]);
        }
        Window { 
            mouse: Mouse::new(),
            keyboard: Keyboard::new(),
            win: Arc::new(win),
            events: event_loop
        }
    }
}

impl Default for WindowBuilder {
    fn default() -> Self {
        WindowBuilder::new()
    }
}

/// An object representing a system window.
pub struct Window {
    win: Arc<WinitWindow>, 
    events: WinitEventsLoop,
    mouse: Mouse,
    keyboard: Keyboard,
}


impl Window {
    /// update the window pushing input to the various contexts.
    pub fn update(&mut self) {
        {
            let mouse = &mut self.mouse;
            let keyboard = &mut self.keyboard;
            self.events.poll_events(|e|{
                let e = InputEvent::from(e);
                mouse.consume(e);
                keyboard.consume(e);
            });
        }
        self.mouse.update(&self.win);
        self.keyboard.update();
    }


    /// Returns the size of the window in pixels.
    pub fn get_size(&self) -> [u32; 2] {
        self.win.get_inner_size()
            .map(|e| [e.0,e.1])
            .unwrap_or([0,0])
    }

    /// Returns the mouse handeler.
    pub fn mouse(&self) -> &Mouse{
        &self.mouse
    }


    /// Returns the mouse handeler.
    pub fn keyboard(&self) -> &Keyboard{
        &self.keyboard
    }
}

impl Drop for Window{
    fn drop(&mut self){
        info!("Destroying window");
    }
}
