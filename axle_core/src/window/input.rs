use ::crossbeam::sync::MsQueue;
use super::{InputEvent,KeyboardEvent,Key,KeyState,MouseEvent,WinitWindow};
use super::winit::CursorState as WCursorState;


use common::sync::spin_lock::SpinLock;

use std::sync::Arc;
use std::sync::atomic::{AtomicBool,Ordering};
use std::collections::HashMap;


/// A trait for objects which handle events.
pub trait EventSink{
    fn consume(&mut self,InputEvent);
}

impl EventSink for (){
    fn consume(&mut self,_: InputEvent){}
}

/// A enum representing the possible way one grab the mouse context.
#[derive(Clone,Copy)]
pub enum MouseMode{
    /// The mouse is hidden and when the mouse is moved the move event is submited
    Grabbed,
    /// The mouse is free to move about and when the mouse is inside the window the pos
    /// command is submited.
    Free,
}

/// A enum representing the way one can grab the keyboard context.
#[derive(Clone,Copy)]
pub enum KeyMode{
    /// Characters are returned.
    Text,
    /// The key states are returned.
    Keys,
}

impl MouseMode{
    fn is_grabbed(&self) -> bool{
        match *self{
            MouseMode::Grabbed => true,
            MouseMode::Free => false,
        }
    }
}

/// A struct which handels things to do with mouse events.
pub struct Mouse{
    context: SpinLock<Vec<(MouseMode,Arc<MsQueue<MouseEvent>>)>>,
    grabbed: AtomicBool,
    current_pos: [f32; 2],
}

impl EventSink for Mouse{
    fn consume(&mut self,ev: InputEvent){
        if let Some(x) = self.context.lock().last(){
            match ev{
                InputEvent::Mouse(y) => {
                    if self.grabbed.load(Ordering::Acquire){
                        match y {
                            MouseEvent::Pos(z) => {
                                self.current_pos = z;
                            },
                            z => { x.1.push(z); } ,
                        }
                    }else{
                        x.1.push(y);
                    }
                },
                _ => {},
            }
        }
    }
}

impl Mouse{
    #[doc(hidden)]
    pub fn new() -> Self{
        Mouse{
            context: SpinLock::new(Vec::new()),
            grabbed: AtomicBool::new(false),
            current_pos: [0.0,0.0],
        }
    }

    /// Grab a mouse context with a specific mode.
    /// As long as the context grabbed is the last context grabbed events are submited to 
    /// the context. If an other mouse context is grabbed events are submited to that context.
    /// If a context is dropped the events are submited to the previous context.
    pub fn grab(&self,mode: MouseMode) -> MouseContext{
        trace!("[window] Mouse grabbed!");
        let que = Arc::new(MsQueue::new());
        self.context.lock().push((mode,que.clone()));
        MouseContext(que)
    }

    #[doc(hidden)]
    pub fn update(&mut self,win: &Arc<WinitWindow>){
        let mut borrow = self.context.lock();
        while let Some(x) = borrow.pop(){
            if Arc::strong_count(&x.1) != 1{
                borrow.push(x);
                break;
            }
            trace!("Mouse released!");
        }
        if let Some(x) = borrow.last(){
            if x.0.is_grabbed() != self.grabbed.load(Ordering::Acquire) {
                trace!("Change mouse mode!");
                self.grabbed.store(x.0.is_grabbed(),Ordering::Release);
                if x.0.is_grabbed(){
                    trace!("[Window] Window grabbed!");
                    win.set_cursor_state(WCursorState::Grab).unwrap();
                }else{
                    trace!("[Window] Window released!");
                    win.set_cursor_state(WCursorState::Normal).unwrap();
                }
            }
        }else{
            if self.grabbed.load(Ordering::Acquire){
                self.grabbed.store(false,Ordering::Release);
                win.set_cursor_state(WCursorState::Normal).unwrap();
            }
        }
        if self.grabbed.load(Ordering::Acquire) {
            let (x,y) = win.get_inner_size()
                .unwrap_or((0,0));
            win.set_cursor_position((x/2) as i32,(y/2) as i32).unwrap();
            let mut size = [x,y];
            size[0] /= 2;
            size[1] /= 2;
            let size = [size[0] as f32, size[1] as f32];
            let mov = [size[0] - self.current_pos[0],size[1] - self.current_pos[1]];
            if mov != [0.0,0.0] {
                borrow.last().unwrap().1.push(MouseEvent::Move(mov));
            }
            self.current_pos = size;
        }
    }
}

/// A struct representing a mouse context.
/// As long as the context grabbed is the last context grabbed events are submited to 
/// the context. If an other mouse context is grabbed events are submited to that context.
/// If a context is dropped the events are submited to the previous context.
pub struct MouseContext(Arc<MsQueue<MouseEvent>>);

impl MouseContext{
    /// Return a mouse event if one is available.
    #[inline]
    pub fn poll(&self) -> Option<MouseEvent>{
        self.0.try_pop()
    }

    /// returns an iterator which iters over all the available events.
    #[inline]
    pub fn iter(&self) -> MouseIter{
        MouseIter(self)
    }
}

pub struct MouseIter<'a>(&'a MouseContext);

impl<'a> Iterator for MouseIter<'a>{
    type Item = MouseEvent;

    #[inline]
    fn next(&mut self) -> Option<Self::Item>{
        self.0.poll()
    }
}

pub struct Keyboard{
    context: SpinLock<Vec<Arc<MsQueue<KeyboardEvent>>>>,
}

impl Keyboard{
    #[doc(hidden)]
    pub fn new() -> Keyboard{
        Keyboard{
            context: SpinLock::new(Vec::new()),
        }
    }

    #[doc(hidden)]
    pub fn update(&self){
        let mut borrow = self.context.lock();
        while let Some(x) = borrow.pop(){
            if Arc::strong_count(&x) != 1{
                borrow.push(x);
                return;
            }
            trace!("Keyboard released!");
        }
    }

    /// Grab a key context.
    pub fn grab_key(&self) -> KeyContext{
        trace!("[window] keyboard grabbed!");
        let que = Arc::new(MsQueue::new());
        self.context.lock().push(que.clone());
        KeyContext{
            messages: que,
            state: HashMap::new(),
        }
    }

    /// Grab a text context.
    pub fn grab_text(&self) -> TextContext{
        trace!("[window] keyboard grabbed!");
        let que = Arc::new(MsQueue::new());
        self.context.lock().push(que.clone());
        TextContext{
            messages: que,
        }
    }
}

impl EventSink for Keyboard{
    fn consume(&mut self,ev: InputEvent){
        if let Some(x) = self.context.lock().last(){
            match ev{
                InputEvent::Keyboard(y) => {
                    x.push(y)
                },
                _ => {},
            }
        }
    }
}

/// An struct which represents a grabbed context of the keyboard which returns charaters.
pub struct TextContext{
    messages: Arc<MsQueue<KeyboardEvent>>
}

impl TextContext{
    pub fn poll(&self) -> Option<char>{
        while let Some(x) = self.messages.try_pop(){
            match x{
                KeyboardEvent::Character(x) =>{
                    return Some(x);
                },
                _ => {},
            }
        }
        None
    }

    pub fn iter<'a>(&'a self) -> TextIter<'a>{
        TextIter(self)
    }
}

pub struct TextIter<'a>(&'a TextContext);

impl<'a> Iterator for TextIter<'a>{
    type Item = char;

    #[inline]
    fn next(&mut self) -> Option<Self::Item>{
        self.0.poll()
    }
}

/// An struct representing a grabbed context of keyboard which returns keys.
pub struct KeyContext{
    messages: Arc<MsQueue<KeyboardEvent>>,
    state: HashMap<Key,KeyState>,
}

impl KeyContext{
    /// Recieve possible events
    pub fn poll(&self) -> Option<(KeyState,Key)>{
        while let Some(x) = self.messages.try_pop(){
            match x{
                KeyboardEvent::Key(x,y) =>{
                    return Some((x,y));
                },
                _ => {},
            }
        }
        None
    }

    /// Create an iterator of all recieved events
    pub fn iter<'a>(&'a self) -> KeyIter<'a>{
        KeyIter(self)
    }

    /// Update the tracked state with the event.
    pub fn update(&mut self,event: (KeyState,Key)){
        if let Some(x) = self.state.get_mut(&event.1){
            *x = event.0;
        }
    }

    /// Register a key to be tracked in the state.
    pub fn track(&mut self,key: Key,state: KeyState){
        self.state.insert(key,state);
    }

    /// Flush all events to the state
    /// simular to calling poll and update.
    pub fn flush(&mut self){
        while let Some(x) = self.poll(){
            self.update(x);
        }
    }
}

pub struct KeyIter<'a>(&'a KeyContext);

impl<'a> Iterator for KeyIter<'a>{
    type Item = (KeyState,Key);

    #[inline]
    fn next(&mut self) -> Option<Self::Item>{
        self.0.poll()
    }
}


