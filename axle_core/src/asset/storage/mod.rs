//!
//! This module deals with the storing of assets data.
//!
use super::{Asset,Result};

pub mod simple;
pub use self::simple::SimpleStorage;

/// A trait defining asset handles which are structs which point to a specific asset.
pub trait AssetHandle: Clone + Send {
    type Location: Send + 'static;

    fn location(&self) -> Self::Location;
}

/// The struct which handles loaded assets and places them into a storage.
pub trait AssetProcessor<A: Asset,H: AssetHandle>: Send + 'static{
    fn prepare(&self,asset: AssetProcessData<A,H>);
}

/// An enum which is what is pused to a asset processor to handle finished assets.
pub enum AssetProcessData<A: Asset,H: AssetHandle>{
    Load{
        asset: Result<A>,
        name: String,
        location: H::Location,
    },
    Reload{
        asset: Result<A>,
        name: String,
        location: H::Location,
    }
}

/// An trait which defines the a struct which can store assets.
pub trait AssetStorage<A: Asset>: Sync{
    type Handle: AssetHandle;
    type Processor: AssetProcessor<A,Self::Handle>;

    /// This function returns the processor for the asset storage to which one can push new assets.
    fn processor(&self) -> Self::Processor;

    /// Allocate a handle to which a new asset might be pushed
    fn alloc_handle(&self) -> Self::Handle;

    /// Returns a asset accompying a handle
    /// Do i need this function??
    fn get(&self,Self::Handle) -> Option<&A>;

    /// Updates storage allowing pushing pending assets to the storage.
    /// Do i really need this function??
    fn update(&mut self);
}

