use crossbeam::sync::MsQueue;
use super::{AssetStorage,AssetProcessor,AssetHandle,AssetProcessData};
use super::super::Asset;

use std::cell::UnsafeCell;
use std::sync::Arc;
use std::sync::atomic::{AtomicUsize,Ordering};

/// A simple implementation for storage which does nothing but keep things in memory
/// and allow quick access.
/// TODO: at some point move the vec storage over to a packed vector to safe memory
struct Internal<A: Asset>{
    processed: MsQueue<AssetProcessData<A,Handle<A>>>,
    free_handles: MsQueue<usize>,
    next_handle: AtomicUsize,
    assets: Vec<UnsafeCell<Option<A>>>,
}

/// A simple storage which can be used for all assets which have no futher special needs.
pub struct SimpleStorage<A:Asset>(Arc<Internal<A>>);
/// A processor belonging to the simple storage
pub struct SimpleProcessor<A: Asset>(Arc<Internal<A>>);
/// The handle belonging to the simple storage
pub struct Handle<A: Asset>{
    pointer: Arc<Internal<A>>,
    index: Arc<usize>,
}

unsafe impl<A: Asset> Send for SimpleStorage<A>{}
unsafe impl<A: Asset> Sync for SimpleStorage<A>{}

unsafe impl<A: Asset> Send for SimpleProcessor<A>{}
unsafe impl<A: Asset> Sync for SimpleProcessor<A>{}

unsafe impl<A: Asset> Send for Handle<A>{}
unsafe impl<A: Asset> Sync for Handle<A>{}

impl<A: Asset + Default> SimpleStorage<A>{
    /// Create a new simple storage object
    pub fn new() -> Self{
        let internal = Internal{
            processed: MsQueue::new(),
            next_handle: AtomicUsize::new(0),
            free_handles: MsQueue::new(),
            assets: Vec::new(),
        };

        SimpleStorage(Arc::new(internal))
    }
}

impl<A: Asset> AssetProcessor<A,Handle<A>> for SimpleProcessor<A>{
    fn prepare(&self,asset: AssetProcessData<A,Handle<A>>){
        self.0.processed.push(asset);
    }
}

impl<A> AssetStorage<A> for SimpleStorage<A>
    where A: Asset + Default
{
    type Handle = Handle<A>;
    type Processor = SimpleProcessor<A>;

    fn processor(&self) -> Self::Processor{
        SimpleProcessor(self.0.clone())
    }

    fn alloc_handle(&self) -> Self::Handle{
        if let Some(x) = self.0.free_handles.try_pop(){
            return Handle{
                pointer: self.0.clone(),
                index: Arc::new(x),
            };
        }
        Handle{
            pointer: self.0.clone(),
            index: Arc::new(self.0.next_handle.fetch_add(1,Ordering::AcqRel)),
        }
    }

    fn update(&mut self){
        while let Some(x) = self.0.processed.try_pop(){
            match x{
                AssetProcessData::Load{
                    asset,
                    name:_,
                    location,
                } => {
                    let asset = match asset{
                        Err(e) => {
                            warn!("Error while loading asset: {}",e);
                            continue;
                        },
                        Ok(x) => x,
                    };
                    unsafe{
                        let ptr = self.0.assets[location].get();
                        (*ptr) = Some(asset);
                    }
                }
                AssetProcessData::Reload{
                    asset,
                    name:_,
                    location,
                } => {
                    let asset = match asset{
                        Err(e) => {
                            warn!("Error while re loading asset: {}",e);
                            continue;
                        },
                        Ok(x) => x,
                    };
                    unsafe{
                        let ptr = self.0.assets[location].get();
                        (*ptr) = Some(asset);
                    }
                }
            }
        }
    }

    fn get(&self,handle: Handle<A>) -> Option<&A>{
        unsafe{
            (*self.0.assets[*handle.index].get()).as_ref()
        }
    }
}

impl<A: Asset> AssetHandle for Handle<A>{
    type Location = usize;

    fn location(&self) -> usize{
        *self.index
    }
}

impl<A: Asset> Drop for Handle<A>{
    fn drop(&mut self) {
        let count = Arc::strong_count(&self.index);
        if count == 1{
            unsafe{
                let ptr = self.pointer.assets[*self.index].get();
                *ptr = None;
            }
            self.pointer.free_handles.push(*self.index);
        }
    }
}

impl<A: Asset> Clone for Handle<A>{
    fn clone(&self) -> Self{
        Handle{
            pointer: self.pointer.clone(),
            index: self.index.clone(),
        }
    }
}

