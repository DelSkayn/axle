//!
//! Module defining errors which can happen in the asset system
//!

use serde_json;

error_chain!{
    errors{
        Source {
            description("error in source"),
        }

        Format{
            description("error in format"),
        }

        Asset{
            description("error in loading asset"),
        }
        
    }

    foreign_links{
        Io(::std::io::Error);
        //ByteFormat(bincode::Error);
        TextFormat(serde_json::Error);
    }
}

/*
        InvalidFile(what: &'static str){
            description("parse error in asset file")
            display("parse error in asset file: {}",what)
        }


        /*
        InvalidFileType(is: AssetType, expected: AssetType){
            description("package has the wrong type")
            display("package has the wrong type expected {} found {}",expected,is)
        }
        */

        IncompetibleVersion(is: Version,should: Version){
            description("package has a version which is not competible with the current engine version")
            display("incompatible package version, is: {}, should be :{}",is,should)
        }

        InvalidPath(path: PathBuf){
            description("the packages path is not valid")
            display("invalid path: {}",path.to_str().unwrap_or("error: could not convert path to utf8 string"))
        }

        PostProcess(error: Box<Error>){
            description("asset loading encountered an error in post process")
            display("asset loading error: {}", error)
        }

*/
