//!
//! This module defines functionality for loading assets
//!

use super::{
    ErrorKind,
    Result,
    ResultExt,
    Format,
    Source,
    Asset,
    AssetStorage,
    AssetHandle,
};
use super::storage::{AssetProcessData,AssetProcessor};

use registery::FromRegistery;
use common::sync::SpinLock;

use rayon;

use std::path::{Path,PathBuf};
use std::collections::HashMap;
use std::fs::File;
use std::sync::Arc;
use std::io::Read;


fn default_base_source() -> String{
    "./".to_string()
}

fn default_hot_reload() -> bool{
    true
}

///
/// The struct which is used in configuration.
/// This is the struct which is deserialized from the registery when creating a loader from
/// registery.
///
#[derive(Serialize,Deserialize)]
pub struct LoaderConfig{
    #[serde(default = "default_base_source")]
    pub base_source: String,
    #[serde(default = "default_hot_reload")]
    pub hot_reload: bool,
    /// TODO: Add different kinds of Sources
    #[serde(default)]
    pub sources: HashMap<String,String>,
}

/// 
/// A basic source which loades files relative to a certain directory.
///
pub struct Directory{
    path: PathBuf,
}

impl Directory{
    /// Create a new directory struct the path given will be the one to which loaded path will be
    /// relative.
    fn new<P: AsRef<Path>>(path: P) -> Self{
        Directory{
            path: path.as_ref().to_path_buf(),
        }
    }
}

impl Source for Directory{
    fn load(&self,path: &str) -> Result<Vec<u8>>{
        let mut file = File::open(self.path.join(path))?;
        let mut buf = Vec::new();
        file.read_to_end(&mut buf)?;
        Ok(buf)
    }
}

type FormatMap<T> = HashMap<String,Box<Format<T>>>;

/// A struct which manages the loading of assets.
pub struct Loader{
    hot_reload: bool,
    sources: SpinLock<HashMap<String,Arc<Source>>>,
    formats: HashMap<TypeId,Box<Any>>,
}

impl FromRegistery for Loader{
    type Config = LoaderConfig;

    fn from_registery(config: LoaderConfig) -> Self{
        let mut sources: HashMap<_,_> = config.sources.into_iter().map(|(k,v)|{
                (k,Arc::new(Directory::new(v)) as Arc<Source>)
            }).collect();
        sources.insert("".to_string(),Arc::new(Directory::new(config.base_source)) as Arc<Source>);
        Loader{
            hot_reload: config.hot_reload,
            sources: SpinLock::new(sources),
        }
    }
}

impl Arc<Loader>{
    /// Create a new loader with a basic source which can be accessed by using "" as a source
    pub fn new<S: Source>(base_source: S) -> Self{
        let mut sources = HashMap::new();
        sources.insert("".to_string(),Arc::new(base_source) as Arc<Source>);
        Loader{
            hot_reload: true,
            sources,
        }
    }

    /// Add a source to the loader with a name.
    /// The name can then be used to reference the source when loading
    pub fn add_source<N,S>(&mut self,name: N, source: S)
        where N: Into<String>,
              S: Source,
    {
        self.sources.insert(name.into(),Arc::new(source) as Arc<Source>);
    }

    pub fn add_storage<A,S>(&self, storage: S){

    }

    /// Load a asset from a source.
    /// Function can complete before the asset is loaded and thus might be 
    pub fn load<A,F,S>(&self,name: &str,format: F,options: F::Options,storage: &S) -> S::Handle
        where A: Asset,
              F: Format<A>,
              S: AssetStorage<A>,
    {
        if let Some(location) = name.find("://"){
            let (source,path) = name.split_at(location);
            let path = &path[3..];
            self.load_from_source(path,format,options,source,storage)
        }else{
            self.load_from_source(name,format,options,"",storage)
        }
    }

    pub fn load_from_source<A,N,F,S>(&self,name: N,format: F,options: F::Options,source: &str,storage: &S) -> S::Handle 
        where A: Asset,
              N: Into<String>,
              F: Format<A>,
              S: AssetStorage<A>,
    {
        let source = self.source(source).clone();
        let processor = storage.processor();
        let name = name.into();
        let name_clone = name.clone();
        let load_func = move || {
            let data = source.load(&name_clone)
                .chain_err(|| ErrorKind::Source)?;
            let data = format.format(data,options)
                .chain_err(|| ErrorKind::Format)?;
            let asset = A::load(data)
                .chain_err(|| ErrorKind::Asset)?;
            Ok(asset)
        };
        let handle = storage.alloc_handle();
        let location = handle.location();
        rayon::spawn(move ||{
            processor.prepare(AssetProcessData::Load{
                asset: load_func(),
                name,
                location: location,
            })
        });
        handle
    }

    fn source(&self,name: &str) -> Arc<Source>{
        self.sources
            .get(name)
            .expect("Invalid source name, Did you forget to add it?")
            .clone()
    }
}
