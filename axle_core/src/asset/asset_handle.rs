
use std::sync::atomic::Ordering;

use crossbeam::epoch::{Owned,Atomic};
use common::sync::sleep;

pub(crate) struct AssetHandleBuilder<A: Asset>{
    handle: AssetHandle<A>,
}

impl AssetHandleBuilder{
    fn catch<F: FnOnce() -> Result<A>>(func: F){
        match func(){
            Ok(e) => {
                handle.asset.store(Some(Owned::new(e)),Ordering::Release)
                handle.status.set(AssetStatus::Loaded as usize)
            }
            Err(e) => {
                handle.error.swap(e,Ordering::AcqRel);
                handle.status.set(AssetStatus::Error as usize)
            }
        }
        handle.asset.sleep.wake();
    }
}

#[derive(Debug,Eq,PartialEq)]
pub enum AssetStatus{
    Unloaded = 0,
    Loaded = 1,
    Reload = 2,
    Error = 3,
}

pub struct AssetHandle<A: Asset>{
    status: AtomicUsize,
    asset: Atomic<A>,
    error: AtomicOption<Error>,
    sleep: Sleep
}

impl<A: Asset> AssetHandle<A>{
    /// Waits till an asset completes loading.
    /// If an error happend during loading the error is returned.
    pub fn wait(&self) -> Result<&A>{
        if self.status() != AssetStatus::Error{
            self.
        }
    }

    /// Returns the stage of loading 
    pub fn status(&self) -> AssetStage{
        match self.status.load(Ordering::Acquire){
            0 => AssetStatus::Unloaded,
            1 => AssetStatus::Loaded,
            2 => AssetStatus::Reload,
            3 => AssetStatus::Error,
            _ => panic!("Invalid asset status")
        }
    }
}
