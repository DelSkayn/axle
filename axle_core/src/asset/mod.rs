//!
//! This crate implements functionality for loading assets.
//!
//! Assets are chunks of data which are loaded while the engine is running.
//!

use std::sync::Arc;

pub mod storage;
pub mod error;
pub mod loader;

pub use self::error::*;
pub use self::storage::{AssetHandle,AssetStorage};
pub use self::loader::Loader;

/// An trait all assets need to implement
pub trait Asset: Sync + Send + Sized + 'static{
    /// The type of the data that will be used to write and save assets.
    type Data: Send + Sized + 'static;
    //type Processor: Sync + 'static;

    /// Used for debugging purposes.
    /// TODO: Create an auto derive trait.
    const NAME: &'static str;

    /// Convert from data to asset
    fn load(data: Self::Data) -> Result<Self>;
}

/// An trait for formats which produce asset data from data.
pub trait Format<A Asset>: Send + 'static{
    /// The possible options the format has
    type Options: Send;

    /// The function which formats data.
    fn format(&self,data: Vec<u8>,options: Self::Options) -> Result<A::Data>;
}

/// An trait for sources which produce data from a path.
pub trait Source: Sync + Send + 'static{
    fn load(&self,path: &str) -> Result<Vec<u8>>;
}

impl<S: Source> Source for Arc<S>{
    fn load(&self,path: &str) -> Result<Vec<u8>>{
        (**self).load(path)
    }
}
