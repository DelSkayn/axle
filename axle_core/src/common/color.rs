//!
//! Module which implements utilties for working with a color
//!

#[derive(Debug,Copy,Clone,PartialEq)]
pub struct Rgba(pub f32, pub f32, pub f32, pub f32);

#[derive(Debug,Copy,Clone,PartialEq)]
pub struct Hsla(pub f32, pub f32, pub f32, pub f32);

#[derive(Debug,Copy,Clone,PartialEq)]
pub struct Rgb(pub f32, pub f32, pub f32);

#[derive(Debug,Copy,Clone,PartialEq)]
pub struct Hsl(pub f32, pub f32, pub f32);

impl From<Rgba> for Hsla{
    fn from(c: Rgba) -> Self{
        let (h,s,l) = rgb_to_hsl(c.0,c.1,c.2);
        Hsla(h,s,l,c.3)
    }
}

impl From<Hsla> for Rgba{
    fn from(c: Hsla) -> Self{
        let (r,g,b) = hsl_to_rgb(c.0,c.1,c.2);
        Rgba(r,g,b,c.3)
    }
}

impl From<Rgb> for Rgba{
    fn from(c: Rgb) -> Self{
        Rgba(c.0,c.1,c.2,1.0)
    }
}

impl From<Hsl> for Rgba{
    fn from(c: Hsl) -> Self{
        let (r,g,b) = hsl_to_rgb(c.0,c.1,c.2);
        Rgba(r,g,b,1.0)
    }
}

impl From<Hsl> for Hsla{
    fn from(c: Hsl) -> Self{
        Hsla(c.0,c.1,c.2,1.0)
    }
}

impl From<Rgb> for Hsla{
    fn from(c: Rgb) -> Self{
        let (h,s,l) = rgb_to_hsl(c.0,c.1,c.2);
        Hsla(h,s,l,1.0)
    }
}

impl From<Rgba> for Rgb{
    fn from(c: Rgba) -> Rgb{
        Rgb(c.0,c.1,c.2)
    }
}

impl From<Hsla> for Rgb{
    fn from(c: Hsla) -> Rgb{
        let r: Rgba = c.into();
        Rgb(r.0,r.1,r.2)
    }
}

impl From<Rgba> for Hsl{
    fn from(c: Rgba) -> Hsl{
        let h: Hsla = c.into();
        Hsl(h.0,h.1,h.2)
    }
}

impl From<Hsla> for Hsl{
    fn from(c: Hsla) -> Hsl{
        Hsl(c.0,c.1,c.2)
    }
}

impl From<Rgb> for (f32,f32,f32){
    fn from(c: Rgb) -> Self{
        (c.0,c.1,c.2)
    }
}

fn rgb_to_hsl(r: f32, g: f32, b: f32) -> (f32, f32, f32) {
    let c_max = r.max(g).max(b);
    let c_min = r.min(g).min(b);
    let c = c_max - c_min;

    let hue = if c == 0.0 {
        0.0
    } else {
        1.04719755 * if      c_max == r { fmod((g - b) / c, 6) }
                     else if c_max == g { ((b - r) / c) + 2.0 }
                     else               { ((r - g) / c) + 4.0 }
    };

    let lightness = (c_max + c_min) / 2.0;
    let saturation = if lightness == 0.0 { 0.0 }
                     else { c / (1.0 - (2.0 * lightness - 1.0).abs()) };
    (hue, saturation, lightness)
}

fn hsl_to_rgb(hue: f32, saturation: f32, lightness: f32) -> (f32, f32, f32) {
    let chroma = (1.0 - (2.0 * lightness - 1.0).abs()) * saturation;
    let hue = hue /  1.04719755;
    let x = chroma * (1.0 - (fmod(hue, 2) - 1.0).abs());
    let (r, g, b) = match hue {
        hue if hue < 0.0 => (0.0, 0.0, 0.0),
        hue if hue < 1.0 => (chroma, x, 0.0),
        hue if hue < 2.0 => (x, chroma, 0.0),
        hue if hue < 3.0 => (0.0, chroma, x),
        hue if hue < 4.0 => (0.0, x, chroma),
        hue if hue < 5.0 => (x, 0.0, chroma),
        hue if hue < 6.0 => (chroma, 0.0, x),
        _ => (0.0, 0.0, 0.0),
    };
    let m = lightness - chroma / 2.0;
    (r + m, g + m, b + m)
}

fn fmod(f: f32, n: i32) -> f32{
    let i = f.floor() as i32;
    (i % n) as f32 + f - i as f32
}
