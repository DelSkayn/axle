use std::sync::atomic::{Ordering, AtomicBool,AtomicUsize};
use std::cell::UnsafeCell;
use std::ops::{Deref, DerefMut};

pub struct SpinLock<T> {
    data: UnsafeCell<T>,
    lock: AtomicBool,
}

unsafe impl<T> Send for SpinLock<T>{}
unsafe impl<T> Sync for SpinLock<T>{}

impl<T> SpinLock<T> {
    pub fn new(t: T) -> Self {
        SpinLock {
            data: UnsafeCell::new(t),
            lock: AtomicBool::new(false),
        }
    }

    pub fn lock<'a>(&'a self) -> Guard<'a, T> {
        while !self.lock.compare_and_swap(false, true, Ordering::AcqRel) {}
        Guard {
            lock: &self.lock,
            data: unsafe { &mut (*self.data.get()) },
        }
    }

    pub fn get_mut(&mut self) -> &mut T {
        unsafe { &mut (*self.data.get()) }
    }

    pub fn into_inner(self) -> T {
        assert!(!self.lock.load(Ordering::Acquire));
        unsafe { self.data.into_inner() }
    }
}

pub struct Guard<'a, T: 'a> {
    lock: &'a AtomicBool,
    data: &'a mut T,
}

impl<'a, T: 'a> Deref for Guard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T {
        self.data
    }
}

impl<'a, T: 'a> DerefMut for Guard<'a, T> {
    fn deref_mut(&mut self) -> &mut T {
        self.data
    }
}

impl<'a, T: 'a> Drop for Guard<'a, T> {
    fn drop(&mut self) {
        self.lock.store(false, Ordering::Release);
    }
}

pub struct RwSpinLock<T>{
    data: UnsafeCell<T>,
    lock: AtomicUsize,
}

impl<T> RwSpinLock<T>{
    pub fn new(t: T) -> Self {
        RwSpinLock {
            data: UnsafeCell::new(t),
            lock: AtomicUsize::new(0),
        }
    }

    pub fn write(&self) -> WriteGuard<T> {
        while self.lock.compare_and_swap(0, usize::max_value(), Ordering::AcqRel) != 0{}
        WriteGuard {
            lock: &self.lock,
            borrow: unsafe { &mut (*self.data.get()) },
        }
    }

    pub fn read(&self) -> ReadGuard<T>{
        loop {
            let current = self.lock.load(Ordering::Acquire);
            if current != usize::max_value()
                && self.lock.compare_and_swap(current,current + 1,Ordering::AcqRel) == current{
                break;
            }
            //worker::work();
        }
        ReadGuard{
            lock: &self.lock,
            borrow: unsafe{ &(*self.data.get()) }
        }
    }

    pub fn try_write(&self) -> Option<WriteGuard<T>> {
        if self.lock.compare_and_swap(0, usize::max_value(), Ordering::AcqRel) == 0{
            Some(WriteGuard {
                lock: &self.lock,
                borrow: unsafe { &mut (*self.data.get()) },
            })
        }else{
            None
        }
    }

    pub fn try_read(&self) -> Option<ReadGuard<T>>{
        let current = self.lock.load(Ordering::Acquire);
        if current != usize::max_value()
            && self.lock.compare_and_swap(current,current + 1,Ordering::AcqRel) == current{
            Some(ReadGuard{
                lock: &self.lock,
                borrow: unsafe{ &(*self.data.get()) }
            })
        }else{
            None
        }
    }

    pub fn get_mut(&mut self) -> &mut T {
        unsafe { &mut (*self.data.get()) }
    }

    pub fn into_inner(self) -> T {
        unsafe { self.data.into_inner() }
    }
}

pub struct WriteGuard<'a,T: 'a>{
    borrow: &'a mut T,
    lock: &'a AtomicUsize,
}

impl<'a,T> Deref for WriteGuard<'a,T>{
    type Target = T;

    fn deref(&self) -> &T{
        self.borrow
    }
}

impl<'a,T> DerefMut for WriteGuard<'a,T>{
    fn deref_mut(&mut self) -> &mut T{
        self.borrow
    }
}

impl<'a,T> Drop for WriteGuard<'a,T>{
    fn drop(&mut self){
        self.lock.store(0,Ordering::Release);
    }
}

pub struct ReadGuard<'a,T: 'a>{
    borrow: &'a T,
    lock: &'a AtomicUsize,
}

impl<'a,T> Deref for ReadGuard<'a,T>{
    type Target = T;

    fn deref(&self) -> &T{
        self.borrow
    }
}

impl<'a,T> Drop for ReadGuard<'a,T>{
    fn drop(&mut self){
        self.lock.fetch_sub(1,Ordering::AcqRel);
    }
}
