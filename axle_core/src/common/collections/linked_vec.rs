
use std::{mem,ops};

pub enum ValueNext<T>{
    Value(T),
    Next(usize),
}

impl<T> ValueNext<T>{
    fn as_value(&self) -> &T{
        match *self{
            ValueNext::Value(ref x) => x,
            _ => panic!("Tried to get a value which was not a value"),
        }
    }

    fn as_mut_value(&mut self) -> &mut T{
        match *self{
            ValueNext::Value(ref mut x) => x,
            _ => panic!("Tried to get a value which was not a value"),
        }
    }

    fn as_next(&self) -> usize{
        match *self{
            ValueNext::Next(x) => x,
            _ => panic!("Tried to get a index which was not a index"),
        }
    }

    fn is_next(&self) -> bool{
        match *self{
            ValueNext::Next(_) => true,
            _ => false,
        }
    }

    fn replace(&mut self,value: T) -> usize{
        if let ValueNext::Next(x) = *self{
            *self = ValueNext::Value(value);
            x
        }else{
            panic!("Value was not free.");
        } }

    fn take(&mut self,value: usize) -> T{
        if let ValueNext::Value(x) = mem::replace(self,ValueNext::Next(value)){
            x
        }else{
            panic!("Value was not used.");
        }
    }
}

/// A data structure which allows stores data in somewhat continuous memory and 
/// allows O(1) insertion, removal and indexing
///
/// Data is stored in a vector and appon removal the empty spot in the array is put on a linked
/// list containing all removed values.
/// Durint insertion and remove the index of existing values are preserved.
/// If a value is inserted while there are empty spots in the vector the empty spots will be filled
/// first.
/// This means that the array will end up with open values after repeated removal.
/// Also in order to keep insertion O(1) values removed last will be the first to be filled again.
/// It might be gooed for performance to call `order_links()` before inserting a bunch of values so
/// that the front of the array will be filled up and thus removing bubles.
pub struct LinkedVec<T>
{
    data: Vec<ValueNext<T>>,
    next: usize,
    len: usize,
    free: usize,
}

impl<T> LinkedVec<T>{
    /// Creates a new const vec object.
    pub fn new() -> Self{
        LinkedVec{
            data: Vec::new(),
            next: usize::max_value(),
            len: 0,
            free: 0,
        }
    }

    /// Inserts a new value in the const vec.
    pub fn insert(&mut self,value: T) -> usize{
        if self.next == usize::max_value() {
            self.data.push(ValueNext::Value(value));
            self.len += 1;
            self.data.len() - 1
        }else{
            let res = self.next;
            self.next = self.data[self.next].replace(value);
            self.free -= 1;
            self.len += 1;
            res
        }
    }

    /// Removes an value from the const vec and returns it.
    pub fn remove(&mut self,index: usize) -> T{
        let x = self.data[index].take(self.next);
        self.next = index;
        self.free += 1;
        self.len -= 1;
        x
    }

    /// Return a reference to the value at index
    /// # Panic
    /// Function will panic if the index is out of bounds or there is no value at the index
    pub fn get(&self, index: usize) -> &T{
        self.data[index].as_value()
    }

    /// Return a reference to the value at index if there is one.
    pub fn try_get(&self, index: usize) -> Option<&T>{
        if index >= self.data.len(){
            None
        }else if let ValueNext::Value(ref x) = self.data[index]{
            Some(x)
        }else{
            None
        }
    }

    /// Return a mutable reference to the value at index
    /// # Panic
    /// Function will panic if the index is out of bounds or there is no value at the index
    pub fn get_mut(&mut self,index: usize) -> &mut T{
        self.data[index].as_mut_value()
    }
    /// Return a mutable reference to the value at index if there is one.
    pub fn try_get_mut(&mut self, index: usize) -> Option<&T>{
        if let ValueNext::Value(ref mut x) = self.data[index]{
            Some(x)
        }else{
            None
        }
    }

    /// Returns whether there is a value at the index.
    pub fn is_present(&self,index: usize) -> bool{
        if index >= self.data.len(){
            false
        } else if let ValueNext::Value(_) = self.data[index]{
            true
        } else{
            false
        }
    }

    /// Will sort the next free links so that the free spot with the lowest index will be used
    /// first. You might want to call this function every so often to make sure that most values
    /// will be at the start of the vector allowing the vector to be cleaned if nessecary.
    pub fn sort_links(&mut self){
        if self.free > 1 {
            let mut buffer = Vec::with_capacity(self.free);
            let mut next = self.next;
            while next != usize::max_value(){
                buffer.push(next);
                next = self.data[next].as_next();
            }
            // change with unstable sort once stablized.
            buffer.sort();
            let mut cur = buffer[0];
            for e in &buffer[1..]{
                self.data[cur] = ValueNext::Next(*e);
                cur = *e;
            }
            self.data[cur] = ValueNext::Next(usize::max_value());
        }
    }

    /// Will try to clean unused values by shrinking the vector.
    /// Also calls the sort links function.
    pub fn clean(&mut self){
        self.sort_links();
        for i in (0..self.data.len()).rev(){
            if self.data[i].is_next() {
                self.data.pop();
                self.free -= 1;
            }else{
                break;
            }
        }

        if self.free == 0{
            self.next = usize::max_value();
        }else{
            for i in (0..self.data.len()).rev(){
                if self.data[i].is_next(){
                    self.data[i] = ValueNext::Next(usize::max_value());
                    self.data.shrink_to_fit();
                    return;
                }
            }
            panic!("Missing a next value to set as the last!");
        }

    }

    /// Returns the amount of values in the array.
    pub fn len(&self) -> usize{
        self.len
    }

    /// Returns the number of free values before i the array.
    pub fn free(&self) -> usize{
        self.free
    }
}

impl<T> ops::Index<usize> for LinkedVec<T>{
    type Output = T;
    fn index(&self,index: usize) -> &T{
        self.get(index)
    }
}

impl<T> ops::IndexMut<usize> for LinkedVec<T>{
    fn index_mut(&mut self,index: usize) -> &mut T{
        self.get_mut(index)
    }
}


