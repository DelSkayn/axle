
/// Struct for containing a lot of boolean values in less memory then Vec<boolean> would use.
#[derive(Debug,Default)]
pub struct BitMap {
    map: Vec<u8>,
    len: usize,
}

impl BitMap {
    pub fn new() -> Self {
        BitMap {
            map: Vec::new(),
            len: 0,
        }
    }

    /// resize the map to contain i bits.
    /// only makes the map larger not smaller.
    pub fn reserve(&mut self, i: usize) {
        self.map.reserve((i / 8) + 1);
        for _ in self.map.len()..((i / 8) + 1) {
            self.map.push(0);
        }
        self.len = i;
    }

    /// resize the map to contain i bits.
    /// only makes the map smaller not bigger.
    pub fn resize(&mut self, i: usize) {
        for _ in ((i / 8) + 1)..self.map.len() {
            self.map.pop();
        }
    }

    /// returns the bit at the given index,
    /// resizes the map to contain enough bits when the size is to small
    pub fn get_resize(&mut self, index: usize) -> bool {
        if index >= self.len {
            self.reserve(index + 1);
        }
        let i = index / 8;
        let map = index % 8;
        let r = self.map[i];
        ((r >> map) & 1) != 0
    }

    pub fn get(&self, index: usize) -> bool{
        if index >= self.len{
            return false;
        }
        let i = index / 8;
        let map = index % 8;
        let r = self.map[i];
        ((r >> map) & 1) != 0
    }

    /// returns the length of the bitmap.
    pub fn len(&self) -> usize {
        self.len
    }

    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    /// Set bit at index to true.
    pub fn set(&mut self, index: usize) {
        if self.get(index) {
            panic!("Tried to sit bit which was already set!");
        }
        let i = index / 8;
        let map = index % 8;
        self.map[i] |= 1 << map;
    }

    /// Set bit at index to false.
    pub fn unset(&mut self, index: usize) {
        if !self.get(index) {
            panic!("Tried to unsit bit which was not set!");
        }
        let i = index / 8;
        let map = index % 8;
        self.map[i] ^= 1 << map;
    }

    /// Removes all unset bit at the end of the map
    /// resizing accordingly.
    /// TODO: increase speed by testing entire u8.
    pub fn clean(&mut self) {
        let mut size = 0;
        for i in (0..self.len()).rev() {
            if self.get(i) {
                size = i + 1;
                break;
            }
        }
        self.resize(size);
    }
}
