
use std::slice::Iter;

use std::ops::{Index,IndexMut};

pub struct PackedVec<T>{
    next_free: usize,
    index: Vec<usize>,
    data: Vec<T>,
}

/// A data struct which stores values in continuous memory by a indirection.
/// The datastructure allows O(1) insertion, removal, and lookup.
/// During insertion and removal indexes of existing values are preserved.
///
/// This all comes at the cost of slower lookup and iteration not guaranteeing than
/// the values will be in any order.
impl<T> PackedVec<T>{
    pub fn new() -> Self{
        PackedVec{
            next_free: usize::max_value(),
            index: Vec::new(),
            data: Vec::new(),
        }
    }

    /// Insert a new value an retrieve the index of the 
    /// new value
    pub fn push(&mut self,value: T) -> usize{
        if self.next_free == usize::max_value(){
            let index = self.index.len();
            self.index.push(self.data.len());
            self.data.push(value);
            index
        } else {
            let next = self.next_free;
            self.next_free = self.index[next];
            unsafe{
                *self.index.get_unchecked_mut(next) = self.data.len();
            }
            self.data.push(value);
            next
        }
    }

    /// Remove a value with a index.
    pub fn remove(&mut self,index: usize) -> T{
        let res = self.data.swap_remove(self.index[index]);
        unsafe{
            *self.index.get_unchecked_mut(index) = self.next_free;
        }
        self.next_free = index;
        res
    }

    pub fn iter(&self) -> Iter<T>{
        self.data.iter()
    }

    pub unsafe fn get_unchecked(&self, index: usize) -> &T{
        self.data.get_unchecked(*self.index.get_unchecked(index))
    }

    pub unsafe fn get_unchecked_mut(&mut self, index: usize) -> &mut T{
        self.data.get_unchecked_mut(*self.index.get_unchecked(index))
    }

    #[inline]
    pub fn get(&self, index: usize) -> &T{
        &self.data[self.index[index]]
    }

    #[inline]
    pub fn get_mut(&mut self, index: usize) -> &mut T{
        &mut self.data[self.index[index]]
    }

    #[inline]
    pub fn len(&self) -> usize{
        self.data.len()
    }

    #[inline]
    pub fn unused_capacity(&self) -> usize{
        self.index.len() - self.len()
    }

}

impl<T> Index<usize> for PackedVec<T>{
    type Output = T;
    fn index(&self, index: usize) -> &T{
        self.get(index)
    }
}

impl<T> IndexMut<usize> for PackedVec<T>{
    fn index_mut(&mut self, index: usize) -> &mut T{
        self.get_mut(index)
    }
}
