//!
//! Module which implements error type which can be used as an overarching error for all errors.
//!

use asset;
use registery;

error_chain!{
    links{
        Asset(asset::Error, asset::ErrorKind);
        Registery(registery::Error, registery::ErrorKind);
    }
}
