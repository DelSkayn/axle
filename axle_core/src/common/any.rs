//!
//! This module defines a type id which is consistant between compilation.
//!
//! The type id is based on the name of the struct and the type of the fields of the struct.
//!

use std::mem;

#[derive(Debug,Copy,Clone,Eq,PartialEq,Hash)]
pub struct TypeId(u64);

impl TypeId{
    pub fn of<T: Any>() -> Self{
        unsafe{
            // should be fine since type_id does not access any of
            // data that is pressent in borrow
            let borrow: &T = mem::transmute(&());
            borrow.type_id()
        }
    }
}

pub trait Any: 'static {
    #[doc(hidden)]
    fn id_u64(&self) -> u64;

    fn type_id(&self) -> TypeId{
        TypeId(self.id_u64())
    }
}
