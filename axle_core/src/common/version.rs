//!
//! Module which defines a functionality for dealing with version
//!

#[derive(Debug,Serialize,Deserialize)]
pub struct Version{
    pub major: u16,
    pub minor: u16,
    pub patch: u32,
}
