extern crate pancurses;

use super::Terminal;

use std::sync::mpsc::{self, Receiver};
use std::thread;
use std::io::{self, BufRead};
use std::sync::atomic::{AtomicBool, Ordering};

pub struct Curses{
    input: Receiver<String>,
    output: Sender<&str>
}

impl Curses{
    pub fn new() -> Curses{
        let
    }
}

impl Terminal for Curses{
    fn read(&self) -> Vec<String>{
        self.input.try_iter().collect()
    }

    fn write(&self, line: &str) {
    }

    fn enable(&self) {
    }

    fn disable(&self){
    }
}
