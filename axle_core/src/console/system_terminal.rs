use super::Terminal;

use std::sync::mpsc::{self, Receiver};
use std::thread;
use std::io::{self, BufRead};
use std::sync::atomic::{AtomicBool, Ordering};

/// TODO switch over to a task thread.
pub struct SystemTerminal {
    input: Receiver<String>,
    enabled: AtomicBool,
}

impl SystemTerminal {
    pub fn new() -> SystemTerminal {
        let (s, r) = mpsc::channel();
        thread::spawn(move || {
            let stdin = io::stdin();
            let mut handle = stdin.lock();
            loop {
                let mut buffer = String::new();
                match handle.read_line(&mut buffer) {
                    Ok(_) => {
                        buffer.pop();
                        match s.send(buffer) {
                            Ok(_) => {}
                            Err(e) => {
                                println!("[Consl] Terminal encountered error: {:?}.", e);
                                return;
                            }
                        };
                    }
                    Err(e) => {
                        println!("[Consl] Terminal encountered error while reading: {:?}.", e)
                    }
                }
            }
        });
        SystemTerminal {
            input: r,
            enabled: AtomicBool::new(true),
        }
    }
}

impl Terminal for SystemTerminal {
    fn read(&self) -> Vec<String> {
        self.input.try_iter().collect()
    }

    fn write(&self, line: &str) {
        if self.enabled.load(Ordering::Acquire) {
            println!("{}", line);
        }
    }

    fn enable(&self) {
        self.enabled.store(true, Ordering::Release);
    }

    fn disable(&self) {
        self.enabled.store(false, Ordering::Release);
    }

    fn is_enabled(&self) -> bool {
        self.enabled.load(Ordering::Acquire)
    }
}
