//!
//! This module deals with logging and managing the console.
//!
// TODO:
//
// * Remove dropped triggered functions.

mod logger;
mod error;
pub use self::logger::Logger;
pub use self::error::*;

/*
mod data;


mod system_terminal;
pub use self::system_terminal::SystemTerminal;
use std::sync::RwLock;

use std::collections::HashMap;

pub mod command;
use self::command::{Parser,ParserError,ToCommand,BuildError};

use std::sync::Arc;

//pub mod predefined;

implement_hotloading!{
    static CONSOLE: Console = Console::new();
}

pub trait Terminal {
    fn read(&self) -> Vec<String>;

    fn write(&self, line: &str);

    fn disable(&self);
    fn enable(&self);
    fn is_enabled(&self) -> bool;
}

pub struct Console {
    functions: RwLock<HashMap<String, Arc<Parser>>>,
    terminal: Box<Terminal>,
}

impl Console {
    fn new() -> Self {
        Logger::init().unwrap();
        println!("{}",data::CONSOLE_ICON_COLOR);
        Console {
            terminal: Box::new(SystemTerminal::new()),
            functions: RwLock::new(HashMap::new()),
        }
    }

    pub fn add_command<T: ToCommand>(command: T) -> Result<(),BuildError>
    {
        let command = command.to_command()?;
        let inter_cmd = Arc::new(command.parser);
        let mut borrow = CONSOLE.functions.write().unwrap();
        for n in command.names{
            borrow.insert(n, inter_cmd.clone());
        }
        Ok(())
    }

    pub fn update() {
        let mut input = CONSOLE.terminal.read();
        if input.len() != 0 {
            let mut should = !CONSOLE.terminal.is_enabled();
            if should {
                CONSOLE.terminal.enable();
            }
            let borrow = CONSOLE.functions.read().unwrap();
            for e in input.drain(..) {
                let mut args = e.split(' ').filter(|e| e.len() > 0);
                let cmd: &str =  if let Some(x) = args.next() { x } else { continue };
                if "log" == cmd {
                    should = !should;
                } else if let Some(parser) = borrow.get(cmd) {
                    match parser.parse(args,&*CONSOLE.terminal){
                        Ok(_) => {},
                        Err(x) => {
                            match x {
                                ParserError::MisingIdentifierAfterStripe => CONSOLE.terminal.write("[Consl] Found a lone \"-\", give it a letter!"),
                                ParserError::InvalidFlag(x) => CONSOLE.terminal.write(&format!("[Consl] Unknown flag \"{}\" for command",x)),
                                ParserError::MissingArgument => CONSOLE.terminal.write("[Consl] Missing Arguments"),
                                ParserError::ToManyArguments => CONSOLE.terminal.write("[Consl] To Many Arguments for command"),
                                ParserError::Help(x) => CONSOLE.terminal.write(&x),
                                ParserError::CouldNotParseArgument(x) => CONSOLE.terminal.write(&format!("[Consl] Invalid input for argument: \"{}\"!", x)),
                                ParserError::MissingSubCommand => CONSOLE.terminal.write("[Consl] Missing a sub command for command"),
                                ParserError::InvalidSubCommand => CONSOLE.terminal.write("[Consl] SubCommand not found"),
                            }
                            continue;
                        }
                    };
                } else {
                    CONSOLE.terminal
                        .write(&format!("[Consl] Function \"{}\" is not defined!", cmd));
                }
            }
            if should {
                CONSOLE.terminal.disable();
            }
        }
    }

    pub fn commands() -> Vec<String>{
        let borrow = CONSOLE.functions.read().unwrap();
        borrow.keys().map(|e| e.to_string()).collect()
    }
}
*/
