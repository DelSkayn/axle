use super::Console;
use super::command::{CommandBuilder, SubCommandBuilder, ArgumentBuilder};
use ::registery::Registery;

pub fn register() {
    let cmd = SubCommandBuilder::new("help")
        .help("Yes you can ask for help for the help command")
        .function(|_, term| {
        term.write("[Consl] This is the command line, verious functionality of the engine can be \
                    accessed from the command line. If you want to the which commands are \
                    available you can use the \"commands\" command.");
        Ok(())
    });
    Console::add_command(cmd).unwrap();
    let cmd = SubCommandBuilder::new("commands").function(|_,term|{
        term.write("[Consl] Commands:");
        for c in Console::commands(){
            term.write(&format!("[Consl]\t\t{}",c));
        }
        Ok(())
    });
    Console::add_command(cmd).unwrap();
    registery_command();
}

pub fn registery_command(){
    let cmd = CommandBuilder::new("registery")
        .sub_command(SubCommandBuilder::new("set")
            .arg(ArgumentBuilder::new("PLACE").help("To place in the registery to set"))
            .arg(ArgumentBuilder::new("VALUE").help("The value to adjust the registery entry to"))
            .function(|matches, term| {
                let which: String = matches.argument("PLACE")?;
                let value: String = matches.argument("VALUE")?;
                Registery::set_str(&which, &value)
                    .map_err(|e| term.write(&format!("Could not write registery value: {:?}",e)))
                    .ok();
                term.write("[Consl] Done!");
                Ok(())
            }))
        .sub_command(SubCommandBuilder::new("get")
            .arg(ArgumentBuilder::new("PLACE").help("The place of the registery to return"))
            .function(|matches, term| {
                let mut which: String = matches.argument("PLACE")?;
                if which == "."{
                    which.pop();
                }
                match Registery::get_str(&which){
                    Ok(x) => {
                        term.write(&format!("[Consl] value: {}",x));
                    }
                    Err(e) => {
                        term.write(&format!("[Consl] Value not found: {:?}",e));
                    }
                }
                Ok(())
            }))
        .help("A command for doing stuff with the registery");
    Console::add_command(cmd).unwrap();
}
