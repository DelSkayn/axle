
use log;
use fern;
use chrono;
use backtrace;

use super::Result;

use std::io::Write;
use std::env;
use std::panic::{self, PanicInfo};


pub struct Logger;

impl Logger {
    /// Initializes the logger
    /// Called once at the start of the engine.
    pub fn init() -> Result<()> {
        let mut filter = if cfg!(debug_assertions) { log::LevelFilter::Debug } else { log::LevelFilter::Error };
        for (key,value) in env::vars(){
            if key == "AXLE_LOG" {
                match value.as_str() {
                    "trace" => filter = log::LevelFilter::Trace,
                    "debug" => filter = log::LevelFilter::Debug,
                    "info" => filter = log::LevelFilter::Info,
                    "warn " => filter = log::LevelFilter::Warn,
                    "error" => filter = log::LevelFilter::Error,
                    "off" => filter = log::LevelFilter::Off,
                    x => { println!("invalid environment variable AXLE_LOG: {}",x) },
                }
            }
        }
        fern::Dispatch::new()
            .chain(fern::log_file("axle_log.log")?)
            .chain(::std::io::stdout())
            .level(filter)
            .format(|out, message,record|{
                out.finish(format_args!(
                        "{}[{}][{}] {}",
                        chrono::Local::now().format("[%H:%M:%S]"),
                        record.target(),
                        record.level(),
                        message
                    ))
            }).apply()?;
        panic::set_hook(Box::new(log_panic));
        Ok(())
    }
}

/// logs a panic and the stacktrace.
fn log_panic(info: &PanicInfo) {
    if let Some(x) = info.location() {
        error!("panic in file \"{}\" at line \"{}\"", x.file(), x.line());
    } else {
        error!("panic from unkown location!");
    };

    if let Some(x) = info.payload().downcast_ref::<&'static str>() {
        error!("payload: {}", x);
    } else if let Some(x) = info.payload().downcast_ref::<String>() {
        error!("payload: {}", x);
    }

    let mut backtrace = Vec::<u8>::new();
    backtrace::trace(|frame| {
        let ip = frame.ip();
        backtrace::resolve(ip, |sym| {
            let name = sym.name()
                .map_or("! unknown !", |x| x.as_str().unwrap_or("! Error !"));

            let line = sym.lineno()
                .map_or(-1, |x| x as i64);

            let filename = sym.filename()
                .map_or("! unknown !", |x| x.to_str().unwrap_or("! Error !"));

            write!(&mut backtrace,
                   "[\x1b[31mbacktrace\x1b[0m] --> {}\n            file {}:{}\n\n",
                   name,
                   filename,
                   line)
                .is_ok();
        });
        true
    });
    error!("         ##Backtrace##\n{}",
           String::from_utf8(backtrace).unwrap_or("error during backtrace generation".to_string()));
}
