use crossbeam::sync::MsQueue;
use super::CONSOLE;
use super::Terminal; use std::collections::{HashSet, HashMap};
use std::sync::Arc;

use std::str::FromStr;

#[derive(Debug)]
pub struct TriggeredFunction(Arc<MsQueue<Matches>>);

impl TriggeredFunction {
    pub fn new() -> Self {
        TriggeredFunction(Arc::new(MsQueue::new()))
    }

    pub fn call<F>(&self, f: F)
        where F: FnOnce(Matches, &Terminal) -> Result<(), ParserError>
    {
        if let Some(x) = self.0.try_pop() {
            match f(x, &*CONSOLE.terminal) {
                Err(ParserError::MissingArgument) => {
                    CONSOLE.terminal.write("[Consl] Missing Arguments")
                }
                _ => {}
            }
        }
    }
}

pub struct Command {
    pub names: Vec<String>,
    pub parser: Parser,
}

pub type CommandFunc = Fn(Matches, &Terminal) -> Result<(), ParserError> + Send + Sync + 'static;

pub trait ToCommand {
    fn to_command(self) -> Result<Command, BuildError>;
}

#[derive(Debug,Copy,Clone)]
pub enum BuildError {
    ConfilictingIndex,
    OptionalInputBeforeRequired,
    MissingFunction,
}

pub struct CommandBuilder {
    name: Vec<String>,
    help: Option<String>,
    sub_commands: Vec<SubCommandBuilder>,
}

impl CommandBuilder {
    pub fn new<T: Into<String>>(name: T) -> Self {
        CommandBuilder {
            name: vec![name.into()],
            sub_commands: Vec::new(),
            help: None,
        }
    }
    pub fn help<T: Into<String>>(mut self, help: T) -> Self {
        self.help = Some(help.into());
        self
    }

    pub fn sub_command(mut self, sub: SubCommandBuilder) -> Self {
        self.sub_commands.push(sub);
        self
    }
}

impl ToCommand for CommandBuilder {
    fn to_command(self) -> Result<Command, BuildError> {
        let mut help = String::new();
        help.push_str("Help for command: ");
        help.push_str(&self.name[0]);
        help.push('\n');
        if let Some(x) = self.help.as_ref() {
            help.push_str(x.as_str());
        }
        help.push_str("\n\n[USAGE]\n\t");
        help.push_str(&self.name[0]);
        help.push_str(" [SUBCOMMAND]\n\nSUBCOMMAND:\n");
        for com in self.sub_commands.iter() {
            help.push('\t');
            help.push_str(&com.name[0]);
            help.push_str("\t\t");
            if let Some(h) = com.help.as_ref() {
                help.push_str(h);
            }
            help.push('\n');
        }

        let mut sub = HashMap::with_capacity(self.sub_commands.len());

        for s in self.sub_commands {
            sub.insert(s.name[0].clone(), s.to_command()?.parser);
        }

        let parser = Parser::SubParser {
            sub_commands: sub,
            help: help,
        };
        Ok(Command {
               names: self.name,
               parser: parser,
           })
    }
}

pub struct SubCommandBuilder {
    name: Vec<String>,
    flags: Vec<FlagBuilder>,
    arguments: Vec<ArgumentBuilder>,
    func: Option<Box<CommandFunc>>,
    help: Option<String>,
}

impl SubCommandBuilder {
    pub fn new<T: Into<String>>(name: T) -> Self {
        SubCommandBuilder {
            name: vec![name.into()],
            arguments: Vec::new(),
            func: None,
            flags: Vec::new(),
            help: None,
        }
    }

    pub fn help<T: Into<String>>(mut self, name: T) -> Self {
        self.help = Some(name.into());
        self
    }

    pub fn alias(mut self, name: String) -> Self {
        self.name.push(name);
        self
    }

    pub fn flag(mut self, flag: FlagBuilder) -> Self {
        self.flags.push(flag);
        self
    }

    pub fn arg(mut self, argument: ArgumentBuilder) -> Self {
        self.arguments.push(argument);
        self
    }

    pub fn function<T>(mut self, t: T) -> Self
        where T: Fn(Matches, &Terminal) -> Result<(), ParserError> + Send + Sync + 'static
    {
        self.func = Some(Box::new(t));
        self
    }

    pub fn trigger(mut self, trigger: &TriggeredFunction) -> Self {
        let clone = trigger.0.clone();
        self.func = Some(Box::new(move |m, _| {
                                      if Arc::strong_count(&clone) == 1 {
                                          CONSOLE.terminal.write("[Consl] Triggered function no longer available");
                                      } else {
                                          clone.push(m);
                                      }
                                      Ok(())
                                  }));
        self
    }

    fn gen_usage(&self, w: &mut String) {
        w.push_str(" [FLAGS]");
        for args in self.arguments.iter() {
            w.push_str(" <");
            w.push_str(&args.name);
            w.push('>');
        }
        w.push('\n');
    }

    fn gen_flags(&self, w: &mut String) {
        w.push_str("FLAGS:\n");
        w.push_str("\t-h,--help\t\t\tPrint this message");
        for flag in self.flags.iter() {
            flag.gen_help(w);
        }
    }

    fn gen_args(&self, w: &mut String) {
        w.push_str("ARGUMENT:\n");
        for arg in self.arguments.iter() {
            arg.gen_help(w);
        }
    }
}

impl ToCommand for SubCommandBuilder {
    fn to_command(self) -> Result<Command, BuildError> {
        if self.func.is_none() {
            return Err(BuildError::MissingFunction);
        }
        let mut help = String::new();
        help.push_str("Help for Command: ");
        help.push_str(&self.name[0]);
        help.push('\n');
        if let Some(x) = self.help.as_ref() {
            help.push_str(x);
        }
        help.push_str("\nUSAGE:\n\t");
        help.push_str(&self.name[0]);
        self.gen_usage(&mut help);
        help.push('\n');
        self.gen_flags(&mut help);
        help.push('\n');
        self.gen_args(&mut help);


        let parser = Parser::Root {
            flags_short: self.flags
                .iter()
                .filter(|e| e.short.is_some())
                .map(|e| (e.short.unwrap(), e.name.clone()))
                .collect(),

            flags: self.flags.iter().map(|e| e.name.clone()).collect(),

            input: self.arguments.iter().map(|e| e.name.clone()).collect(),
            func: self.func.unwrap(),
            help: help,
        };

        Ok(Command {
               names: self.name,
               parser: parser,
           })
    }
}

#[derive(Debug)]
pub struct FlagBuilder {
    name: String,
    short: Option<char>,
    //    input: bool,
    help: Option<String>,
}

impl FlagBuilder {
    pub fn new<T: Into<String>>(name: T) -> Self {
        FlagBuilder {
            name: name.into(),
            short: None,
            help: None,
        }
    }

    pub fn short(mut self, short: char) -> Self {
        self.short = Some(short);
        self
    }

    pub fn help<T: Into<String>>(mut self, help: T) -> Self {
        self.help = Some(help.into());
        self
    }


    /*    pub fn takes_input(mut self) -> Self{
        self.input = true;
        self
    }*/

    fn gen_help(&self, w: &mut String) {
        if let Some(help) = self.help.as_ref() {
            if let Some(short) = self.short.as_ref() {
                w.push_str(&format!("\t-{},--{}\t\t\t{}", short, self.name, help));
            } else {
                w.push_str(&format!("\t--{}\t\t\t{}", self.name, help));
            }
        }
        if let Some(short) = self.short.as_ref() {
            w.push_str(&format!("\t-{},--{}", short, self.name));
        } else {
            w.push_str(&format!("\t--{}", self.name));
        }
    }
}

/// Struct which is used for creating a argument of the command
#[derive(Debug)]
pub struct ArgumentBuilder {
    name: String,
    //required: bool,
    help: Option<String>,
}

impl ArgumentBuilder {
    pub fn new<T: Into<String>>(name: T) -> Self {
        ArgumentBuilder {
            name: name.into(),
            //required: false,
            help: None,
        }
    }

    /*
    pub fn required(mut self,which: bool) -> Self{
        self.required = which;
        self
    }
    */

    pub fn help<T: Into<String>>(mut self, help: T) -> Self {
        self.help = Some(help.into());
        self
    }

    fn gen_help(&self, w: &mut String) {
        w.push('\t');
        w.push_str(&self.name);
        if let Some(help) = self.help.as_ref() {
            w.push('\t');
            w.push('\t');
            w.push_str(help);
            w.push('\n');
        }
    }
}

/// Struct which contains the arguments given to the command.
#[derive(Debug)]
pub struct Matches {
    flags: HashMap<String, u16>,
    args: HashMap<String, String>,
}

impl Matches {
    pub fn flag_present(&self, flag: &str) -> bool {
        self.flags.contains_key(flag)
    }

    pub fn argument<F: FromStr>(&self, name: &str) -> Result<F, ParserError> {
        self.args
            .get(name)
            .unwrap()
            .parse()
            .map_err(|_| ParserError::CouldNotParseArgument(name.to_string()))
    }
}

#[derive(Debug,Clone)]
pub enum ParserError {
    /// Help was requested print given string
    Help(String),
    /// Command was missing an argument.
    MissingArgument,
    /// Found a flag which was not part of the specified flags of the command
    InvalidFlag(String),
    /// Found to many arguments
    ToManyArguments,
    /// Found a stripe (AKA "-") without a specifier like "-v" 
    MisingIdentifierAfterStripe,
    /// Missing a subcommand for a function which requires a sub command.
    MissingSubCommand,
    /// Found a subcommand which was not specified in the function
    InvalidSubCommand,
    /// Could not parse a given input to the proper type.
    CouldNotParseArgument(String),
}

/// A build command
pub enum Parser {
    Root {
        flags_short: HashMap<char, String>,
        flags: HashSet<String>,
        input: Vec<String>,
        func: Box<CommandFunc>,
        help: String,
    },
    SubParser {
        sub_commands: HashMap<String, Parser>,
        help: String,
    },
}

impl Parser {
    pub fn parse<'a, I>(&self, mut words: I, term: &Terminal) -> Result<(), ParserError>
        where I: Iterator<Item = &'a str>
    {
        match *self {
            Parser::Root {
                ref flags_short,
                ref flags,
                ref input,
                ref func,
                ref help,
            } => {
                let mut res = Matches {
                    flags: HashMap::new(),
                    args: HashMap::new(),
                };
                for word in words {
                    if word == "-h" || word == "--help" {
                        // show the help for the commands
                        // TODO make a desision on wether we want to print help here on in the
                        // console.
                        return Err(ParserError::Help(help.clone()));
                    } else if word.starts_with("--") {
                        // Parsing of long commands.
                        if word[2..].len() == 0 {
                            return Err(ParserError::MisingIdentifierAfterStripe);
                        }
                        if flags.contains(&word[2..]) {
                            if let Some(x) = res.flags.get_mut(&word[2..]) {
                                *x += 1;
                                continue;
                            }
                            res.flags.insert(word[2..].to_string(), 1);
                        } else {
                            return Err(ParserError::InvalidFlag(word[2..].to_string()));
                        }
                    } else if word.starts_with('-') {
                        // Parsing of single commands.
                        let mut itt = word.chars();
                        itt.next();
                        let ident = if let Some(x) = itt.next() {
                            x
                        } else {
                            return Err(ParserError::MisingIdentifierAfterStripe);
                        };
                        if let Some(x) = flags_short.get(&ident) {
                            if let Some(x) = res.flags.get_mut(x) {
                                *x += 1;
                                continue;
                            }
                            res.flags.insert(x.to_string(), 1);
                        } else {
                            return Err(ParserError::InvalidFlag(word[2..].to_string()));
                        }
                    } else {
                        // Input
                        let len = res.args.len();
                        if len < input.len() {
                            res.args.insert(input[len].clone(), word.to_string());
                        } else {
                            return Err(ParserError::ToManyArguments);
                        }
                    }
                }
                if res.args.len() != input.len() {
                    Err(ParserError::MissingArgument)
                } else {
                    func(res, term)
                }
            }
            Parser::SubParser {
                ref sub_commands,
                ref help,
            } => {
                if let Some(sub_command) = words.next() {
                    if sub_command == "-h" || sub_command == "--help" {
                        return Err(ParserError::Help(help.clone()));
                    }
                    match sub_commands.get(sub_command) {
                        Some(x) => x.parse(words, term),
                        None => Err(ParserError::InvalidSubCommand),
                    }
                } else {
                    Err(ParserError::MissingSubCommand)
                }
            }
        }
    }
}
