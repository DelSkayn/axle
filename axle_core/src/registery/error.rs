
use serde_json;

error_chain!{
    errors{
        /// An error which is returned when a value is not found in the registery.
        NotFound {
            description("value was not found in registery"),
        }

        /// An error which happens when there is a mismatch between the type requested and the one
        /// found in the registery.
        TypeMismatch {
            description("there was a type mismatch between what value was requested and what was found"),
        }

        /// An error which happens when deserializing a map which has a key which is not a string.
        KeyMustBeAString {
            description("A key in a map must be a string but is not")
        }

        /// An error which happens when a keyword used in the registery format is misused.
        InvalidKeyword {
            description("A key word used in the registery format was used in an invalid way")
        }

        /// An error which happens when parsing a file.
        FileParseError (path: String) {
            description("An error happend while parsing a registery file"),
            display("An error happend while parsing registery file {}",path),
        }

    }
    foreign_links{
        SerdeJson(serde_json::Error);
        Io(::std::io::Error);
    }
}
