use std::collections::BTreeMap;
use std::path::{Path,PathBuf};
use std::fs::File;
use serde_json::{self,Value as JsonValue};
use serde::Serialize;

use super::{Result,Error,ResultExt,ErrorKind};

mod de;
mod ser;
//
// This is here to remined me why I need to implement Deserialize for Value eventough it should be
// obvious, I keep forgetting:
//
// In order to be able to use things like value and being able to deserialize into structs we need these implemententations.


#[derive(PartialEq,Debug,Clone)]
pub enum Value{
    Float(f64),
    Integer(i64),
    String(String),
    Bool(bool),
    Array(Vec<Value>),
    Object(BTreeMap<String,Value>),
    /// The values in this tree are stored in an other file then the parent values are stored in.
    /// In the config file the external file is referenced with a path relative to the current
    /// file. The path here is relative to the working dir at time of loading the config file.
    External(PathBuf,Box<Value>),
    Null,
}

impl Value {
    /// Returns a value of the Object variant with no fields
    pub fn empty_object() ->  Self{
        Value::Object(BTreeMap::new())
    }

    /// Returns a the value at a given path
    /// A path here is a set of names with no space delimited by '.'
    /// Example: 'some_field.other_field.a_value'
    pub fn path(&self,path: &str) -> Option<&Value>{
        if path.find(' ').is_some(){
            panic!("Spaces are not allowed in a registery path");
        }

        let location = path.find('.');
        let (first,last) = if let Some(location) = location {
            let (first,rest) = path.split_at(location);
            (first,&rest[1..])
        }else{
            if path.len() == 0 {
                return Some(self);
            }
            (path,"")
        };

        match *self {
            Value::Array(ref x) => {
                if let Ok(index) = first.parse::<usize>(){
                    if index < x.len(){
                        let value: Option<&Value> = x[index].path(last);
                        return value;
                    }
                }            
            },
            Value::Object(ref x) => {
                if let Some(x) = x.get(first){
                      return x.path(last);
                }
            },
            Value::External(_,ref x) => {
                  // External is mearly a marker and takes no part in the path
                  return x.path(path);
            }
            _ => {} 
        }
        None
    }

    pub fn path_mut(&mut self,path: &str) -> Option<&mut Value>{
        if path.find(' ').is_some(){
            panic!("Spaces are not allowed in a registery path");
        }

        let location = path.find('.');
        let (first,last) = if let Some(location) = location {
            let (first,rest) = path.split_at(location);
            (first,&rest[1..])
        }else{
            if path.len() == 0 {
                return Some(self);
            }
            (path,"")
        };

        match *self {
            Value::Array(ref mut x) => {
                if let Ok(index) = first.parse::<usize>(){
                    if index < x.len(){
                        return x[index].path_mut(last);
                    }
                }            
            },
            Value::Object(ref mut x) => {
                if let Some(x) = x.get_mut(first){
                   return x.path_mut(last);
                }
            },
            Value::External(_,ref mut x) => {
                // External is mearly a marker and takes no part in the path
                return x.path_mut(path);
            }
            _ => {} 
        }
        None
    }

    /// Tries to insert a value into a path on an existing path.
    /// Will only succeed if the value is structuraly simular
    pub fn insert_path<S: Serialize>(&mut self,file_path: &Path,path: &str,s: S) -> Result<()>{
        if path.find(' ').is_some(){
            panic!("Spaces are not allowed in a registery path");
        }

        if let Some(location) = path.find('.'){
            let (first,rest) = path.split_at(location);
            // Remove the '.'
            let rest = &rest[1..];
            match *self{
                Value::Array(ref mut x) => {
                    if let Ok(idx) = first.parse::<usize>(){
                        if idx < x.len(){
                            return x[idx].insert_path(file_path,rest,s);
                        }
                    }
                },
                Value::Object(ref mut x) => {
                    if let Some(x) = x.get_mut(first){
                        return x.insert_path(file_path,rest,s);
                    }
                    // Value not found creating a temp value
                    // Wait till everthing finishes
                    // If everything is okay add the temp value to the object
                    let mut temp_value = Value::empty_object();
                    match temp_value.insert_path(file_path,rest,s){
                        Ok(()) => {
                            x.insert(first.to_string(),temp_value);
                            return Ok(())
                        },
                        x => return x,
                    }
                },
                Value::External(_,ref mut x) => {
                    return x.insert_path(file_path,path,s);
                }
                _ => {},
            }
        }else{
            match *self{
                Value::Array(ref mut x) => {
                    if let Ok(idx) = path.parse::<usize>(){
                        if idx < x.len(){
                            let value = Value::from_json_value(None,serde_json::to_value(s)?)?;
                            if x[idx].structural_eq(&value) {
                                x[idx].replace(value);
                                return Ok(());
                            }
                        };
                    }
                },
                Value::Object(ref mut x) => {
                    if let Some(x) = x.get_mut(path){
                        let value = Value::from_json_value(None,serde_json::to_value(s)?)?;
                        if x.structural_eq(&value){
                            x.replace(value);
                            return Ok(());
                        }
                        bail!(ErrorKind::NotFound);
                    }
                    // Value not found so it can be inserted
                    let value = Value::from_json_value(None,serde_json::to_value(s)?)?;
                    x.insert(path.to_string(),value);
                    return Ok(());
                },
                Value::External(ref p,ref mut x) => {
                    return x.insert_path(p,path,s);
                },
                _ => {}
            }
        }
        bail!(ErrorKind::NotFound)
    }

    /// Tries to insert a value as a json string into a path on an existing path.
    /// Will only succeed if the value is structuraly simular
    pub fn insert_path_str(&mut self,file_path: &Path,path: &str, value: &str) -> Result<()>{
        if path.find(' ').is_some(){
            panic!("Spaces are not allowed in a registery path");
        }

        if let Some(location) = path.find('.'){
            let (first,rest) = path.split_at(location);
            // Remove the '.'
            let rest = &rest[1..];
            match *self{
                Value::Array(ref mut x) => {
                    if let Ok(idx) = first.parse::<usize>(){
                        if idx < x.len(){
                            return x[idx].insert_path_str(file_path,rest,value);
                        }
                    }
                },
                Value::Object(ref mut x) => {
                    if let Some(x) = x.get_mut(first){
                        return x.insert_path_str(file_path,rest,value);
                    }
                    // Value not found creating a temp value
                    // Wait till everthing finishes
                    // If everything is okay add the temp value to the object
                    let mut temp_value = Value::empty_object();
                    match temp_value.insert_path_str(file_path,rest,value) {
                        Ok(()) => {
                            x.insert(first.to_string(),temp_value);
                            return Ok(())
                        },
                        x => return x,
                    }
                },
                Value::External(ref p,ref mut x) => {
                    return x.insert_path_str(p,path,value);
                }
                _ => {},
            }
        }else{
            match *self{
                Value::Array(ref mut x) => {
                    if let Ok(idx) = path.parse::<usize>(){
                        if idx < x.len(){
                            let value = Value::from_json_value(None,serde_json::from_str(value)?)?;
                            if x[idx].structural_eq(&value) {
                                x[idx].replace(value);
                                return Ok(());
                            }
                        };
                    }
                },
                Value::Object(ref mut x) => {
                    if let Some(x) = x.get_mut(path){
                        let value = Value::from_json_value(None,serde_json::from_str(value)?)?;
                        if x.structural_eq(&value){
                            x.replace(value);
                            return Ok(());
                        }
                        bail!(ErrorKind::NotFound);
                    }
                    // Value not found so it can be inserted
                    let value = Value::from_json_value(None,serde_json::from_str(value)?)?;
                    x.insert(path.to_string(),value);
                    return Ok(());
                },
                Value::External(ref p,ref mut x) => {
                    return x.insert_path_str(p,path,value);
                },
                _ => {},
            }
        }
        bail!(ErrorKind::NotFound)
    }

    pub fn from_file<P: AsRef<Path>>(path: P) -> Result<Value>{
        let path = path.as_ref();
        let file = File::open(path)?;
        let serde_value = serde_json::from_reader(file).chain_err(|| {
                let path = path.to_str().unwrap_or("could not convert path!");
                ErrorKind::FileParseError(path.to_string())
            })?;
        Value::from_json_value(Some(path.parent().unwrap()),serde_value)
    }

    pub fn to_value<S: Serialize>(value: S) -> Result<Value>{
        Value::from_json_value(None,serde_json::to_value(value)?)
    }

    pub fn from_json_string(value: &str) -> Result<Value>{
        Value::from_json_value(None,serde_json::from_str(value)?)
    }

    fn from_json_value(path: Option<&Path>,value: JsonValue) -> Result<Value>{
        match value{
            JsonValue::Null => {
                Ok(Value::Null)
            }
            JsonValue::Bool(b) => {
                Ok(Value::Bool(b))
            },
            JsonValue::Number(n) => {
                if n.is_i64() {
                    Ok(Value::Integer(n.as_i64().unwrap()))
                }else{
                    Ok(Value::Float(n.as_f64().unwrap()))
                }
            }
            JsonValue::String(s) => {
                Ok(Value::String(s))
            },
            JsonValue::Array(a) => {
                let mut vec = Vec::with_capacity(a.len());
                let iter = a.into_iter().map(|e| Value::from_json_value(path,e));
                for val in iter{
                    vec.push(val?);
                }
                Ok(Value::Array(vec))
            },
            JsonValue::Object(o) => {
                if let Some(x) = o.get("extern") {
                    if path.is_none(){
                        panic!("Serializing value for registery used the field extern! Dont do that!");
                    }
                    let path = path.unwrap();

                    ensure!(o.len() != 1,
                        Error::from_kind(ErrorKind::InvalidKeyword)
                            .chain_err(|| ErrorKind::Msg("Extern should be the only key in an object".to_string()))
                    );

                    let path_extern = match *x {
                        JsonValue::String(ref s) => s,
                        _ => bail!(
                            Error::from_kind(ErrorKind::InvalidKeyword)
                                .chain_err(|| ErrorKind::Msg("Extern path is not a string.".to_string()))),
                    };

                    let path = path.join(path_extern).canonicalize()
                        .map_err(|e| Error::from_kind(ErrorKind::InvalidKeyword)
                                    .chain_err(||ErrorKind::Msg(format!("Extern path not a proper path: {:?},{:?}",path_extern,e))))?;

                    match Value::from_file(&path){
                        Ok(x) => return Ok(Value::External(path,Box::new(x))),
                        Err(e) => {
                            warn!("Error while loading external registery file: '{}' setting external to empty",e);
                            return Err(e);
                        }
                    }
                }
                let iter = o.into_iter().map(|(s,v)| (s,Value::from_json_value(path,v)));
                let mut map = BTreeMap::new();
                for (k,v) in iter{
                    map.insert(k,v?);
                }
                Ok(Value::Object(map))
            },
        }
    }

    fn replace(&mut self,mut value: Value){
        self.copy_external(&mut value);
        *self = value;
    }

    fn copy_external(&mut self,value: &mut Value){
        match *self{
            Value::Object(ref mut m1) => {
                match *value {
                    Value::Object(ref mut m2) => {
                        for (key,value) in m1.iter_mut(){
                            if let Some(x) = m2.get_mut(key){
                                value.copy_external(x);
                            }
                        }
                    }
                    _ => {}
                }
            },
            Value::Array(ref mut array1) => {
                match *value {
                    Value::Array(ref mut array2) => {
                        array1.iter_mut()
                            .zip(array2.iter_mut())
                            .map(|(v1,v2)| v1.copy_external(v2))
                            .last();
                    }
                    _ => {}
                }
            },
            Value::External(ref p,ref mut m1) => {
                let mut new = value.clone();
                m1.copy_external(&mut new);
                *value = Value::External(p.clone(),Box::new(new));
            }
            _ => {},
        }
    }

    ///Checks if the types in the value are the same as the one in the argument.
    ///Does not check missing or addition values
    fn structural_eq(&self,value: &Value) -> bool{
        match *self {
            Value::Integer(_) => {
                match *value { 
                    Value::Integer(_) => true, 
                    _ => false
                }
            },
            Value::Float(_) => {
                match *value{
                    Value::Float(_) => true,
                    _ => false
                }
            },
            Value::Bool(_) => {
                match *value {
                    Value::Bool(_) => true,
                    _ => false
                }
            },
            Value::String(_) => {
                match *value {
                    Value::String(_) => true,
                    _ => false
                }
            },
            Value::Null => {
                match *value {
                    Value::Null => true,
                    _ => false
                }
            }
            Value::Object(ref m1) => {
                match *value {
                    Value::Object(ref m2) => {
                        for (key,value) in m1.iter(){
                            let is_eq = m2.get(key)
                                .map(|v| value.structural_eq(v))
                                .unwrap_or(false);

                            if !is_eq{
                                return false;
                            }
                        }
                        true
                    }
                    _ => false,
                }
            },
            Value::Array(ref array1) => {
                match *value {
                    Value::Array(ref array2) => {
                        if array1.len() != array2.len(){
                            return false;
                        }
                        for (value1,value2) in array1.iter().zip(array2.iter()){
                            if !value1.structural_eq(value2){
                                return false;
                            }
                        }
                        true
                    }
                    _ => false,
                }
            },
            Value::External(_,ref value) => {
                value.structural_eq(value)
            },
        }
    }
}
