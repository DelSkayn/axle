
use super::*;

#[test]
fn basic_use(){
    env_logger::init().unwrap();
    let registery = Registery::from_file("./res/test/registery/basic_use.json");
    println!("{:#?}",registery);
    assert_eq!(registery.get::<u64>("value").unwrap(),3u64);
    assert_eq!(registery.get::<bool>("nested.some_nested_value").unwrap(),false);
    assert_eq!(registery.get::<String>("otherfile.Hello").unwrap(),"World");
}
