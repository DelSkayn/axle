use std::cell::{Cell,UnsafeCell};
use std::sync::Arc;
use std::ops::{Deref,DerefMut};

use ::serde::Deserialize;
use super::{Result,Error,Version,Registery};
use std::result::Result as StdResult;

// TODO better name?
/// A object containing a value from the registery.
/// Will automaticly update the value if the registery changed.
pub struct LazyValue<T> 
    where T: for<'a> Deserialize<'a>
{
    registery: Arc<Registery>,
    version: Cell<Version>,
    value: UnsafeCell<T>,
    path: String,
}

impl<T>  LazyValue<T>
    where T: for<'a> Deserialize<'a>
{
    /// Create a new value object from path.
    pub fn new(registery: Arc<Registery>,path: &str) -> Result<Self>{
        let version = registery.version();
        let value = registery.get(path)?;
        Ok(LazyValue{
            registery: registery,
            version: Cell::new(version),
            value: UnsafeCell::new(value),
            path: path.to_string(),
        })
    }

    /// Returns the local version.
    /// Can be used to check if the value is updated.
    /// Is updated when the value is derefed.
    pub fn version(&self) -> Version{
        self.version.get()
    }
}

impl<T>  LazyValue<T>
    where T: for<'a> Deserialize<'a> + Default
{
    /// Try to create a value from path otherwise return a value object with a 
    /// default value.
    pub fn new_or_default(registery: Arc<Registery>,path: &str) -> StdResult<Self,(Self,Error)>{
        match Self::new(registery.clone(),path){
            Err(e) => {
                Err((LazyValue{
                    version: Cell::new(registery.version()),
                    registery: registery,
                    value: UnsafeCell::new(Default::default()),
                    path: path.to_string(),
                },e))
            }
            Ok(x) => Ok(x),
        }
    }
}

impl<T> Deref for LazyValue<T>
    where T: for<'a> Deserialize<'a> + Default
{
    type Target = T;

    fn deref(&self) -> &T{
        if self.registery.version() != self.version.get(){
            self.version.set(self.registery.version());
            unsafe{
                match self.registery.get(&self.path){
                    Ok(x) => {
                        *self.value.get() = x;
                    },
                    Err(e) => {
                        warn!("Registery value failed to recieve changed value: {:?}",e)
                    }
                }
            }
        }
        unsafe{&*self.value.get()}
    }
}

impl<T> DerefMut for LazyValue<T>
    where T: for<'a> Deserialize<'a> + Default
{

    fn deref_mut(&mut self) -> &mut T{
        if self.registery.version() != self.version.get(){
            self.version.set(self.registery.version());
            unsafe{
                match self.registery.get(&self.path){
                    Ok(x) => {
                        *self.value.get() = x;
                    },
                    Err(e) => {
                        warn!("Registery value failed to recieve changed value: {:?}",e)
                    }
                }
            }
        }
        unsafe{&mut *self.value.get()}
    }
}
