//!
//! The registery is the place which holds the configuration of the engine.
//!
//! It allows for specifyng values to be configured in a file.
//! By default this file is called "axle.toml"
//! The registery also contains the values in the cargo.toml.  

use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::RwLock;
use std::path::{Path,PathBuf};
use std::env;

use serde::Serialize;
use serde::de::{Deserialize,DeserializeOwned};


mod value;
mod lazy_value;

#[cfg(test)]
mod test;
mod error;

pub use self::error::*;

use self::value::Value;
pub use self::lazy_value::LazyValue;

/// The default file name for the registery.
const DEFAULT_REGISTERY_PATH: &'static str = "Axle.toml";



/// An object representing a version of the registery.
#[derive(Debug,Clone,Copy,Eq,PartialEq)]
pub struct Version(usize);

/// A trait for creating objects from the registery
pub trait FromRegistery{
    type Config: DeserializeOwned;

    fn from_registery(config: Self::Config) -> Self;
}

/// The struct containing the data of the registery.
#[derive(Debug)]
pub struct Registery {
    // The version, changes when a value is updated
    version: AtomicUsize,
    // Contains the parsed values
    value: RwLock<value::Value>,
    // the file location of the root file,
    root: PathBuf,
}


impl Registery{
    /// Loads the registery from a specific file,
    pub fn from_file<P: AsRef<Path>>(path: P) -> Self {
        let reg_path = path.as_ref().canonicalize().unwrap();
        info!("Opening registery file at: {}", 
                 reg_path.to_str().unwrap_or("Could not parse file path"));

        let mut root_value;

        match Value::from_file(&reg_path) {
            Ok(value) => {
                root_value = match value{
                    Value::Object(table) => {
                        Value::Object(table)
                    }
                    _ => {
                        warn!("Root registery value is not an object! Discarding and using empty object as root instead.");
                        Value::empty_object()
                    }
                };
            },
            Err(e) => {
                warn!("Could not open the registery file: {:?}.",e);
                warn!("Using empty object as registery root instead");
                root_value = Value::empty_object();
            }
        };
        // Load arguments given to the program over the pressent options.
        let mut args = env::args();
        // discard command path
        args.next();
        for argument in args {
            if let Some(location) = argument.find('='){
                let (name,value) = argument.split_at(location);
                // discarding '='
                let value = &value[1..];
                if let Err(e) = root_value.insert_path_str(&reg_path,name,&value){
                    warn!("Error while trying to parse argument: {}",e);
                    warn!("Discarding argument!");
                }
            }else{
                warn!("Invalid argument: {}",argument);
            }
        }
        println!("[Info] Loading of registery complete!");
        Registery {
            version: AtomicUsize::new(0),
            value: RwLock::new(root_value),
            root: reg_path.to_path_buf(),
        }
    }

    fn new() -> Self{
        Registery::from_file(DEFAULT_REGISTERY_PATH)
    }


    /// get the value at a path in the registery.
    /// # argument
    /// The path argument should be simular to a path in toml.
    /// Eg: indentifiers seperated by dots.
    /// A dot specifies a entry in a table.
    /// TODO: Array support: window.0
    /// # return
    /// function tries to return a serialized version of the data pressent at the given path.
    pub fn get<T>(&self,path: &str) -> Result<T>
        where T: for <'a> Deserialize<'a> 
    {
        let borrow = self.value.read().unwrap();
        if let Some(x) = borrow.path(path){
            Ok(T::deserialize(x)?)
        }else{
            bail!(ErrorKind::NotFound)
        }
    }

    pub fn from<F: FromRegistery>(&self,path: &str) -> Result<F>{
        self.get::<F::Config>(path).map(|e| F::from_registery(e))
    }

    /// Returns a object representing a version
    /// Versions can be used to check if any change has happend in the registery.
    /// This is a lot faster then comparing values recieved from get.
    pub fn version(&self) -> Version{
        Version(self.version.load(Ordering::Acquire))
    }

    /// Set a value at a path in the registery.
    /// It is not allowed to change the type of existing values in the registery.
    /// Will change the version if the funtion executes without errors.
    pub fn set<T: Serialize>(&self,path: &str, value: T) -> Result<()>{
        let mut borrow = self.value.write().unwrap();
        borrow.insert_path(&self.root,path,value)?;
        self.version.fetch_add(1, Ordering::AcqRel);
        Ok(())
    }

    /// Simular to set however the value may be a string formated as a toml value.
    /// It is not allowed to change the type of existing values in the registery.
    /// Will change the version if the funtion executes without errors.
    pub fn set_str(&self,path: &str,value: &str) -> Result<()>{
        let mut borrow = self.value.write().unwrap();
        borrow.insert_path_str(&self.root,path,value)?;
        self.version.fetch_add(1, Ordering::AcqRel);
        Ok(())
    }

    /// Write the registery as it is currently to a file
    /// # argument
    /// The argument should be a path to file which may or may not exist in a existing directory.
    pub fn write<P: AsRef<Path>>(_path: P) -> Result<()> {
        unimplemented!();
        /*
        let path = path.as_ref();

        let mut file = File::create(path)?;
        let borrow = REGISTERY.value.read().unwrap();
        let buffer = toml::to_vec(&*borrow)?;
        file.write_all(&buffer)?;
        Ok(())
        */
    }
}
