#![crate_type = "bin"]
#![allow(dead_code)]
#![allow(unused_imports)]

//! This crate implements the axle command line tool
//! uses for managing axle projects.

extern crate axle_core;
extern crate clap;
#[macro_use]
extern crate log;
extern crate axle_import;
use clap::{Arg, App, SubCommand,AppSettings};

use std::{io,fmt};

mod new;
mod env;
mod import;
mod workspace;
mod consts;
mod logger;

use self::env::{EnvironmentBuilder,Environment};

#[derive(Debug)]
enum Error{
    Io(io::Error),
    Env(env::Error),
    Import(import::Error),
}

impl From<io::Error> for Error{
    fn from(e: io::Error) -> Self{
        Error::Io(e)
    }
}

impl From<env::Error> for Error{
    fn from(e: env::Error) -> Self{
        Error::Env(e)
    }
}

impl From<import::Error> for Error{
    fn from(e: import::Error) -> Self{
        Error::Import(e)
    }
}

impl fmt::Display for Error{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::Io(ref e) => write!(f,"Io error: {}", e),
            Error::Env(ref e) => write!(f,"Environment error: {}", e),
            Error::Import(ref e) => write!(f,"Import error: {}",e),
        }
    }
}

fn main(){
    match run(){
        Ok(_) => {},
        Err(x) => println!("Error while executing command: {:#?}",x),
    }
}

fn run() -> Result<(),Error>{
    let app = App::new("Axle Command-line Tool")
        .version(env!("CARGO_PKG_VERSION"))
        .author("Mees Delzenne")
        .about("The Axle Command-line tool is meant to help developer with \
                managing axle projects.")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .global_setting(AppSettings::ColorAuto)
        .arg(Arg::with_name("no-context")
            .short("n")
            .long("nocontext")
            .help("Only use provided values, Dont use the values in the context like current workspace."))
        .arg(Arg::with_name("workspace")
            .short("w")
            .long("wspace")
            .value_name("PATH")
            .help("Set the axle workspace for which the command will execute.")
            .takes_value(true))
        .arg(Arg::with_name("verbosity")
            .short("v")
            .multiple(true)
            .help("Set the level of verbosity."))
        .subcommand(SubCommand::with_name("new")
                    .about("Command for creating a new axle project")
                    .setting(AppSettings::ArgRequiredElseHelp)
                    .arg(Arg::with_name("NAME")
                         .help("The name of the new project")
                         .required(true)
                         .index(1))
                    .arg(Arg::with_name("dont change")
                    .short("d")
                    .long("dont_change")
                    .help("Does not change the workspace to the new ")))
        .subcommand(SubCommand::with_name("run")
                    .about("Run an existing axle project"))
        .subcommand(SubCommand::with_name("build")
                    .about("Build a axle project"))
        .subcommand(SubCommand::with_name("workspace")
                    .about("Functionality dealing with the current workspace.")
                    .subcommand(SubCommand::with_name("set")
                        .about("Set current workspace. By default sets the workspace to the current dir.")
                        .arg(Arg::with_name("DIR")
                             .help("The directory to set the workspace to.")
                             .index(1)))
                    .subcommand(SubCommand::with_name("get")
                        .about("Print the current workspace"))
                    .subcommand(SubCommand::with_name("clear")
                        .about("Set the current workspace to none.")))
        .subcommand(SubCommand::with_name("import")
                    .about("Functionality for importing asset files.")
                    .arg(Arg::with_name("FILE")
                        .help("The path of the file to import")
                        .required(true)
                        .index(1))
                    .arg(Arg::with_name("NAME")
                        .help("The path of the file to import")
                        .required(true)
                        .index(2)))
        .get_matches();

    let mut builder = EnvironmentBuilder::new();

    if app.is_present("no-context"){
        builder = builder.no_context();
    }

    for _ in 0..app.occurrences_of("verbosity"){
        builder = builder.verboser();
    }

    if let Some(x) = app.value_of("workspace"){
        builder = builder.with_workspace(x);
    }

    let env = builder.build()?;

    logger::init_logger(env.verbosity);

    //println!("{:?}", app);
    if let Some(matches) = app.subcommand_matches("new"){
        new::new(&env,matches)?;
    }
    if let Some(_) = app.subcommand_matches("run"){
        unimplemented!();
    }
    if let Some(_) = app.subcommand_matches("build"){
        unimplemented!();
    }

    if let Some(matches) = app.subcommand_matches("workspace"){
        workspace::workspace(&env,matches)?;
    }

    if let Some(matches) = app.subcommand_matches("import"){
        import::import(&env,matches)?;
    }
    Ok(())
}
