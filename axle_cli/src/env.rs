
use std::path::{Path,PathBuf};
use std::{io,env,fmt,fs,os};

#[derive(Debug)]
pub enum Error{
    NoHomeDir,
    MissingConfig,
}

impl fmt::Display for Error{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::NoHomeDir => write!(f,"Could not find the user home directory to look for axle-cli-tool files"),
            Error::MissingConfig => write!(f,"Could not find the \"axle.toml\" configuration file in workspace"),
        }
    }
}

pub struct EnvironmentBuilder{
    workspace: Option<PathBuf>,
    verbosity: u32,
    allow_context: bool,
}


pub struct Environment{
    pub workspace: Option<PathBuf>,
    pub verbosity: u32,
    pub allow_context: bool,
}

impl Environment{
    #[cfg(target_os = "window")]
    pub fn change_workspace<P: AsRef<Path>>(p: P) -> Result<(),super::Error>{
        let mut path = p.as_ref().to_path_buf();
        if p.is_relative(){
            path = env::current_dir()?.join(path).canonicalize();
        }
        let data_dir = env::home_dir().ok_or(Error::NoHomeDir)?;
        let workspace_dir = data_dir.join(".axle/workspace");
        if !workspace_dir.exists(){
            Self::unset_workspace()?;
        }
        //TODO test on windows platform
        os::windows::fs::symlink_file(path, workspace_dir)?;
        Ok(())
    }

    #[cfg(any(target_os = "unix",target_os = "linux"))]
    pub(super) fn change_workspace<P: AsRef<Path>>(p: P) -> Result<(),super::Error>{
        let mut path = p.as_ref().to_path_buf();
        if path.is_relative(){
            path = env::current_dir()?.join(path).canonicalize()?;
        }
        let data_dir = env::home_dir().ok_or(Error::NoHomeDir)?;
        let workspace_dir = data_dir.join(".axle/workspace");
        if workspace_dir.exists(){
            Self::unset_workspace()?;
        }
        os::unix::fs::symlink(path, workspace_dir)?;
        Ok(())
    }

    pub(super) fn unset_workspace() -> Result<bool,super::Error>{
        let data_dir = env::home_dir().ok_or(Error::NoHomeDir)?.join(".axle/workspace");
        if !data_dir.exists(){
            return Ok(false)
        }
        fs::remove_dir_all(data_dir)?;
        Ok(true)
    }
}

impl EnvironmentBuilder{
    pub fn new() -> Self{
        EnvironmentBuilder{
            workspace: None,
            verbosity: 0,
            allow_context: true,
        }
    }

    pub fn with_workspace<P: AsRef<Path>>(mut self,p: P) -> Self{
        self.workspace = Some(p.as_ref().to_path_buf());
        self
    }

    pub fn verboser(mut self) -> Self{
        self.verbosity += 1;
        self
    }

    pub fn no_context(mut self) -> Self{
        self.allow_context = false;
        self
    }

    fn setup_env(path: &Path) -> Result<(),super::Error>{
        fs::create_dir(path)?;
        Ok(())
    }

    fn workspace() -> Result<Option<PathBuf>,super::Error>{
        let dir = env::home_dir().ok_or(Error::NoHomeDir)?;
        let axle_path = dir.join(Path::new(".axle"));
        if !axle_path.exists(){
            Self::setup_env(&axle_path)?
        }
        let workspace_link = axle_path.join("workspace");
        if !workspace_link.exists(){
            Ok(None)
        }else{
            let res = fs::read_link(workspace_link)
                .map_err(Into::into)
                .map(Some);
            if res.is_err(){
                Environment::unset_workspace()?;
            }
            res
        }
    }

    pub(super) fn build(self) -> Result<Environment,super::Error>{
        let workspace = if let Some(x) = self.workspace{
            Some(x)
        }else{
            if self.allow_context {
                Self::workspace()?
            }else{
                None
            }
        };
        let allow_context = self.allow_context;
        let verbosity = self.verbosity;
        Ok(Environment{
            workspace,
            verbosity,
            allow_context,
        })
    }
}
