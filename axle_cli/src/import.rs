

use super::axle_core::asset;
use ::axle_util::import;
pub use ::axle_util::import::Error as ImportError;
use self::asset::Error as AssetError;

use super::{env,clap};

use std::path::Path;
use std::{fmt,fs,error};


#[derive(Debug)]
pub enum Error{
    NoWorkspace,
    NoFiletype,
    FiletypeNotSupported,
    Import(ImportError),
    Asset(Box<error::Error>),
}

impl From<axle_import::Error> for Error{
    fn from(e: axle_import::Error) -> Self{
        Error::Import(e)
    }
}

impl<T: error::Error + 'static> From<AssetError<T>> for Error{
    fn from(e: AssetError<T>) -> Self{
        Error::Asset(Box::new(e))
    }
}

impl fmt::Display for Error{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::NoWorkspace => write!(f,"No workspace specified."),
            Error::NoFiletype => write!(f,"The file does not have an extension,\
                                                  filetype could not be determined"),
            Error::FiletypeNotSupported => write!(f,"The filetype for this file is not supported"),
            Error::Import(ref e) => write!(f,"Import error: {}", e),
            Error::Asset(ref e) => write!(f,"Asset error: {}",e),
        }
    }
}

impl error::Error for Error{
    fn description(&self) -> &str{
        match *self {
            Error::NoWorkspace => "No workspace was specified.",
            Error::NoFiletype => "The file type of the file to be imported could not be found.",
            Error::FiletypeNotSupported => "The file type of the file to be imported is not supported.",
            Error::Import(_) => "An problem happend during import.",
            Error::Asset(_) => "An problem happend in the loading of assets."
        }
    }
}

pub(super) fn import(e: &env::Environment,matches: &clap::ArgMatches) -> Result<(),super::Error> {
    let workspace = e.workspace.as_ref().ok_or(Error::NoWorkspace)?;
    let path = Path::new(matches.value_of("FILE").unwrap());
    if let Some(x) = path.extension(){
        let name_path = Path::new(workspace).join("res").join(matches.value_of("NAME").unwrap());
        fs::create_dir_all(&name_path.parent().unwrap())?;
        let filetype =  match x.to_str().unwrap() {
            "jpg" =>   asset::AssetType::Texture,
            "png" =>   asset::AssetType::Texture,
            "bmp" =>   asset::AssetType::Texture,
            "tga" =>   asset::AssetType::Texture,
            "ppm" =>   asset::AssetType::Texture,
            "tiff" =>  asset::AssetType::Texture,
            "dea" =>   asset::AssetType::Model,
            "fbx" =>   asset::AssetType::Model,
            "blend" => asset::AssetType::Model,
            "3ds" =>   asset::AssetType::Model,
            "obj" =>   asset::AssetType::Model,
            "stl" =>   asset::AssetType::Model,
            "ase" =>   asset::AssetType::Model,
            _ => return Err(Error::FiletypeNotSupported.into()),
        };
        match filetype{
            asset::AssetType::Texture => {
                let image = import::texture::import(path).map_err(Error::from)?;
                info!("Writing asset");
                asset::package::Package::write(&image,name_path).map_err(Error::from)?;
            },
            asset::AssetType::Model => {
                let model = import::model::import(path,&name_path).map_err(Error::from)?;
                info!("Writing asset");
                asset::package::Package::write(&model,name_path).map_err(Error::from)?;
            }
            _ => {
                return Err(Error::FiletypeNotSupported.into());
            }
        }
        Ok(())
    }else{
        Err(Error::NoFiletype.into())
    }
}
