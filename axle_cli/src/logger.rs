
use super::log;


struct Logger{
    verbosity: u32,
}

pub fn init_logger(verbosity: u32){
    log::set_logger(|max_level| {
        max_level.set(log::LogLevelFilter::max());
        Box::new(Logger{
            verbosity,
        })
    }).unwrap()
}

impl log::Log for Logger{
    fn enabled(&self,metadata: &log::LogMetadata) -> bool{
        match self.verbosity{
            0 => metadata.level() <= log::LogLevel::Info,
            1 => metadata.level() <= log::LogLevel::Debug,
            _ => metadata.level() <= log::LogLevel::Trace,
        }
    }

    fn log(&self, record: &log::LogRecord){
        if self.enabled(record.metadata()){
            println!("[{}] {}", record.level(),record.args());
        }
    }
}
