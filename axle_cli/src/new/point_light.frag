#version 330
#define M_PI 3.1415926535897932384626433832795
uniform sampler2D position;
uniform sampler2D normal;
uniform sampler2D albedo;
uniform float exposure;

uniform vec3 light_position;
uniform vec3 light_color;
uniform float light_strenght;
uniform vec3 view_position;
uniform float gamma = 2.2;

in vec2 tex_coord;

out vec3 color;

void main(){
    const float metalness = 0;

    vec3 position = texture(position, tex_coord).rgb;
    float dist = length(position-light_position);
    vec3 normal = texture(normal, tex_coord).rgb;
    vec4 temp = texture(albedo, tex_coord);
    vec3 diff_color = temp.rgb;
    float roughness = 0.01;

    vec3 view = normalize(view_position - position);
    vec3 light = normalize(light_position - position);
    vec3 h = normalize(light + view);

    //D term
    float diff = max(dot(light, normal),0.0);
    vec3 diffuse = diff * diff_color;

    float spec = pow(max(dot(normal, h), 0.0), 32.0);
    vec3 specular = vec3(0.2) * spec;

    float att = 1.0 / (1.0 + 0.1 * dist + 0.01 * dist * dist);
    att *= light_strenght;

    color = (diffuse + specular) * att;
}
