#version 330
in vec2 position;
in vec2 texture_coord;

out vec2 tex_coord;

void main()
{
    gl_Position = vec4(position, 0.5f, 1.0f);
    tex_coord = texture_coord;
}
