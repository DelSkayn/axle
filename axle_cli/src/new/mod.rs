
use clap::ArgMatches;
use super::Environment;

use std::process::Command;
use std::fs::{self, File};
use std::path::Path;
use std::env;

use std::io::Write;

static CARGO_TOML: &'static str = include_str!("Cargo.toml");
static AXLE_TOML: &'static str = include_str!("Axle.toml");
static MAIN_RS: &'static str = include_str!("main.rs");
static LIB_RS: &'static str = include_str!("lib.rs");
static POINT_LIGHT_FRAG: &'static str = include_str!("point_light.frag");
static POINT_LIGHT_VERT: &'static str = include_str!("point_light.vert");
static POSTPROCESS_FRAG: &'static str = include_str!("postprocess.frag");
static POSTPROCESS_VERT: &'static str = include_str!("postprocess.vert");
static GEOM_FRAG: &'static str = include_str!("geom.frag");
static GEOM_VERT: &'static str = include_str!("geom.vert");

pub(super) fn new(_: &Environment,matches: &ArgMatches) -> Result<(),super::Error> {
    let name = matches.value_of("NAME").unwrap();
    if name.len() < 2 {
        println!("Please choose a longer name");
        return Ok(());
    }
    let name = name.to_lowercase();
    fs::create_dir(&name)?;
    if let Err(e) = try_new(&name) {
        if let Err(e2) = fs::remove_dir_all(&name) {
            println!("Tried to remove directory after error but recieved another error: {:?}",
                     e2);
        }
        return Err(e);
    }
    if !matches.is_present("dont change"){
        Environment::change_workspace(&name)?;
        println!("Changed workspace to new directory.");
    }
    println!("Finished, Created new project \"{}\".",name);
    Ok(())
}

pub(super) fn try_new(name: &str) -> Result<(),super::Error> {
    let mut class = name[0..1].to_uppercase();
    class.push_str(&name[1..]);
    fs::create_dir(Path::new(name).join("src"))?;
    fs::create_dir(Path::new(name).join("res"))?;
    File::create(Path::new(name).join("Cargo.toml"))?
        .write_all(CARGO_TOML.replace("$NAME", name).as_bytes())?;
    File::create(Path::new(name).join("Axle.toml"))?
        .write_all(AXLE_TOML.as_bytes())?;

    {
        let mut main = File::create(Path::new(name).join("src").join("main.rs"))?;
        main.write_all(MAIN_RS
                           .replace("$NAME", name)
                           .replace("$CLASS", &class)
                           .as_bytes())?;
    }
    File::create(Path::new(name).join("src").join("lib.rs"))?
        .write_all(LIB_RS.replace("$NAME", name)
                     .replace("$CLASS", &class)
                     .as_bytes())?;

    let shader_path = Path::new(name).join("res").join("shaders");
    fs::create_dir(shader_path.clone())?;
    File::create(shader_path.join("geom.frag"))?
        .write_all(GEOM_FRAG.as_bytes())?;

    File::create(shader_path.join("geom.vert"))?
        .write_all(GEOM_VERT.as_bytes())?;

    File::create(shader_path.join("point_light.frag"))?
        .write_all(POINT_LIGHT_FRAG.as_bytes())?;

    File::create(shader_path.join("point_light.vert"))?
        .write_all(POINT_LIGHT_VERT.as_bytes())?;

    File::create(shader_path.join("postprocess.frag"))?
        .write_all(POSTPROCESS_FRAG.as_bytes())?;

    File::create(shader_path.join("postprocess.vert"))?
        .write_all(POSTPROCESS_VERT.as_bytes())?;


    Ok(())
}
