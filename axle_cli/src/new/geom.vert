#version 330

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texture_coord;
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out VS_OUT{
    vec3 position;
    vec2 tex_coord;
    mat3 tbn;
    vec3 normal;
} vs_out;

void main(){
    mat4 mv = view * model;
    gl_Position = projection * mv * vec4(position, 1.0);

    vs_out.position = (model * vec4(position,1.0)).xyz;
    vs_out.tex_coord = texture_coord;

    vs_out.normal = mat3(model) * normal;

    // model mat not the problem
    mat3 normal_matrix = mat3(model);
    vec3 t = normal_matrix * normalize(tangent);
    vec3 n = normal_matrix * normalize(normal);
    vec3 b = normal_matrix * normalize(bitangent);

    //orthogonalize not the problemm
    t = normalize(t - n * dot(n,t));
    b = normalize(b - n * dot(n,b));

    mat3 tbn = mat3(t,n,b);
    vs_out.tbn = tbn;
}
