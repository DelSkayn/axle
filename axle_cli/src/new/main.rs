extern crate $NAME;
extern crate axle;

fn main(){
    axle::go::<$NAME::$CLASS>();
}
