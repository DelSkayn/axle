#[macro_use]
extern crate axle;

use axle::Game;
use axle::render::Render;
use axle::window::Window;

export_game!($CLASS);

pub struct $CLASS{
    timer: u64,
}

impl Game for $CLASS{
    fn new(_: &mut Render,_: &Window) -> Self{
        $CLASS{
            timer: 0,
        }
    }

    fn update(&mut self,running: &mut bool,_: &Window, _: &mut Render){
        self.timer += 1;
        *running = self.timer < 300;
    }
}
