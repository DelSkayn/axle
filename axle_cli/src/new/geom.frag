#version 330
#define M_PI 3.1415926535897932384626433832795

uniform float reflectance = 0.2;
uniform float metalness = 0.0;
uniform vec3 spec_color;

uniform sampler2D diffuse_map;
uniform sampler2D normal_map;

uniform bool has_normal_texture;

in VS_OUT{
    vec3 position;
    vec2 tex_coord;
    mat3 tbn;
    vec3 normal;
} fs_in;

out vec4 albedo;
out vec3 normal;
out vec3 position;


void main(){
    if(false){
        vec3 n = texture(normal_map,fs_in.tex_coord).gbr;
        n = normalize(n * 2.0 - 1.0);
        normal = normalize(fs_in.tbn * n);
    }else{
        normal = normalize(fs_in.normal);
    }
    vec3 color = texture(diffuse_map, fs_in.tex_coord).rgb;
    albedo = vec4(color,1.0f);
    position = fs_in.position;
}
