#version 330
uniform float gamma;
uniform float exposure;
uniform sampler2D in_color;

in vec2 tex_coord;
out vec4 out_color;

float A = 0.15;
float B = 0.50;
float C = 0.10;
float D = 0.20;
float E = 0.02;
float F = 0.30;
float W = 11.2;

/// Taken from a slide about uncharted 2
vec3 tonemap(vec3 x){
    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

void main(){
    vec3 color = texture(in_color,tex_coord).rgb;
    vec3 mapped = vec3(1.0) - exp(-color * exposure);
    //mapped = pow(mapped, vec3(1.0/ gamma));
    out_color = vec4(mapped,1.0);
}
