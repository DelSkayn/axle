
use super::{env,clap,consts};
use std::env as sysenv;
use std::path::Path;

pub(super) fn workspace(e: &env::Environment,matches: &clap::ArgMatches) -> Result<(),super::Error> {
    if !e.allow_context{
        info!("Cannot change workspace when no context use is allowed!");
        return Ok(());
    }
    if let Some(x) = matches.subcommand_matches("set") {
        if let Some(x) = x.value_of("DIR"){
            let path = Path::new(x);
            if !path.exists(){
                warn!("Given path does not exists!");
                info!("Workspace not set.");
                return Ok(());
            }
            if !path.join(consts::CONFIGURATION_FILE_NAME).exists(){
                warn!("No axle.toml configuration file found at given path.");
                info!("Workspace not set.");
                return Ok(());
            }
            env::Environment::change_workspace(path)?;
            info!("Workspace set succesfully");
            return Ok(());
        }else{
            if !Path::new(consts::CONFIGURATION_FILE_NAME).exists(){
                warn!("No axle.toml configuration file found in current directory.");
                info!("Workspace not set.");
                return Ok(());
            }
            env::Environment::change_workspace(sysenv::current_dir()?)?;
            info!("Workspace set succesfully");
            return Ok(());
        }
    }else if matches.subcommand_matches("get").is_some(){
        if let Some(x) = e.workspace.as_ref(){
            println!("{}",x.to_str().unwrap_or("Could not convert path"));
        }else{
            println!("There is currently no workspace set.")
        }
    }else if matches.subcommand_matches("clear").is_some(){
        if env::Environment::unset_workspace()?{
            info!("Cleared workspace.");
        }else{
            info!("No workspace was set.");
        }
    }else{
        if let Some(x) = e.workspace.as_ref(){
            println!("{}",x.to_str().unwrap_or("Could not convert path"));
        }else{
            println!("There is currently no workspace set.")
        }
    }
    Ok(())
}
