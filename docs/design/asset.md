What
----

The asset system deals with loading of nessessary static data during runtime.
Any data that is needed in the engine and is not part of the executable is considered an asset, 
excluding the specific configuration files of the registery.

Features
--------
```
1) Async loading
    load asset without blocking the execution of the game.

    1) Asset handle
        A way to represent an asset even while it is loading.

    2) Thread pool
        A way to implement async loading is by having the loads be done on a different thread.

        We will use the rayon thread pool

    3) 

2) Hotloading
    respond to changes in asset files and update the used assets as the engine is running

    1) File change notification
        A system that is capable of recognizing changes.

        We will use INotify for this

    2) File reloading
        A system that allows files to be loaded again

        This part is trivial since we already will need an async loading sollution 
        we can use that to reload the nessessary files

    3) Asset reloading
        A way to reload assets.

        This should be part of the handle. 
        Not only should it be capable of representing unloaded assets but also assets which are being reloaded. 
        There should also be some way to recognize a asset which has been reloaded.

    4) System reloading
        A way for systems which use assets to see reloading changes.

        Possible Solutions:
            A) Polling
                In this case the system would need to check if a asset has been changed.
                This could be done by keep track of the version of assets and check if an asset version has changed
                   
                Advantages:
                    - Simple
                    - Little coupling
                Problems:
                    - Performance?
                    - How often should one check changes
                    - Possible not immediate changes

                Thoughts:
                    I find this solutions a bit to simple. Like its the easy way out.
                    Just check every frame if things have changed.
                    That seems to me like something that might lead to poor performance,
                    And we already have some system which checks what changes and changes corresponding assets,
                    I think I should be able to hook into such a system.


            B) Callbacks
                In this case the system registers some kind of callback to the asset 
                system an is notified when a reload takes place.

                Advantages:
                    - Immediate response to changes
                    - Simple to use once system is in place.
                Disadvantages:
                    - Hard to syncronize
                    - Coupling problems

                Thoughts:
                    Conceptually I like this one the most.
                    However syncronizing the different threads so that callbacks can happen might be a bit of a nightmere.
                    This problem is extended by the fact that the renderengine is not in lockstep with the game logic.

            C) Events
                In this case the asset system would have some event source which other systems could use to determin changes
                
                Advantages:
                    - Good decoupling
                    - Immediate response to changes
                Disadvantages:
                    - Performance
                    - Complexity
                    - Hard to extend
    
                Thoughts:
                    This one might fit the engine parallel structure the best but I dont like it.
                    Events seem to introduce a lot of copying and unnessary function calls.
                    To explain: if a event is raised but non of the systems use it it will still need to be
                    created copied over to each system and then each system needs to decide to not used it.


        
3) Location handeling
    Load assets from various different cross-platform locations like: resources, local user, tempary, etc. 

4) Cache
    Cache various assets in order to avoid loading duplicates. 

5) Asset identification
    Different assets should be able to be recognized quickly.

6) LOD
    Level of detail handeling allowing assets to be loaded at various level of detail.
    LOD systems seems to have a lot of parst which are unique to the asset so it should probably be implemented by the asset and not have an overarching system.


```
Design version 1
-------------------------------

The first design consisted of 4 primary components
1. Assets
    A struct which managed the loading and storing of the assets.
2. Handle (or AssetRef)
    A struct which managed access to a asset and managed dealing with replacing assets.
3. Asset Trait
    A trait implemented by all the different types of assets.
4. Asset Data layout

The assets struct would be what a user who wants to load assets interacts with.
It has a `load<A: Asset>()` function which is used get a handle to an asset and initiate loading if the asset was not loaded already.
For loading a threadpool was used on which a job was created for each asset which needed to be loaded.
For this design the Asset struct was not directly created but instead a lazy static value initialized on first interaction.

Assets define how they are loaded inside the asset trait.
The asset trait looks as following:
```
pub trait Asset{
    type Data: Serialize + Deserialize;
    const ty: AssetType;
    const format: AssetFormat;

    fn load(data: Self::Data) -> Result<Self>;

    fn write(&self,asset_writer: &AssetWriter) -> Result<()>;
}
```
The `load()` function defines how the asset is loaded.
It can return a result in case the loading can go wrong.
This function takes a Data struct which the type defines it self.
This data type is what is read from a file in a format which implements the Deserializer trait from serde.
The design used either the bincode format, a binary format which seems to mostly be used for sending messages across channels, or the json format.
The implementation is responsable for kicking of loading any other assets which the asset that is being loaded might reference

Futhermore the trait also defines 2 constants.
First ty which is of the type AssetType an enum which is unique for each asset type and which is used for determining the type of asset contained in a file.
Second format which defined the type of format namely text or bytes, text being json format and bytes being the bincode format.

Lastly the trait also contains a `write()` function which is used in exporting assets.
The function takes a reference to self and turns that into the defined Data type for writing to a file.
Since a asset might reference other assets the function would need a way to start the writing of other assets.
This is what the AssetWriter type is for, it has functions for starting writing of other assets.

In order to facilitate hotloading and loading asyncronous the asset data would need to be able the change at any time.
This means that direct access to the data is not possible and some abstraction is nessessary in between the reference to an asset and the data.
The design uses a struct called AssetRef.
This struct allowed access to assets while also being able to change the underlying data.
This was implemented using the Atomic type from the crossbeam library.
The Atomic type guarentees that data borrowed will be available for the duration borrow.
So even if the underlying data changed possible borrows would still see the same asset as before the change until the asset was reborrowed.
One could check if the asset had changed by comparing version.
The version of an asset is an new type over an integer which increments when a change has happend.
So checking if an asset is different would only be integer comparison.

The design also had its own format which would evently allow for packing multiple assets into as single file.
However at the moment of scrapping the own format did not add a lot to the design.
The only thing it did was that assets had an uniform format.


How does amethyst do things?
----------------------------

The amethyst engine has a pretty good solution to handeling assets.
The requirements for the assets system in amethyst and the requirements I have seem to be pretty simular if not the same.
However the design seems to have been designed with specs, the ECS library used by amethyst, in mind.

Amethyst has a more split up approch, instead of having one struct do the loading and the storing the engine has a seperate loader struct and a storage struct.
Each asset has its own `AssetStorage<A: Asset>` struct.
Assets are retrieved by there handles: `Handle<A: Asset>`.
The handle contains an offset in an array.
This probably results in reasonably good access speeds.
Handles are retrieved on loading an asset using the `Loader` struct.
The loader does not care about asset type and is used for loading all possible types of assets.
This loading is done via the `load<..>(name: &str,format: F,options: F::Options,progress: P, storage: &Storage) -> Handle<A>` function (the function here is simplified).
The name is a string which handed to a source which then returns data.
A source is a struct which implements the trait `Source` which looks as following:
```
trait Source{
    fn get(&self,name: &str) -> Result<Vec<u8>>;
}
```
The source is thus an abstraction over loading and can be used to set file load location or do extra functionality on load.
In the implementation the source has also a function for dealing with reloading but this has been removed from the example for brevity.
After the data is returned by a source the struct passed in a format takes the data and tries to construct a assets from it.
A format is a struct which implements the `Format` trait which looks as following:
```
trait Format<A: Asset>{
    type Options; 

    fn format(&self,data: Vec<u8>,options: Options) -> Result<A>;
}
```
The handle which is returned is constructed from the passed asset storage and returned after starting loading while the loading is happening. 
The name is thus not used for anything other than finding out where to load the asset from.
This means that the system has no way to determin if a file is already loaded.

The asset storage itself does not allow for parallel access but instead relies on specs to manage access.

The asset systems also employs a subsystem to deal with determining loading progress and error handeling.
This system uses a struct called tracker which counts the amount of assets that are being loaded and will recieve any errors that might be produced.

Debate
------

The following section is not really wel written or thought out.
Its primary function is to write down and organize my thoughts.
Thoughts might not be finised or be contradictory.


The implementation of amethyst is rather good I am going to copy some parts of the amethyst asset system but I have some changes.
First the amethyst engine focuses on specs and thus the solutions will be designed with specs in mind but I would like my engine to be able to be used without specs.
I am still not 100% confinced that ECS are the way to go so I would like to be able to swap out the main loop.
Futhermore specs is missing some functionality I would like to see.
For instance I would like a way to send messages from one component to other components.
I currently do not see a way to do that.


Second asset system in amethyst is focused on using formats this promotes using external formats.
External formats are great.
I will probably never write a image format which has better compression then png so using those formats is probably fine.
But with external formats comes external assumptions which might not line up with the engines assumptions.
Futhermore external formats might not allow storing metadata which the engine might need.
Take models, obj does, by default, not have a good way to store pbr material values.
Especially the format of game world would need to be custom.
Also custom formats might be slow to load.
KSP load times where really bad for a while before they switched formats.
If I only allow loading assets with predifined formats it might be possible to avoid this.

Third the engine reclames handles by going each frame through all handles and checking if they are still alive.
I think this can be done better by having the handles be reclamed on drop.
How this can be done better is however not so simple.
The simpelest solution is to include a pointer to the storage inside the handle and when the handle is dropped use the pointer to automaticly subtract count and when 0 add the pointer to a list of dropped pointers.
I think this is fine.
It still allows quick indexing while also avoiding the O(n) where n is the amount of assets cost each frame at the expance of a few extra atomic increments and an extra pointer stored in the handle.


Fourth amethysts asset system has no direct way of determining if a file is already loaded.
The only way to make sure that an asset is only loaded once is to assign it an other key put it in a `Cache` struct and first check if the asset is already pressent in cache if loading a file.
I do not believe this is the way to do things since this means default behaviour is loading duplicate assets while by default asset duplicates should be avoided.
If assets can reference other assets determining if a file is already loaded is crucial, otherwise if we have multiple asset which reference the same asset it would be duplicated in memory.
Determining what asset is loaded is not as strait forward as it used to be before the introduction of sources.
Because sources determin what file is loaded it 2 assets with the same name might be different assets. 
This means that if we would want to use the name of an asset to determin if a asset is loaded we would need to use the source as only it can tell if to files are different.
However sources have no knowledge of handles which is a problem cause if an asset is already loaded whe should return the handle of the asset which is already loaded.
But source has no idea which handle belongs to which asset and adding that to the source might be a bit of a violation of single purpose.



Finally while the tracker system is an elegent way to deal with progress it I have some reservations about using at as a way to deal with errors.
Since the tracker is an object which is pased into the load function it the default way of handeling things might be to ignore the tracker.
Which goes against the rust way of explicit error handeling.
The tracker can be made `#[must_use]` which might solve this.

Lastly the design of amethyst has one major problem.
Assets cannot trigger the loading of other assets.
The way the loader is written prefents the loading of assets inside asset implementation since the loader cannot be referenced.
I think this is a big problem.
Manually having to load all the meshes for a scene and then for each of those meshes loaded also load materials which possibly need multiple textures
is way to much boilerplat for loading assets.

The solution
-------------

THe amethyst solution is thus good but not good enough to directly use.
So we need to makes some adaptions.

### Handle reclaming
In order to avoid a O(n) with n being the amount of assets in preformance cost each frame we need a better way to reclaim handles.
This can probebly be done best by having the handles have some sort of a pointer to the storage to notify it of its demise.
A possible way is to have to handle have a reference to the process queueu which is currently only used for new assets.
When an handle is dropped and it was the last handle of its asset it will just push a message to the que for processing.

### The part where asset reference each other.
This problem consists of 2 parts.

1. Allowing assets to trigger the loading of other assets
    In order to allow this we need to solve some more sub problems.

    1. The asset loading pipeline needs to be able to reach functionality for loading assets somewhere.
    ```
        For the question of where in the pipeline child asset loading needs to take place the final conversion from Data to Asset is a good place.
        Then what provides the asset loading functionality:
            A global object?
                The easy way, quick but can become confusing
            A refenrence to the loader object.
                I think this is the best option
                Forces the loading of child assets to happen in the final step since only then the objects is accessable.
                Note: This will lead to some problems with loading progress tracking.
    ```
    2. Assets need a way of accessing formats of referenced assets.
        This is a problem since the format trait has a generic argument we cant just store all formats in a box of the same type.
        Currently I have the following solution:

        * Have the asset downcast the data to the proper type.
           Eww, why do I even use static typing
           Possibly allows formats to load data which is not the right type
           Also allocation only for bypassing the type system.
        * Store formats as a other boxed trait object and then transmute back to the right format when used.
           Pretty gross hack.
           Since we know when we use a format what the type of the asset is where loading we also know the trait where using so 
           it will not cause problems where a trait is converted to the wrong type.
           Does solve to problem but uses a transmute which does work but is not guarenteed to work by lang spec.
           So the hack can possibly fail after every update to the compiler.
        * Using the generic constants we can just store the format as a [u8; size_of::<Box<Format<A>>>()].
            Will possibly break less quick but can still breack if size_of::<Box<Format<A>>>() != size_of::<Box<Format<A>>>()
            can be true. There is no guarentee that it wil never be true.
        * Remove formats althogether.
            The best way to solve a problem is by saying it does not exist /s
        * An typeid map which holds a the formats in a other map which is downcast to the proper type when nessessary

    3. Asset need a way to determining which format a asset needs.
        This was done in amehyst by passing a the format as a argument to the load function.
        However since assets can load assets we can not just pass in the right format since an asset might reference assets of other formats.
        Possible sollutions:
        
        * Determin the format from the extension.
            In this solution the file extension determines what format is used for which asset.
            Fromats would then be registered in the loader with the extension they belong to.

    4. Assets need to know where the assets need to be stored.
        This was also solved in amethyst by just passing in a object.
        But when assets have childeren those childeren also need to know where there stored.
        Passing in a storage for each possible asset type which might be loaded is not feasable.
        So we need some other solution.
        Possible solutions:
        
        * Register storage fo each asset type.
            Would only need to reference the processor or the queue which contians new assets.
            Allows only one storage but I dont see many instances where one would need more then one storage.
            Dont like the many cross referneces nessary.
        * Merge all storage back into 1 object.
            Solves the storing of assets since all posible assets are stored in the same place.
            Would need to implement syncronization menually.
            Also might collised a bit with how specs works.
            Specs prevers things that are accessed seperatly to be seperate objects so that it can parralize more effectively.
        
