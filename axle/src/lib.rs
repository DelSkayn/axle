//! This is the prime axle crate.
//! Although axle's code is split up into multiple crates.
//! This crate exposes all the crates for use in a confient wrapper.
//! The crate also implements some components nessary for the hotloading functionality.

#[macro_use]
extern crate log;

extern crate axle_core;
pub use self::axle_core::{registery, console, task, window, asset,  cgmath, common};
#[cfg(feature = "dylib")]
pub use self::axle_core::hotload;

pub use window::{Window, InputEvent};
//use window::context::Context;

use std::thread;
use std::time::Duration;

pub extern crate axle_render;
pub use self::axle_render as render;
use self::axle_render::{Scene,Render};

#[macro_export]
macro_rules! export_game{
    ($g:ident) => {
        #[allow(non_camel_case_types)] #[allow(dead_code)]
        struct __assertion_struct{
            x: $g
        }

        impl __assertion_struct{
            #[allow(dead_code)]
            fn if_this_function_fails_to_compile_your_given_struct_does_not_impl_game()
                where $g: Game
                {}
        }

        #[no_mangle]
        #[cfg(feature = "dylib")]
        pub unsafe extern fn __axle_update_game(g: *mut (),r: &mut bool,w: &Window,render: &mut Render){
            use std::mem;
            mem::transmute::<*mut(),&mut $g>(g).update(r,w,render);
        }

        #[no_mangle]
        #[cfg(feature = "dylib")]
        pub unsafe extern fn __axle_create_new_game(r: &mut Render,w: &Window) -> *mut (){
            use std::mem;
            let temp = $g::new(r,w);
            mem::transmute(Box::into_raw(Box::new(temp)))
        }
    }
}

pub trait Game {
    fn new(&mut Render,scene: &mut Scene, &Window) -> Self;

    fn update(&mut self, should_continue: &mut bool, window: &Window, render: &mut Render,scene: &mut Scene);
}

pub fn go<G: Game>() {
    {
        console::Console::update();
        console::predefined::register();
        let mut window = window::WindowBuilder::from_registery().build();
        let context = window::context::OpenGlContext::new(&window);
        //let mut render = Render::new(Context::new(&window));
        let (mut render,mut scene) = Render::new(context);
        let mut g = G::new(&mut render,&mut scene, &window);
        let mut running = true;
        while running {
            window.update();
            g.update(&mut running, &window, &mut render,&mut scene);
            console::Console::update();
            thread::sleep(Duration::from_millis(16));
        }
    }
    info!("Engine quit, Have a good day!");
}
